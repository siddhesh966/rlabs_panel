$(".show-more").click(function(){
  $(".more-text").show("fast");
  $(this).hide("fast");
  $(".show-less").show("fast");
});
$(".show-less").click(function(){
  $(".more-text").hide("fast");
  $(this).hide("fast");
  $(".show-more").show("fast");
});

  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });


  // top menu highlight
  $('#navi .nav-item').each(function(){
    var fg = $(this).find('a').attr('href');

    if(window.location.href.indexOf(fg)>-1){
        var gh=$(this).find('a').addClass('active');
        $('a').not(gh).removeClass('active')
    }
});

// dropdown active
$("#indcomp").change(function(){
  $(this).find("option:selected").each(function(){
      var optionValue = $(this).attr("value");
      if(optionValue){
          $(".comp").not("." + optionValue).hide();
          $("." + optionValue).show();
      } else{
          $(".comp").hide();
      }
  });
})

$("#indcomp-lead").change(function(){
  $(this).find("option:selected").each(function(){
      var optionValue = $(this).attr("value");
      if(optionValue){
          $(".compa").not("." + optionValue).hide();
          $("." + optionValue).show();
      } else{
          $(".compa").hide();
      }
  });
})
  




  
