(function (window, document) {

    window.masterJS = {
        initialize: function () {
        },
        hitAjax: function (url, parameter, method, callback, error) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: method,
                cache: false,
                timeout: 20000,
                dataType: 'text',
                data: parameter
            }).done(function (response) {
                callback(response)
            }).error(function (response) {
                error(response)
            });
        }
    };
})(window, document);

function ajaxFetch(routeName, formId, successDivId, errorDivId,
                   loaderClass, showToastMsg = true, method = "post", getRawResponse = false,
                   callback) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: method,
        url: routeName,
        data:  new FormData($(formId)[0]),
        beforeSend: function() {
            $(loaderClass).show();
        },
        cache: false,
        contentType: false,
        processData:false,
        success: function(response) {
            if(getRawResponse == true){
                callback(response);
            }else{
                if(response.code == 200){
                    if(response.type == 'error'){
                        if(showToastMsg == true){
                            toastr.error(response.message, 'Error', toastr.options);
                        }
                        $(errorDivId).show();
                        $(errorDivId).html(response.view);
                        $(successDivId).hide();
                    }else if(response.type == 'success'){
                        if(showToastMsg == true){
                            toastr.success(response.message, 'Success', toastr.options);
                        }
                        $(successDivId).show();
                        $(successDivId).html(response.view);
                        $(errorDivId).hide();
                    }
                }else{
                    toastr.error('Something went wrong...!', 'Error', toastr.options);
                }
                $(loaderClass).hide();
            }
        }
    });
}

function showConfirmMessage(text = "", type = "warning",
                            confirmButtonText = "Yes, delete it!",
                            successTitle = "Success!", successMsg = "Task completed successfully.",
                            errorTitle = "Error!", errorMsg = "Something went wrong.", routeName = "",
                            loaderClass = "", method = "DELETE") {
    swal({
        title: "Are you sure?",
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: confirmButtonText,
        closeOnConfirm: false
    }, function () {
        ajaxFetch(routeName, "", "", "",
            loaderClass, true, method, true,
            function (response) {
                if(response.code == 200){
                    if(response.type == 'error'){
                        toastr.error(response.message, 'Error', toastr.options);
                        swal(errorTitle, errorMsg, "error");
                    }else if(response.type == 'success'){
                        toastr.success(response.message, 'Success', toastr.options);
                        swal({
                            title: successTitle,
                            text: successMsg,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#dc3545",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false
                        }, function () {
                            location.reload();
                        });
                    }
                }else{
                    toastr.error('Something went wrong...!', 'Error', toastr.options);
                }
                $(loaderClass).hide();
            });
    });
}

function showMessage(title = "", text = "", type = "success",
                            confirmButtonText = "OK", routeName = "") {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: false,
        confirmButtonColor: "#dc3545",
        confirmButtonText: confirmButtonText,
        closeOnConfirm: false
    }, function () {
        location.href = routeName;
    });
}
