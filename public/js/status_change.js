$('.status-change').change(function () {
    var obj = $(this);
    var id = obj.attr('id');
    var value = obj.val();

    bootbox.confirm({
        message: "Are you sure want to change status ?",
        buttons: {
            confirm: {
                label: 'Confirm',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === false) {
                obj.val(value == 1 ? 0 : 1);
            } else if (result === true) {
                masterJS.hitAjax(delete_url + '/' + id, {}, 'GET', function (success) {
                    console.log('success', success);
                    obj.val(value == 1 ? 1 : 0);
                    toastr.success('Status changed successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('error', error);
                    obj.val(value == 1 ? 0 : 1);
                    toastr.error('Unable to change status.', 'Error', toastr.options);
                });
            }
        }
    });
});


$('.delete-record').click(function () {
    var obj = $(this);
    var id = obj.attr('id');

    bootbox.confirm({
        message: "Are you sure want to delete this record ?",
        buttons: {
            confirm: {
                label: 'Confirm',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === true) {
                masterJS.hitAjax(delete_url + '/' + id, {}, 'DELETE', function (success) {
                    console.log('success', success);
                    location.reload();
                }, function (error) {
                    console.error('error', error);
                    toastr.error('Unable to delete this record.', 'Error', toastr.options);
                });
            }
        }
    });
});
