@php

    $mainPath = Request::segment(1);

    $page_title = 'Content';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraScripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">

    <script src="https://cloud.tinymce.com/5/tinymce.js"></script>
    <script type="text/javascript">tinymce.init({selector: 'textarea'});</script>
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->content_id]) }}"
                                  method="post"
                                  data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="content_title">Content Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="content_title" name="content_title"
                                               value="{{ $detail->content_title }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Content title field is required."
                                               placeholder="Enter content title">
                                    </div>

                                    <div class="form-group">
                                        <label for="content_body">Content Body
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control" id="content_body" name="content_body"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="content body field is required."></textarea>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
