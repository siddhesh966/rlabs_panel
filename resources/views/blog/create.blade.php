@php

    $mainPath = Request::segment(1);

    $page_title = 'Blog';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <form role="form" action="{{ route($mainPath.'.store') }}" method="post"
                                  data-parsley-validate>

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="faq_blogTitleque">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="blogTitle" name="blogTitle"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Title field is required."
                                               placeholder="Enter Title">
                                    </div>

                                    <div class="form-group">
                                        <label for="blogDesc">Description
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control ck-editor" id="blogDesc" name="blogDesc"
{{--                                              data-parsley-required="true"--}}
{{--                                              data-parsley-required-message="Description field is required."--}}
                                              placeholder="Enter Description" rows="4"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="blogImage">Image
                                            <span class="red">*</span>
                                        </label>
                                        <input type="file" class="form-control"
                                               id="blogImage" name="blogImage"
                                               data-parsley-required="true" accept="image/*"
                                               data-parsley-required-message="Blog image field is required.">
                                    </div>

                                    <div class="form-group">
                                        <label for="sequence">Sequence</label>
                                        <input type="number" class="form-control"
                                               id="sequence" name="sequence"
                                               placeholder="Enter sequence">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'faq_ans' );
    </script>
@endsection
