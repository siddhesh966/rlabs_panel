@php

    $mainPath = Request::segment(1);

    $page_title = 'Blog';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$details->blog_id]) }}" method="post"
                                  data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="blogTitle">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="blogTitle" name="blogTitle" value="{{ $details->blogTitle }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Title field is required."
                                               placeholder="Enter title">
                                    </div>

                                    <div class="form-group">
                                        <label for="blogDesc">Description
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control ck-editor" id="blogDesc" name="blogDesc"
                                              data-parsley-required="true"
                                              data-parsley-required-message="Description field is required."
                                              placeholder="Enter description" rows="4">{{ $details->blogDesc }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="blogImage">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control" accept="image/*"
                                               id="blogImage" name="blogImage"
                                                {{--data-parsley-required="true" --}}
                                                {{--data-parsley-required-message="Banner image field is required."--}}
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Blog Image Preview
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <div>
                                            <img src="{{ $detail->blogImage }}" alt="blog image"
                                                 style="width: 200px;height: auto;">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="sequence">Sequence</label>
                                        <input type="number" class="form-control"
                                               id="sequence" name="sequence" value="{{ $details->sequence }}"
                                               placeholder="Enter sequence">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'faq_ans' );
    </script>
@endsection
