@php

    $mainPath = Request::segment(1);

    $page_title = 'Promo';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->promo_code_id]) }}"
                                  method="post"
                                  data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="promo_code_name">Promo code name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="promo_code_name" name="promo_code_name"
                                               data-parsley-required="true"
                                               value="{{ $detail->promo_code_name }}"
                                               data-parsley-required-message="Promo code name field is required."
                                               placeholder="Enter promo code name">
                                    </div>

                                    <div class="form-group">
                                        <label for="promo_code_discount">Promo code discount
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="promo_code_discount" name="promo_code_discount"
                                               data-parsley-required="true"
                                               data-parsley-type="number"
                                               value="{{ $detail->promo_code_discount }}"
                                               data-parsley-required-message="Promo code discount field is required."
                                               placeholder="Enter promo code discount">
                                    </div>

                                    <div class="form-group">
                                        <label for="promo_code_discount_type">Display Order
                                            <span class="red">*</span>
                                        </label>
                                        <select class="form-control" name="promo_code_discount_type"
                                                id="promo_code_discount_type"
                                                data-parsley-required="true"
                                                data-parsley-required-message="Promo code discount_type field is required"
                                        >

                                            <option value="">select promo code discount type</option>
                                            <option value="1"
                                                    @if ($detail->promo_code_discount_type == 1) selected @endif>
                                                Percentage
                                            </option>
                                            <option value="2"
                                                    @if ($detail->promo_code_discount_type == 2) selected @endif>Rupees
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="start_date">Start Date
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control datepicker"
                                               id="start_date" name="start_date"
                                               data-parsley-required="true"
                                               value="{{ \Carbon\Carbon::parse($detail->start_date)->format('d-m-Y') }}"
                                               data-parsley-required-message="Promo start date field is required."
                                               placeholder="Enter promo start date">
                                    </div>

                                    <div class="form-group">
                                        <label for="end_date">End Date
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control datepicker"
                                               id="end_date" name="end_date"
                                               data-parsley-required="true"
                                               value="{{ \Carbon\Carbon::parse($detail->end_date)->format('d-m-Y') }}"
                                               data-parsley-required-message="Promo end date field is required."
                                               placeholder="Enter promo end date">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            showButtonPanel: true,
            minDate: 0
        });
    </script>
@endsection
