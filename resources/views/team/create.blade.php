@php

    $mainPath = Request::segment(1);

    $page_title = 'Testimonials';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <form role="form" action="{{ route($mainPath.'.store') }}" method="post"
                                  enctype="multipart/form-data" data-parsley-validate>

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="emp_name">Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="emp_name" name="emp_name"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Name field is required."
                                               placeholder="Enter name">
                                    </div>

                                    <div class="form-group">
                                        <label for="emp_designation">Designation
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="emp_designation" name="emp_designation"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Designation field is required."
                                               placeholder="Enter details">
                                    </div>

                                    <div class="form-group">
                                        <label for="emp_desc">Description
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="emp_desc" name="emp_desc"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Description field is required."
                                               placeholder="Enter Description">
                                    </div>

                                    <!-- <div class="form-group">
                                        <label for="emp_profile_filename">Profile Image
                                            <span class="red">*</span>
                                        </label>
                                        <input type="file" class="form-control"
                                               id="emp_profile_filename" name="emp_profile_filename"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Profile image field is required."
                                               placeholder="Select profile image">
                                    </div> -->

                                    <div class="form-group">
                                        <label for="sequence">Sequence</label>
                                        <input type="number" class="form-control"
                                               id="sequence" name="sequence"
                                               placeholder="Enter sequence">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
