@php

    $mainPath = Request::segment(1);

    $page_title = 'Team Member';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$details->id]) }}" method="post"
                                  enctype="multipart/form-data" data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="emp_name">Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="emp_name" name="emp_name" value="{{ $details->emp_name }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Name field is required."
                                               placeholder="Enter name">
                                    </div>

                                    <div class="form-group">
                                        <label for="emp_designation">Designation
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="emp_designation" name="emp_designation" value="{{ $details->emp_designation }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Designation field is required."
                                               placeholder="Enter designation">
                                    </div>

                                    <div class="form-group">
                                        <label for="emp_profile_filename">Profile Image
                                            <span class="red">*</span>
                                        </label>
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="file" class="form-control"
                                                       id="emp_profile_filename" name="emp_profile_filename"
                                                       placeholder="Select profile image">
                                            </div>
                                            <div class="col-md-1">
                                                @if(is_string($details->emp_profile_filename) && $details->emp_profile_filename !== '')
                                                    <a href="#" class="ModalDocumentViewer btn btn-primary"
                                                       data-toggle="modal" data-target="#ModalView" data-prod-id="Profile Image"
                                                       data-id="{{ fetch_file($details->emp_profile_filename, 'team_images') }}" title="View File">
                                                        View
                                                    </a>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="sequence">Sequence</label>
                                        <input type="number" class="form-control"
                                               id="sequence" name="sequence" value="{{ $details->sequence }}"
                                               placeholder="Enter sequence">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
