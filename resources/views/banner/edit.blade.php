@php

    $mainPath = Request::segment(1);

    $page_title = 'Banner';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->banner_id]) }}" method="post"
                                  data-parsley-validate enctype="multipart/form-data">

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="banner_title">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="banner_title" name="banner_title"
                                               value="{{ $detail->banner_title }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Banner title field is required."
                                               placeholder="Enter banner title">
                                    </div>

                                    <div class="form-group">
                                        <label for="banner_description">Description
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <textarea class="form-control" cols="10" rows="3"
                                                  name="banner_description" id="banner_description"
                                                  {{--data-parsley-required="true"--}}
                                                  {{--data-parsley-required-message="Service description field is required."--}}
                                        >{{ $detail->banner_description }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="banner_image">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control" accept="image/*"
                                               id="banner_image" name="banner_image"
                                                {{--data-parsley-required="true" --}}
                                                {{--data-parsley-required-message="Service image field is required."--}}
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Banner Image Preview
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <div>
                                            <img src="{{ $detail->banner_image_path }}" alt="banner image"
                                                 style="width: 200px;height: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
