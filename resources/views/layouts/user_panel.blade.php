<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ asset('img/admin.png') }}" class="img-circle" alt="User Image"/>
    </div>
    <div class="pull-left info">
        <p>Hello, {{ Auth::user()->name }}</p>

        <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
