@php
    if (!Session::has('last_updated_at') || Session::get('last_updated_at') != Auth::user()->updated_at) {
        Session::put('user_menu', generateMenusContent());
        Session::put('last_updated_at', Auth::user()->updated_at);
    }

    $prefixUri = Request::segment(1);

    if (is_null($prefixUri)) {
        $prefixUri = 'home';
    }
@endphp

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">

    {{--{{ dd(Session::get('user_menu')) }}--}}
    @foreach(Session::get('user_menu') as $menus)
        @if (!empty($menus['sub_menu']))
            <li class="treeview @if (!is_null(checkBelongsToMenusActive($prefixUri,$menus['sub_menu']))) active @endif">
                <a href="#">
                    @if($menus['icon'] != '') <i class="{{ $menus['icon'] }}"></i> @endif
                    <span>{{ $menus['menu_name'] }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach ($menus['sub_menu'] as $sub_menus)
                        <li class="{{ ($prefixUri == $sub_menus['route_prefix']) ? "active" : "" }}">
                            <a href="{{ route($sub_menus['route_name']) }}">
                                <i class="fa fa-angle-double-righ"></i>
                                <span>{{$sub_menus['menu_name']}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @else
            <li class="{{ ($prefixUri == $menus['route_prefix']) ? "active" : "" }}">
                <a href="{{ ($menus['route_name'] != "") ? route($menus['route_name']) : "#" }}">
                    @if($menus['icon'] != '') <i class="{{ $menus['icon'] }}"></i> @endif
                    <span>{{ $menus['menu_name'] }}</span>
                </a>
            </li>
        @endif
    @endforeach

</ul>
