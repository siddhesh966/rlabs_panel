<section class="content-footer">
    <div class="row">
        <p class="clearfix blue-grey lighten-2 mb-0 px-5">
                        <span class="float-md-left">Copyright  © {{ date('Y') }}
                            <a class="text-bold-800 grey darken-2"
                               href="{{ config('app_config.app_company_website') }}"
                               @if(config('app_config.app_company_website') != '#')
                               target="_blank"
                               @endif
                            >{{ config('app_config.app_company') }} </a>, All rights reserved.
                        </span>
            <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted &amp; Made with
                            <a class="text-bold-800 grey darken-2"
                               href="{{ config('app_config.powered_by_website') }}"
                               @if(config('app_config.powered_by_website') != '#')
                               target="_blank"
                               @endif
                               >{{ config('app_config.powered_by') }} </a>
                        </span>
        </p>
    </div>
</section>
