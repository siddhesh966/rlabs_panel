<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="{{ route('home.index') }}" class="logo">
        {{ config('app_config.app_name') }}
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>{{ Auth::user()->name }} <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="{{ asset('img/admin.png') }}" class="img-circle" alt="User Image"/>
                            <p>
                                {{ Auth::user()->name }}
                                <small>Member since {{ date('M. Y', strtotime(Auth::user()->created_at)) }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('password.change') }}"
                                   class="btn btn-default btn-flat">Change Password</a>
                            </div>
                            {{--<div class="pull-right">--}}
                            {{--<a href="#" class="btn btn-default btn-flat">Sign out</a>--}}
                            {{--</div>--}}
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('logout') }}" onclick="return confirm('Are you want to logout from application?');" title="User Logout">
                        <i class="fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
