@php

    $mainPath = Request::segment(1);

    $page_title = 'Service';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->services_id]) }}" method="post"
                                  data-parsley-validate enctype="multipart/form-data">

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="services_title">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="services_title" name="services_title"
                                               value="{{ $detail->services_title }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Service title field is required."
                                               placeholder="Enter service title">
                                    </div>

                                    <div class="form-group">
                                        <label for="services_description">Description
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control" cols="10" rows="10"
                                                  name="services_description" id="services_description"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="Service description field is required.">{{ $detail->services_description }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="services_image">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control" accept="image/*"
                                               id="services_image" name="services_image"
                                                {{--data-parsley-required="true" --}}
                                                {{--data-parsley-required-message="Service image field is required."--}}
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Service Image Preview
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <div>
                                            <img src="{{ $detail->services_image }}" alt="service image"
                                                 style="width: 200px;height: auto;">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="display_order">Display Order
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="display_order" name="display_order"
                                               value="{{ $detail->display_order }}"
                                               data-parsley-required="true"
                                               data-parsley-type="digits"
                                               data-parsley-required-message="Service display order field is required."
                                               placeholder="Enter service display order">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
