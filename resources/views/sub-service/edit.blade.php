@php

    $mainPath = Request::segment(1);

    $page_title = 'Sub-Service';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraScripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->sub_service_id]) }}" method="post"
                                  data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="services_id">Service
                                            <span class="red">*</span>
                                        </label>
                                        <select class="form-control" name="services_id" id="services_id"
                                                data-parsley-required="true"
                                                data-parsley-required-message="Service field is required"
                                        >

                                            <option value="">select service option</option>
                                            @foreach($services as $service)
                                                <option value="{{ $service->services_id }}"
                                                        @if ($service->services_id == $detail->services_id) selected @endif>{{ $service->services_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="sub_services_title">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="sub_services_title" name="sub_services_title"
                                               value="{{ $detail->sub_services_title }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Sub-Service title field is required."
                                               placeholder="Enter sub-service title">
                                    </div>

                                    <div class="form-group">
                                        <label for="sub_services_description">Description
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control" cols="10" rows="3"
                                                  name="sub_services_description" id="sub_services_description"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="Sub-Service description field is required.">{{ $detail->sub_services_description }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="sub_services_image">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control" accept="image/*"
                                               id="sub_services_image" name="sub_services_image"
                                                {{--data-parsley-required="true" --}}
                                                {{--data-parsley-required-message="Service image field is required."--}}
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Sub-Service Image Preview
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <div>
                                            <img src="{{ $detail->sub_services_image }}" alt="sub-service image"
                                                 style="width: 200px;height: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
