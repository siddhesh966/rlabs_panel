<!--====== BRAND PART START ======-->

<section id="brand-part" class="pt-0 pb-30">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2>WHOM WE SERVE</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="brand-slied owl-carousel mt-45">
                @forelse($weserves as $key => $serve)
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ $serve->image_path }}" alt="Brand">
                        <p class="w-100 text-center">{{ $serve->title ?? '' }}</p>
                    </div>
                </div>
                {{--<div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/government-bodies.png')}}" alt="Brand">
                        <p class="w-100 text-center">Government Bodies</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/household-society.png')}}" alt="Brand">
                        <p class="w-100 text-center">Households & Societies</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/industries-private-organizations.png')}}" alt="Brand">
                        <p class="w-100 text-center">Industries & Private Organizations</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/hotels-restaurants.png')}}" alt="Brand">
                        <p class="w-100 text-center">Hotels & Restaurants</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/export-import.png')}}" alt="Brand">
                        <p class="w-100 text-center">Export & Import</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="singel-brand">
                        <img src="{{ asset('assets/images/patnarlogo/canteen.png')}}" alt="Brand">
                        <p class="w-100 text-center">Canteen & Caterings</p>
                    </div>
                </div>--}}
                @empty
                    No Testimonials Found...!
                @endforelse
            </div>
        </div>
    </div>
</section>

<!--====== BRAND PART ENDS ======-->
