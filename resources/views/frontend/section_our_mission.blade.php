<!--====== Our MISSION START ======-->

<section id="about-part" class="pt-60 pb-60 mb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="section-title text-center pb-30">
                    <h2>Our MISSION</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="mission-content p-4 pt-5 pb-4">
                    <img class="mb-4" src="{{ asset('assets/images/client-sat-icon-hover.png')}}" alt="img">
                    <p>To continually enhance the level of client satisfaction</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mission-content p-4 pt-5 pb-4">
                    <img class="mb-4" src="{{ asset('assets/images/client-port-icon-hover.png')}}" alt="img">
                    <p>To offer the most technologically advanced portfolio of services in the industry</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mission-content p-4 pt-5 pb-4">
                    <img class="mb-4" src="{{ asset('assets/images/client-internal-icon-hover.png')}}" alt="img">
                    <p>To expand our client base in India and International markets</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mission-content p-4 pt-5 pb-4">
                    <img class="mb-4" src="{{ asset('assets/images/client-recognized-icon-hover.png')}}" alt="img">
                    <p>To be recognized as a great place to work and grow professionally</p>
                </div>
            </div>

            <!-- <div class="col-lg-6 text-right mt-45 mb-45">
                <div class="our-mission-round-img">
                    <div class="our-mission-img">
                        <img class="" src="images/client-satisfaction.png" alt="img">
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-lg-6">
                <div class="about-content">
                    <div class="client-stsfacn">
                        <ul>
                            <li><img class="" src="images/client-sat-icon.png" alt="img">
                                <p>To continually enhance the level of <br>client satisfaction</p>
                            </li>
                            <li class="align-cli1"><img class="" src="images/client-port-icon.png" alt="img">
                                <p>To offer the most technologically advanced <br>portfolio of services in the industry
                                </p>
                            </li>
                            <li class="align-cli2"><img class="" src="images/client-internal-icon.png" alt="img">
                                <p>To expand our client base in India and <br>International markets</p></li>
                            <li><img class="" src="images/client-recognized-icon.png" alt="img"><p>To be recognized as a great
                                place to work <br>and grow professionally</p></li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>

<!--====== Our MISSION ENDS ======-->
