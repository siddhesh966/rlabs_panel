@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Cart';
    $totalPrice = 0; $subTotal = 0; $gst = 0; $finalTotal = 0;

    $cartPriceDetails = calculateCartPriceDetails($cart);

    $checkoutRoute = $mainUri.'.checkout';
    $isLogin = 1;
    if(empty($userMasterSession)){
        $checkoutRoute = $mainUri.'.login';
        $isLogin = 0;
    }

@endphp

@extends('frontend.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    @if(!empty($cart))

        <!-- Product start -->
        <div class="container ptb-100 pt-5 mt-4">
            <div class="row pb-3">
                <div style="overflow: auto">
                    <div class="product-list">
                        <div class="product-header rounded-top">
                            <div style="width: 15%;text-align:center">#</div>
                            <div style="width: 30%;text-align:left">Service</div>
                            <div style="width: 20%;text-align:center">Price</div>
{{--                            <div style="width: 10%;text-align:center">Quantity</div>--}}
                            <div style="width: 15%;text-align:center">Total</div>
                            <div style="width: 10%;text-align:center"></div>
                        </div>

                        @foreach($cart as $key => $singleService)

                            <div class="pro product-contain border-top-0" id="main_row_{{ $key }}">
                                <div class="d-flex-wrap" style="width: 15%">
                                    <img style="width:60%" src="{{ $singleService['sub_services_image'] ?? '' }}">
                                </div>
                                <div class="d-flex-wrap" style="width: 30%">
                                    <a class="cleaning-title" href="javascript:void(0);">{{ $singleService['sub_services_title'] ?? '' }}</a>
                                    <div class="cleaning-details">
                                        <ul>
                                            <li>House Type: <span>{{ $singleService['services_type_name'] ?? '' }}</span></li>
{{--                                            <li>Service Type:--}}
{{--                                                @if($singleService['service_category'] == 1)--}}
{{--                                                    <span>Single Service</span>--}}
{{--                                                @elseif($singleService['service_category'] == 2)--}}
{{--                                                    <span>4 Services Per Year</span>--}}
{{--                                                @elseif($singleService['service_category'] == 3)--}}
{{--                                                    <span>12 Services Per Year</span>--}}
{{--                                                @else--}}
{{--                                                    ---}}
{{--                                                @endif--}}
{{--                                            </li>--}}
                                            <li>Service Date:<span>{{ date("d-m-Y", strtotime($singleService['service_date'])) }}</span></li>
                                            <li>Service Time : <span>{{ $singleService['service_time'] ?? '' }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="d-flex-wrap" style="width: 20%">₹{{ $singleService['services_price'] ?? '' }}</div>
{{--                                <div class="d-flex-wrap" style="width: 10%">--}}
{{--                                    <input style="width:60px;text-indent:5px" type="number" min="1" oninput="validity.valid||(value=''); return UpdateCart(this.value, {{ $key }});"--}}
{{--                                           id="quantity_{{$key}}" name="quantity_{{$key}}"--}}
{{--                                           onchange="return UpdateCart(this.value, {{ $key }});"--}}
{{--                                           onkeyup="return UpdateCart(this.value, {{ $key }});"--}}
{{--                                           value="{{ $singleService['service_quantity'] ?? '' }}" />--}}
{{--                                </div>--}}
                                <div class="d-flex-wrap" style="width: 15%" id="totalPrice{{$key}}">
                                    ₹{{ $totalPrice = $singleService['services_price'] * $singleService['service_quantity'] }}
                                </div>
                                <div class="d-flex-wrap" style="width: 10%">
                                    <button class="btn" onclick="return showConfirmMessage('You will not be able to recover this record!',
                                        'warning', 'Yes, delete it!', 'Success!', 'Record has been deleted.',
                                        'Error!', 'Unable to delete record.', '{{ route($mainUri.'.delete_cart', [$key]) }}',
                                        '.page-loader-wrapper', 'GET');">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>

                        @endforeach

{{--                        <div class="product-contain rounded-bottom pl-4 border-top-0">--}}
{{--                            <input class="coupon-input" type="text" placeholder="Coupon Code">--}}
{{--                            <button style="font-size:14px" class="btn">Apply Coupon</button>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- Product end -->

        <!-- Product start -->
        <div class="container ptb-100 pt-0">
            <div class="row pb-3">
                <div class="col-lg-6">
                    <h5>Cart Total</h5>
                    <ul class="list-group mt-3">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Subtotal
                            <span class="badge badge-light badge-pill" style="font-size: 14px" id="subTotal">₹{{ $cartPriceDetails['subTotal'] }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            GST 18% (estimated for India)
                            <span class="badge badge-light badge-pill" style="font-size: 14px" id="gst">₹{{ $cartPriceDetails['gst'] }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Total
                            <span class="badge badge-light badge-pill" style="font-size: 16px" id="finalTotal">₹{{ $cartPriceDetails['finalTotal'] }}</span>
                        </li>
                    </ul>
{{--                    <a href="#" style="font-size:14px;" class="btn mt-4" onclick="return toCheckout('{{ route($loginRoute) }}');">Proceed To Checkout</a>--}}
                    <button style="font-size:14px" class="btn mt-4" onclick="return toCheckout('{{ route($checkoutRoute) }}', {{ $isLogin }});">Proceed To Checkout</button>
                </div>
            </div>
        </div>
        <!-- Product end -->
    @else
        Cart is empty...!
    @endif

    {{-- Slogan File --}}
    @include('frontend.section_slogan')

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <script>
        function toCheckout(routeName, isLogin)
        {
            if(!isLogin){
                showMessage('', 'You must login to continue with checkout.', '', '', routeName);
            }else{
                location.href = routeName;
            }
        }

        function UpdateCart(quantity, key)
        {
            if(!quantity)
            {
                $("#quantity_"+key).val(1);
                quantity = 1;
            }

            let route = "{{ route($mainUri.'.update_cart') }}";
            route = route + '?quantity=' + quantity + '&key=' + key;

            ajaxFetch(route, "", "", "", ".preloader", true, "get", true,
                function (response) {
                    if(response.code == 200){
                        if(response.type == 'error'){
                            toastr.error(response.message, 'Error', toastr.options);
                        }else if(response.type == 'success'){
                            toastr.success(response.message, 'Success', toastr.options);
                            $("#totalPrice"+key).text("₹"+response.data.totalPrice);
                            $("#subTotal").text("₹"+response.data.subTotal);
                            $("#gst").text("₹"+response.data.gst);
                            $("#finalTotal").text("₹"+response.data.finalTotal);
                        }
                    }else{
                        toastr.error('Something went wrong...!', 'Error', toastr.options);
                    }
                    $('.preloader').hide();
                });
        }
    </script>
@endsection
