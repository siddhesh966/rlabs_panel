<!--====== BLOG PART START ======-->

<section id="blog-part" class="pt-30 pb-30">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center pb-15">
                    <h2>Our ARTICLES</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="blog-slied owl-carousel">
                @forelse($blogs as $key => $blog)
                <div class="col-lg-12">
                    <div class="singel-blog mt-30">
                        <div class="blog-thum">
                            <img src="{{ $blog->blogImage}}" alt="Blog">
                                <div class="date text-center">
                                {{--<h3>22</h3>--}}
                                <span>{{ date("F d, Y", strtotime($blog->created_at)) }}</span>
                            </div>
                        </div>
                        <div class="blog-cont pt-25">
                            <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}"><h5>{{ $blog->blogTitle ?? '' }}</h5></a>
                            <p class="text-justify">{{ $blog->blogSubTitle ?? '' }}</p>
                            <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}">Read More</a>
                        </div>
                    </div>
                </div>
                @empty
                    No Blogs Found...!
                @endforelse
            </div>
        </div>
    </div>
</section>

<!--====== BLOG PART ENDS ======-->
