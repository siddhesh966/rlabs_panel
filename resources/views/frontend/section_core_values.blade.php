<!--====== Core values START ======-->

<section id="" class="delivery-part-2 pb-30">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <h2>Core values</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <p>At RL we believe our values lie at the core of the way we do our business. They enable us to
                        create a distinct identity for ourselves, in front of all our stakeholders. Through our
                        values of Innovation, Achievement Orientation, Customer Focus, Care and Team Work we create
                        a culture for the organization where new ideas thrive, teams focus and persevere with high
                        energy & execution, customers come first and care and team work form organizational pillars.
                    </p>
                </div>
            </div>
            <div class="col-lg-12 mt-4 text-center">
                <img class="infographic" src="{{ asset('assets/images/infographic.png')}}" alt="img">
            </div>
            <!-- <div class="col-lg-12 mb-45">
                <b class="text-white">Innovation: Explore new ways to build on opportunities and solve problems. Thinking like an entrepreneur and creating new possibilities</b>
                <div class="client-stsfacn2">
                    <ul>
                        <li class="text-white">We think and act like owners</li>
                        <li class="text-white">We find solutions to every challenge</li>
                        <li class="text-white">We believe in doing things in new and simpler ways</li>
                    </ul>
                   </div>
            </div> -->


            <!-- <div class="col-lg-12 mb-45">
                <b class="text-white">Achievement Orientation: High levels of drive and planning to realize stretch outcomes while being excited about possibilities, unfazed by challenges and raising the bar for self and others																	</b>
                <div class="client-stsfacn2">
                    <ul>
                        <li class="text-white">We believe in growth, speed and energy to deliver results</li>
                        <li class="text-white">We plan what we do and do what we plan</li>
                    </ul>
                   </div>
            </div> -->


            <!-- <div class="col-lg-12 mb-45">
                <b class="text-white">Customer Focus: Building a deep, and trusted equation with customers and creating long term value for customers</b>
                <div class="client-stsfacn2">
                    <ul>
                        <li class="text-white">We build long lasting customer relationships</li>
                    </ul>
                   </div>
            </div> -->


            <!-- <div class="col-lg-12 mb-45">
                <b class="text-white">Care: Demonstrating care, appreciation and support to each other, thereby creating a nurturing environment that brings out the best in people													</b>
                <div class="client-stsfacn2">
                    <ul>
                        <li class="text-white">We help our team members and peers to grow</li>
                        <li class="text-white">We act with Integrity in whatever we do</li>
                    </ul>
                   </div>
            </div> -->


            <!-- <div class="col-lg-12 mb-45">
                <b class="text-white">Team Work: Inclusive, supportive and engaging with people across the organization to deliver the best solutions.</b>
                <div class="client-stsfacn2">
                    <ul>
                        <li class="text-white">We trust and support each other as together we can win more</li>

                    </ul>
                   </div>
            </div> -->


        </div>
    </div>

</section>

<!--====== Core values ENDS ======-->
