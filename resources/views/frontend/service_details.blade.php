@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Service Details';
@endphp

@extends('frontend.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')


    @if(!is_null($sub_services))

        <!--====== PAGE BANNER PART START ======-->

        <section id="page-banner" class="pt-200 pb-150 bg_cover" style="background-image: url({{ $sub_services->sub_services_image ?? '' }}">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-content">
                            <h3>Food Testing</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->


        <!-- Start Services Details Area -->
        <section class="services-details-section ptb-100">
            <div class="container form-wrapper mb-0">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="main-img">
                            <img src="{{ $sub_services->sub_services_image ?? '' }}" alt="{{ $sub_services->sub_services_title ?? 'No Preview Available' }}">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-8">
                        <div class="estimate-section">
                            <div class="container">
                                <form role="form" id="formAddToCart" name="formAddToCart" method="post" autocomplete="off"
                                      action="{{ route($mainUri.'.add_to_cart') }}" enctype="multipart/form-data">

                                    @csrf

                                    <input type="hidden" name="sub_service_id" id="sub_service_id" value="{{ $sub_services->sub_service_id ?? '0' }}" />

                                    <div class="mb-0">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>House Type <span style="color:red">*</span></label>
                                                    <select class="form-control" id="service_type" name="service_type">
                                                        <option value="">Select</option>
                                                        @foreach($services_area_prices as $services_area_price)
                                                            <option value="{{ $services_area_price->services_type_id }}">
                                                                {{ $services_area_price->services_type . " ( Rs. " . $services_area_price->services_price . " )"  }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

{{--                                            <div class="col-lg-6 col-md-6">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Service Type <span style="color:red">*</span></label>--}}
{{--                                                    <select class="form-control" id="service_category" name="service_category">--}}
{{--                                                        <option value="">Select</option>--}}
{{--                                                        <option value="1">Single Service</option>--}}
{{--                                                        <option value="2">4 Services Per Year</option>--}}
{{--                                                        <option value="3">12 Services Per Year</option>--}}
{{--                                                    </select>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <input type="hidden" id="service_category" name="service_category" value="1" /> {{--Single Service--}}

                                            <div class="col-lg-6 col-md-6">
                                                <label>Enter Date <span style="color:red">*</span></label>
                                                <div class="form-group">
                                                    <input type="date" class="form-control" id="service_date" name="service_date" />
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Choose Time <span style="color:red">*</span></label>
                                                    <select class="form-control" id="service_time" name="service_time">
                                                        <option value="">Select</option>
                                                        <option value="10:00 am to 11:00 am">10:00 am to 11:00 am</option>
                                                        <option value="12:00 pm to 1:00 pm">12:00 pm to 1:00 pm</option>
                                                        <option value="2:00 pm to 3:00 pm">2:00 pm to 3:00 pm</option>
                                                        <option value="3:00 pm to 4:00 pm">3:00 pm to 4:00 pm</option>
                                                    </select>
                                                </div>
                                            </div>

{{--                                            <div class="col-lg-6 col-md-6">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <input type="text" class="form-control" id="service_quantity" name="service_quantity" placeholder="Enter Quantity">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <input type="hidden" id="service_quantity" name="service_quantity" value="1" />

                                            <div class="col-lg-6 col-md-6">
{{--                                                <a href="cart.php"><button type="button" class="default-btn">Add To Cart</button></a>--}}
                                                <button name="action" id="btnAddToCart" value="save" type="submit" class="default-btn">Add To Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End Services Details Area -->

        <!-- Start Services Additional Details Area -->
        <section class="services-details-section ptb-100 pt-0">
            <div class="container form-wrapper mb-0">
                <div class="row">
                    <!-- Description start -->
                    <div class="container">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu1">Additional Information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu2">Reviews</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div id="home" class="container tab-pane active"><br>
                                {!! $sub_services->sub_services_description ?? '' !!}
                            </div>

                            <div id="menu1" class="container tab-pane fade"><br>
                                <table class="shop_attributes">
                                    <tbody>
                                    <tr class="">
                                        <th>House Type</th>
                                        <td>
                                            <p><b> : </b>{{ implode(", ", $arrServiceType) }}</p>
                                        </td>
                                    </tr>
                                    <tr class="alt">
                                        <th>Service Type</th>
                                        <td>
                                            <p><b> : </b>Single Service, 4 Services Per Year, 12 Services Per Year</p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div id="menu2" class="container tab-pane fade"><br>

                            </div>

                        </div>
                    </div>
                    <!-- Description end -->

                </div>
            </div>

        </section>
        <!-- End Services Additional Details Area -->

        {{-- Related Services File --}}
        @include('frontend.section_related_services')

    @else
        No Details Found...!
    @endif

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>

    <script>
        $("#formAddToCart").validate({
            rules: {
                service_type: "required",
                service_category: "required",
                service_date: "required",
                service_time: "required",
                service_quantity: {
                    required: true,
                    number: true,
                    integer: true,
                    minStrict: 0,
                },
            },
            messages: {
            },
            submitHandler: function(form) {
                ajaxFetch("{{ route($mainUri.'.add_to_cart') }}", "#formAddToCart", "", "", ".preloader", true, "post", true,
                    function (response) {
                        if(response.code == 200){
                            if(response.type == 'error'){
                                toastr.error(response.message, 'Error', toastr.options);
                            }else if(response.type == 'success'){
                                toastr.success(response.message, 'Success', toastr.options);
                                $("#service_type").val("");
                                $("#service_category").val("");
                                $("#service_date").val("");
                                $("#service_time").val("");
                                $("#service_quantity").val("");
                                $("#cart_counter").text(response.data.cart_counter);
                            }
                        }else{
                            toastr.error('Something went wrong...!', 'Error', toastr.options);
                        }
                        $('.preloader').hide();
                    });
            }
        });

        $.validator.addMethod("minStrict",
            function(value, element, params) {
                return value > params;
            },'Please Enter A Value Greater Than 0.');
    </script>
@endsection
