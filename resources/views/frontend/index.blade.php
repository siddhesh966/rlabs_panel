@php
    $mainUri = Request::segment(1);
    if($mainUri == ""){ $mainUri = "website"; }
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!-- Start Banner -->
    <section id="slider-part" class="slider-part-2">
        <div class="content-slied">
            @forelse($banners as $key => $banner)
            <div class="single-slider d-flex align-items-center bg_cover" style="background-image: url({{ $banner->banner_image_path ?? '' }}">
            {{--<div class="single-slider d-flex align-items-center bg_cover" style="background-image: url({{ asset('assets/images/bg/food.jpg)')}}">--}}
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-8 col-lg-7 col-md-11">
                            <div class="slider-content text-center">
                                <h2 data-animation="fadeInUp" data-delay="1s">{{ $banner->banner_title ?? '' }}</h2>
                                <p class="text-center" data-animation="fadeInUp" data-delay="1.5s">{{$banner->banner_description ?? ''}}</p>
                                <a href="services.php">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
                No Found...!
            @endforelse
            {{--<div class="single-slider d-flex align-items-center bg_cover" style="background-image: url({{ asset('assets/images/bg/water.jpg)')}}">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-8 col-lg-7 col-md-11">
                            <div class="slider-content">
                                <h2 data-animation="fadeInUp" data-delay="1s">Water Testing</h2>
                                <p data-animation="fadeInUp" data-delay="1.5s">Worried about quality of the Water you use for personal or Industrial use? Let Radiant take
                                    care of all your Water Quality issues. We are capable of analysing water for its every
                                    application.</p>
                                <a href="services.php">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider d-flex align-items-center bg_cover" style="background-image: url({{ asset('assets/images/bg/soil.jpg)')}}">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-8 col-lg-7 col-md-11">
                            <div class="slider-content">
                                <h2 data-animation="fadeInUp" data-delay="1s">Soil Testing</h2>
                                <p data-animation="fadeInUp" data-delay="1.5s">Get your Soil analyzed for Quality parameters at Radiant and obtain valuable insights about
                                    the fertilizers to be used and which crops to take in which part of the year thus improving your yield for the crop.</p>
                                <a href="services.php">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </section>
    <!-- Start Banner -->

    <!--====== RUNNING SCRIPT PART START ======-->
    <section style="position: relative;">
        <div id="discover-more"></div>
        <div class="container">
            <div class="row example-result ">
                <div class="col-sm-12 col-md-7 col-lg-8">
                    <div class="as-cta-desc" >
                        <h3 class="font-weight-normal text-white pl-5">To see your history of Analysis with us OR To check The Authenticity of your report</h3>
                        {{--<p class="mb-2 mt-2 text-white">To see your history of Analysis with us OR To check The Authenticity of your report</p>--}}
                    </div>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-4 align-self-center">
                    <div class="as-cta-btn text-center">
                        <a href="javascript:void(0)" class="btn btn-custom btn-lg" data-toggle="modal" data-target="#auth-login">CLICK TO LOGIN</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== RUNNING SCRIPT PART ENDS ======-->

    <!--====== ABOUT PART START ======-->

    <section id="about-part" class="pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="about-content mt-30 text-justify">
                        <h3><span>Welcome to</span> Radiant Laboratories…</h3>
                        <div class="ul-li-style2">
                            <ul>
                                <li>Radiant Laboratoriers started its operations in the year 2000 in Nagpur with a vision to offer a one stop solution to the Analytical Industry. Over the last 2 decades the company has executed various projects spread across India. The company is currently operational from Pune however it is now having a pan India presence based on variety of activities taken across under various fields.</li>
                                <li>These activities are carried out for Clients, Industries, Government Agencies and other Organizations.</li>
                                <li>The quality system of Radiant Laboratories is designed to comply with various National and International standards so as to deliver results with complete traceability, transparency and confidentiality.</li>
                                <li>RL makes use of customized LIMS in all its Analytical Laboratory Divisions to automate & control the key laboratory operations. The benefits of LIMS include:</li>
                                <li>Registration of samples digitally, Tracking of progress during analysis, Direct data capture from instrumentation, calculations & validation of results, Minimize transcription and double handling, Generate analytical and management reports</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="squer-img owl-carousel mt-45">
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/1.jpg')}}" alt="welcome">
                                <div class="caption">Analytical Section</div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/5.jpg')}}" alt="welcome">
                                <div class="caption">Mobile Soil Lab 2</div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/3.jpg')}}" alt="welcome">
                                <div class="caption">Outer View</div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/4.jpg')}}" alt="welcome">
                                <div class="caption">Mobile Soil Lab 1</div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/2.jpg')}}" alt="welcome">
                                <div class="caption">Lobby</div>
                            </div>
                        </div>
                        {{--<div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/6.jpg')}}" alt="welcome">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="singel-brand1">
                                <img src="{{ asset('assets/images/welcome/7.jpg')}}" alt="welcome">
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== ABOUT PART ENDS ======-->

    <!--====== service PART START ======-->

    <section id="services" class="pt-30 pb-30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center">
                        <h2>What We Do</h2>
                        <ul>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center mt-4">
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse service-bg-food bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Food Testing</h3>
                                <p class="card-text mb-3">With the consumer awareness being increased and Regulatory bodies getting stringent, Food Quality Monitoring has a key role to play...</p>
                                <a href="services-food.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse bg-inverse service-bg-water bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Water Testing</h3>
                                <p class="card-text mb-3">If you go with the figures, number of people dying because of drinking water are comparable to those dying because of water scarcity...</p>
                                <a href="services-water.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse bg-inverse service-bg-soil bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Soil Testing</h3>
                                <p class="card-text mb-3">It is estimated that in order to feed the world by 2050, the agricultural output has to be doubled with the same amount of water...</p>
                                <a href="services-soil.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse bg-inverse service-bg-build bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Building Materials</h3>
                                <p class="card-text mb-3">Construction activities can range from building of roads, highways, buildings, towers, dams. Foundation of any construction work...</p>
                                <a href="services-building-material.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse bg-inverse service-bg-audit bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Audit Inspection</h3>
                                <p class="card-text mb-3">Audits and Inspection help either to identify the level of compliance as per the laid standards or to obtain insights for process...</p>
                                <a href="services-audit-and-inspection.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mt-4">
                    <div class="card card-inverse bg-inverse service-bg-consult bg_cover">
                        <div class="bg-trans p-4">
                            <div class="card-block text-center">
                                <h3 class="card-title">Consultancy</h3>
                                <p class="card-text mb-3">We at Radiant firmly believe the old saying that there is a solution to every problem. Our team of experts would be more than happy to...</p>
                                <a href="services-consultancy.php" class="btn btn-custom btn-sm">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== service PART ENDS ======-->

    {{-- Counter File --}}
    @include('frontend.section_counter')

    {{-- Whom we serve File --}}
    @include('frontend.section_whom_we_serve')

    {{-- Whom we serve File --}}
    @include('frontend.section_why_choose_us')

    {{-- Testimonial File --}}
    @include('frontend.section_testimonial')

    {{-- Blog File --}}
    @include('frontend.section_blog')

    {{-- Contact-Us Form File --}}
    {{--@include('frontend.section_contact_us')--}}

    {{-- Cost Estimate File --}}
    {{-- @include('frontend.section_cost_estimate') --}}

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
