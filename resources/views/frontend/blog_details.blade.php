@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Article Details';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

<!--====== PREALOADER PART START ======-->

<div class="preloader">
    <div class="thecube">
        <div class="cube c1"></div>
        <div class="cube c2"></div>
        <div class="cube c4"></div>
        <div class="cube c3"></div>
    </div>
</div>

<!--====== PREALOADER PART START ======-->

@section('content')

    @if(!is_null($blog))
        <!-- Start Blog Details Area -->
        {{--<section class="blog-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="blog-details">
                            <div class="article-img">
                                <img src="{{ $blog->blogImage ?? '' }}" alt="No Preview Available">
                                <div class="date">{{ date("d", strtotime($blog->created_at)) }} <br> {{ date("F", strtotime($blog->created_at)) }}</div>
                            </div>
                            <div class="article-content">
                                <h3>{{ $blog->blogTitle ?? '' }}</h3>

                                {!! $blog->blogDesc ?? '' !!}

                                <div class="share-post">
                                    <ul>
                                        <li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="javascript:void(0);"><i class="fab fa-vimeo-v"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="post-controls-buttons">
                            @if(!is_null($previous_blog))
                                <div class="controls-left">
                                    <a href="{{ route($mainUri.'.blog_details', [$previous_blog->blog_id]) }}"><i class="fas fa-angle-double-left"></i> Previous Post</a>
                                </div>
                            @endif
                            @if(!is_null($next_blog))
                                <div class="controls-right">
                                    <a href="{{ route($mainUri.'.blog_details', [$next_blog->blog_id]) }}">Next Post <i class="fas fa-angle-double-right"></i></a>
                                </div>
                            @endif
                        </div>



                    </div>
                </div>
            </div>
        </section>--}}

        {{--<section id="blog-details-part" class="pt-30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-details">
                            <div class="blog-details-image pb-20">
                                <img src="{{ asset('assets/images/blog/bd-1.jpg')}}" alt="Blog Details">
                            </div>

                            <div class="blog-details-content text-justify">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>{{ $blog->blogTitle ?? '' }}</h4>
                                    </div>
                                </div>
                                <p class="text-justify">{!! $blog->blogDesc ?? '' !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}

        <section id="blog-details-part" class="pt-30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-details">


                            <div class="blog-details-content text-justify">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3 class="mb-1">{{ $blog->blogTitle ?? '' }}</h3>
                                        <p>January 20, 2020 by <a href="index.php">Radiant Laboratories</a></p>
                                    </div>
                                    <div class="blog-details-image pb-20">
                                        <img src="{{ asset('assets/images/blog/article1.jpg')}}" alt="Blog Details">
                                    </div>
                                    {{----}}
                                    {{ $blog->blogDesc ?? '' }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Blog Details Area -->
    @else
        No Details Found...!
    @endif

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
