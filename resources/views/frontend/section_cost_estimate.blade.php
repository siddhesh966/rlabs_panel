<!-- Start Cost Estimate Area -->
<div class="estimate-section ptb-100">
    <div class="container">
        <form>
            <div class="form-wrapper">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="form-group">
                            <label>Choose A Service</label>
                            <select class="form-control">
                                <option>Residential Cleaing</option>
                                <option>Car Cleaing</option>
                                <option>Home Cleaing</option>
                                <option>Floor Cleaing</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="form-group">
                            <label>Type of Clean</label>
                            <select class="form-control">
                                <option>full residential cleaning</option>
                                <option>full car cleaning</option>
                                <option>full home cleaning</option>
                                <option>full floor cleaning</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <label>Total Floor Area</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="id_number" placeholder="3453 sp ft">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <label>Your Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="id_name" placeholder="Enter your full name">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <label>ZIP Code</label>
                        <div class="form-group">
                            <input type="number" class="form-control" id="id_phone_number" placeholder="Zip code">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <label>Email Address</label>
                        <div class="form-group">
                            <input type="email" class="form-control" id="id_email" placeholder="Enter Your email address">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <button type="submit" class="default-btn">Get Cost Estimate</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Cost Estimate Area -->
