@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Contact';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    {{-- Contact-Us Form File --}}
    {{--@include('frontend.section_contact_us')--}}
    <section class="w-100 bg-white float-left" style="border-top:1px solid #f2f2f2">
        <div class="map">
            <div class="w-100">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d236.39172730116755!2d73.8028727!3d18.5619689!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x74299c9c071af672!2sRadiant%20Laboratories!5e0!3m2!1sen!2sin!4v1587470831274!5m2!1sen!2sin" width="100%" height="491" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
        <div class="c-form">
            <div class="contact-form p-lr-35">
                <h4 style="color:#2e4db8">Connect</h4>
                {{--<form id="formLeads" name="formLeads" action="{{ route($mainUri.'.save_leads') }}" autocomplete="off" data-toggle="validator">--}}
                    <form class="" name="formLeads" id="formLeads" method="post"
                          action="{{ route($mainUri.'.save_leads') }}" autocomplete="off">
                        <input type="hidden" name="category_name" value="Contact Us">
                    @csrf
                    <div class="">
                        <div class="w-100">
                            <div class="singel-form form-group">
                                <!-- <label>Full name :<span style="color: red;">*</span></label> -->
                                <input name="lead_name" id="lead_name" type="text" data-error="Name is required." placeholder="Full Name">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="w-100">
                            <div class="singel-form form-group">
                                <!-- <label>Email Address :<span style="color: red;">*</span></label> -->
                                <input type="email" name="lead_email" id="lead_email"
                                       data-error="Valid email is required." placeholder="Email Address">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="w-100">
                            <div class="singel-form form-group">
                                <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                <input type="text" name="lead_mobile" id="lead_mobile"
                                       data-error="Valid mobile no. is required." placeholder="Mobile Number">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="w-100">
                            <div class="singel-form form-group">
                                <!-- <label>Write a message :</label> -->
                                <textarea style="height:105px" name="lead_message" id="lead_message"
                                          data-error="Please,leave us a message." placeholder="Write a message"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <p class="form-message"></p>
                        <div class="w-100">
                            <div class="singel-form">
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
