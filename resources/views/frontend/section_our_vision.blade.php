<!--====== OUR VISION START ======-->

<section id="" class="delivery-part-2 bg_cover pt-60 pb-60 mt-30" data-overlay="8"
         style="background-image: url({{ asset('assets/images/banner-bg1.jpg)')}}">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-12">
                <div class="delivery-text section-title text-center">
                    <h2>Our Vision</h2>
                    <ul>
                        <li class="bg-white"></li>
                        <li class="bg-white"></li>
                        <li class="bg-white"></li>
                        <li class="bg-white"></li>
                        <li class="bg-white"></li>
                    </ul>
                    <p class="pb-0" style="font-size:36px;line-height:42px">To be recognized as the market leader in the Analytical Industry by being the most
                        competitive and most productive service provider to our customers</p>

                </div>
            </div>
        </div>
    </div>

</section>

<!--====== OUR VISION ENDS ======-->
