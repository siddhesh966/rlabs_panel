@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Lead Generation';
@endphp

@extends('frontend.app')

@section('extraStyles')

@endsection

@section('content')
    <style>
        #navi{
            display: none;
        }
    </style>
    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!--====== RUNNING SCRIPT PART START ======-->
    <section class="border-top">
        <div class="container">
            <div class="row pb-5 pt-5 justify-content-center">
                <!-- <h2 class="font-weight-normal text-white w-100 text-center">Lead Generation</h2> -->
                <div class="col-lg-8 border">
                    <img src="{{ asset('assets/images/lead-generation-img.jpg')}}" alt="welcome">
                </div>
                <div class="col-lg-4">
                    <div class="contact-form p-lr-35 example-result2 p-4">
                        <h4>Lead Generation Form</h4>
                        {{--<form id="contact-form" action="contact.php" data-toggle="validator">--}}
                        <form class="" name="formLeads" id="formLeads" method="post"
                              action="{{ route($mainUri.'.save_leads') }}" autocomplete="off">
                            <input type="hidden" name="category_name" value="Lead Generation">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="singel-form form-group">
                                        <!-- <label>Full name :<span style="color: red;">*</span></label> -->
                                        <input name="lead_name" id="lead_name" type="text" data-error="Name is required." placeholder="Enter Full Name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="singel-form form-group">
                                        <!-- <label>Email Address :<span style="color: red;">*</span></label> -->
                                        <input type="email" name="lead_email" id="lead_email" data-error="Valid email is required." placeholder="Email Address">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="singel-form form-group">
                                        <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                        <input type="text" name="lead_mobile" id="lead_mobile" data-error="Valid mobile no. is required." placeholder="Mobile Number">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="singel-form form-group">
                                        <!-- <label>Write a message :</label> -->
                                        <select id="lead_type" name="lead_type">
                                            <option value="" selected>Please Select</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Company">Company</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 compa" style="display:none">
                                    <div class="singel-form form-group">
                                        <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                        <input type="email" name="company" data-error="Valid Company is required." required="required" placeholder="Enter Company Name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 compa" style="display:none">
                                    <div class="singel-form form-group">
                                        <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                        <input type="email" name="designation" data-error="Valid designation is required." required="required" placeholder="Enter Designation">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="singel-form form-group">
                                        <!-- <label>Write a message :</label> -->
                                        <select id="services_id" name="services_id">
                                            <option value="" selected>Please Select Service</option>
                                            <option value="Food Testing">Food Testing</option>
                                            <option value="Water Testing">Water Testing</option>
                                            <option value="Soil Testing">Soil Testing</option>
                                            <option value="Building Materials">Building Materials</option>
                                            <option value="Audit Inspection">Audit Inspection</option>
                                            <option value="Consultancy">Consultancy</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 mb-3">
                                    <div class="singel-form form-group">
                                        <!-- <label>Write a message :</label> -->
                                        <textarea style="height: 100px;" name="lead_message" id="lead_message"
                                                  data-error="Please,leave us a message." placeholder="Write a message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="g-recaptcha" data-sitekey="6LfKURIUAAAAAO50vlwWZkyK_G2ywqE52NU7YO0S" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
                                        <input class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3 text-center">
                                    <button type="submit" class="btn btn-custom">SUBMIT</button></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--====== RUNNING SCRIPT PART ENDS ======-->

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
