@php
    $mainUri = Request::segment(1);
    if($mainUri == ""){ $mainUri = "website"; }
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
@endphp


<footer id="footer-part" class="footer-2">
    <div class="container ">
        <div class="footer pt-20 pb-45">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-about pt-30">
                        <a href="#"><img src="{{ asset('assets/images/logo-foter.svg')}}" alt="logo"></a>
                        <p>Donec vel ligula ornare, finibus ex at, vive risus. Aenean velit ex, finibus elementum eu, dignissim varius augue. </p>
                        <span><i class="fa fa-globe"></i>info@radiantlab.in</span>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Information</h5>
                    </div>
                    <div class="footer-info">
                        <ul>
                            <li><a href="contact.php">Contact Us</a></li>
                            <li><a href="about.php">Who We Are</a></li>
                            <li><a href="career.php">Career</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                            <li><a href="lead-generation.php">Lead Generation</a></li>
                            <li><a href="javascript:void(0)">Privacy Policy</a></li>
                            <!-- <li><a href="javascript:void(0)">Terms & Conditions</a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Our Services</h5>
                    </div>
                    <div class="footer-info">
                        <ul>
                            <li><a href="services-food.php">Food</a></li>
                            <li><a href="services-water.php">Water</a></li>
                            <li><a href="services-soil.php">Soil</a></li>
                            <li><a href="services-audit-and-inspection.php">Building Materials</a></li>
                            <li><a href="services-audit-and-inspection.php">Audit & Inspection</a></li>
                            <li><a href="services-consultancy.php">Consultancy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Recent News</h5>
                    </div>
                    <div class="footer-news">
                        <ul>
                            <li>
                                <div class="f-news">
                                    <h6 class="news-title"><a href="#">Nullam condimenum varius ipsum.</a></h6>
                                    <a class="news-date" href="#"><i class="fa fa-calendar"></i> 15 Aug 2019</a>
                                </div>
                            </li>
                            <li>
                                <div class="f-news">
                                    <h6 class="news-title"><a href="#">Nullam condimenum varius ipsum.</a></h6>
                                    <a class="news-date" href="#"><i class="fa fa-calendar"></i> 15 Aug 2019</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-title pt-30">
                        <h5>Get In Touch</h5>
                    </div>
                    <div class="footer-address">
                        <ul>
                            <li>
                                <div class="icon map-i">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="address">
                                    <h5>Radiant Laboratoriers</h5>
                                    <p>Murkuta Plaza, 2nd & 3rd Floor, Survey No.246/3/1/2,
                                        New DP Road,(Near Midipoint Hospital), Baner, Pune 411045, Maharashtra India
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-volume-control-phone"></i>
                                </div>
                                <div class="address">
                                    <p>+00123654789</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="address">
                                    <p>info@radiantlab.in</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright pt-15 pb-15">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="text-center text-sm-left">
                        <p>&copy; All Rights Reserved Radiant Lab 2020.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-right">
                    <p class="small">Powered by Appectual IT Solutions</p>
                    <!-- <div class="social-icon text-center text-sm-right">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

</footer>

<!--====== FOOTER PART ENDS ======-->

<!--====== BACK TOP TOP PART START ======-->

<a href="#" class="back-to-top">
    <!--<img src="images/back-to-top.png" alt="Icon">-->
    <i class="fa fa-angle-up text-white"></i>
</a>

<!--====== BACK TOP TOP PART ENDS ======-->
