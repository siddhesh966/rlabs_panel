<!--====== Counter PART START ======-->

<section id="exper-counter" class="mt-30">
    <div class="container pt-4 pb-4">
        <div class="row justify-content-center" id="counter">
            <div class="col-lg-10 col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="counter col_fourth text-center">
                            <!-- <i class="fa fa-book fa-2x"></i> -->
                            <img class="conter-img" src="{{ asset('assets/images/experience.png')}}" alt="experience">
                            <div class="love_counter ">
                                <div class="count-title">+<span id="yes"  class="counter-value timer" data-from="0" data-to="20" data-speed="2200" data-target="#yesyes">0</span></div>
                            </div>
                            <p class="count-text ">Years of experience</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="counter col_fourth text-center">
                            <!-- <i class="fa fa-flask fa-2x"></i> -->
                            <img class="conter-img" src="{{ asset('assets/images/analyzed.png')}}" alt="Samples analyzed">
                            <div class="love_counter ">
                                <div class="count-title ">+<span id="yes"  class="counter-value timer" data-from="0" data-to="500000" data-speed="2000" data-target="#yesyes"></span></div>
                            </div>
                            <p class="count-text ">Samples analyzed</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="counter col_fourth text-center">
                            <!-- <i class="fa fa-users fa-2x"></i> -->
                            <img class="conter-img" src="{{ asset('assets/images/customer.png')}}" alt="Happy customers">
                            <div class="love_counter ">
                                <div class="count-title ">+<span id="yes"  class="counter-value timer" data-from="0" data-to="1000" data-speed="2100" data-target="#yesyes"></span></div>
                            </div>
                            <p class="count-text ">Happy customers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== Counter SCRIPT PART ENDS ======-->

<!--====== ISO PDF PART START ======-->
<section id="iso-pdf">
    <div class="container pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12">
                <div class="row">
                    <div class="col-lg-3 mb-4">
                        <!-- Modal start-->
                        <div class="modal fade" id="iso-9001" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <button type="button" class="btn btn-custom btn-circle" data-dismiss="modal"><i class="fa fa-times"></i>
                                    </button>
                                    <div class="modal-body">
                                        <img class="rounded" src="{{ asset('assets/images/iso-certificates/9001.jpg')}}" alt="pdf">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal end-->
                        <div class="card" data-toggle="modal" data-target="#iso-9001">
                            <a class="w-100">
                                <div class="card-body text-center">
                                    <img class="rounded" src="{{ asset('assets/images/iso-certificates/9001.jpg')}}" alt="pdf" height="120" width="150">
                                    <p class="card-title2 mt-3 mb-0">ISO 9001 2015</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <!-- Modal start-->
                        <div class="modal fade" id="iso-14001" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <button type="button" class="btn btn-custom btn-circle" data-dismiss="modal"><i class="fa fa-times"></i>
                                    </button>
                                    <div class="modal-body">
                                        <img class="rounded" src="{{ asset('assets/images/iso-certificates/14001.jpeg')}}" alt="pdf">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal end-->
                        <div class="card" data-toggle="modal" data-target="#iso-14001">
                            <a class="w-100">
                                <div class="card-body text-center">
                                    <img class="rounded" src="{{ asset('assets/images/iso-certificates/14001.jpeg')}}" alt="pdf" height="120" width="150">
                                    <p class="card-title2 mt-3 mb-0">ISO 14001 2015</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <!-- Modal start-->
                        <div class="modal fade" id="iso-45001" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <button type="button" class="btn btn-custom btn-circle" data-dismiss="modal"><i class="fa fa-times"></i>
                                    </button>
                                    <div class="modal-body">
                                        <img class="rounded" src="{{ asset('assets/images/iso-certificates/45001.jpg')}}" alt="pdf">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal end-->
                        <div class="card" data-toggle="modal" data-target="#iso-45001">
                            <a class="w-100">
                                <div class="card-body text-center">
                                    <img class="rounded" src="{{ asset('assets/images/iso-certificates/45001.jpg')}}" alt="pdf" height="120" width="150">
                                    <p class="card-title2 mt-3 mb-0">ISO 45001 2018</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <!-- Modal start-->
                        <div class="modal fade" id="iso-27001" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <button type="button" class="btn btn-custom btn-circle" data-dismiss="modal"><i class="fa fa-times"></i>
                                    </button>
                                    <div class="modal-body">
                                        <img class="rounded" src="{{ asset('assets/images/iso-certificates/27001.jpeg')}}" alt="pdf">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal end-->
                        <div class="card" data-toggle="modal" data-target="#iso-27001">
                            <a class="w-100">
                                <div class="card-body text-center">
                                    <img class="rounded" src="{{ asset('assets/images/iso-certificates/27001.jpeg')}}" alt="pdf" height="120" width="150">
                                    <p class="card-title2 mt-3 mb-0">ISO 27001 2013</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== ISO PDF PART ENDS ======-->
