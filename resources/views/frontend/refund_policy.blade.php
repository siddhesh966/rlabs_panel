@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Refund Policy';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    <!-- Refund Policy start -->
    <div class="container ptb-100">
        <div class="row">
            <div class="col-lg-12 border p-5 rounded">
                <div class="points">
                    <ul>
                        <li>
                            Refund will be issued only if the client has cancelled a cleaning visit within the allowed time (24 hours) prior to the start of the cleaning session and a payment has been already taken KHFM Hospitality & Facility Management Services Ltd.
                        </li>
                        <li>
                            Refund will be issued if a cleaning operative does not attend a cleaning visit, payment for which has been already collected by KHFM Hospitality & Facility Management Services Ltd.
                        </li>
                        <li>
                            Refunds will be issued only be issued after KHFM Hospitality & Facility Management Services Ltd have been allowed to rectify any problems or complaints.
                        </li>
                        <li>
                            Refund Mode :- Online Transfer(NET BANKING, RTGS, NEFT)
                        </li>
                        <li>
                            Refund Duration :- 5 – 7 days
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Refund Policy end -->

    {{-- Slogan File --}}
    @include('frontend.section_slogan')

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
