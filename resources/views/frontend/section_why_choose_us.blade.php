<!--====== SERVICES PART START ======-->

<section id="services-part" class="services-part-2 pt-30">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <h2>Why Choose Us ?</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-4">
                <div class="singel-services mt-45 wcu-mr-r">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-1.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pl-70 text-right">
                        <h4 class="de-color">Depth of Experience</h4>
                        <p>100+ years of combined experience in different verticals of Analytical Industry</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>

                <div class="singel-services">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-3.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pl-70 text-right">
                        <h4 class="pqat-color">Premier Quality and Accurate Testing</h4>
                        <p>No compromise with Quality is our Mantra</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>

                <div class="singel-services wcu-mr-r">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-3.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pl-70 text-right">
                        <h4 class="cfsc-color">Customer Focussed Scientific Consultation</h4>
                        <p>Experts to help you, to provide the best solution
                            to any kind of technical or non-technical queries</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="singel-services mt-50 text-center">
                    <img src="{{ asset('assets/images/choose-us/why-choose-us.svg')}}" alt="Image">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="singel-services right mt-45 wcu-mr-l">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-2.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pr-70 text-left">
                        <h4 class="qtt-color">Quick Turnaround Time</h4>
                        <p>We strive for timely and precise reporting</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>

                <div class="singel-services right">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-4.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pr-70 text-left">
                        <h4 class="sat-color">State of the Art Technology</h4>
                        <p>Latest sophisticated & Modern equipment and world class facilities</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>

                <div class="singel-services right wcu-mr-l">
                    <!-- <div class="services-icon">
                        <img src="images/choose-us/icon-4.png" alt="Icon">
                    </div> -->
                    <div class="services-cont pt-25 pr-70 text-left">
                        <h4 class="uocp-color">Unique Online Customer Portal</h4>
                        <p>Have access to your Reports from anywhere & also check for its Authenticity</p>
                        <!-- <a href="#">Read More <span><i class="fa fa-long-arrow-right"></i></span></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== SERVICES PART ENDS ======-->

<!--====== CALL TO ACTION START ======-->

<section class="as-cta-full-gradient mt-60 mb-60 py-5 px-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-8">
                <div class="as-cta-desc">
                    <h2 class="font-weight-normal">Call to action</h2>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-4 align-self-center">
                <div class="as-cta-btn text-center">
                    <button data-toggle="modal" data-target="#exampleModal2" class="btn btn-custom btn-lg">CLICK ME PLEASE</button>
                </div>
            </div>
        </div>
    </div>
</section>
