@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Service';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <section id="page-banner" class="pt-200 pb-150 bg_cover" style="background-image: url({{ asset('assets/images/bg/what-we-do.jpg)')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-content">
                        <h3>What We Do</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== service PART START ======-->

    <section id="services" class="pt-30 pb-60">
        <div class="container">
            <div class="row justify-content-center mt-4">
                <div class="col-lg-8">
                    <div class="section-title text-center">
                        <h2>What We Do</h2>
                        <ul>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <p>Nunc molestie mi nunc, nec accumsan libero dignissim sit amet. Fusce sit amet tincidunt metus. Nunc eu risus  suscipit massa dapibus blandit. Vivamus ac commodo eros.</p>
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center mt-5">
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Food</h3>
                        <p class="card-text2 mb-3">With the consumer awareness being increased and Regulatory bodies getting
                            stringent, Food Quality Monitoring has a key role to play. Well, this is one of our fortes
                            which include general Food testing, Pesticide Residue Analysis, Allergens testing, Microbial
                            Analysis including pathogens, Toxin analysis, Veterinary Drugs, Shelf life studies and
                            Nutritional Labelling.</p>
                        <p class="card-text2 mb-3">Food Safety is a major health concern for Food Manufacturing, Retail & Hospitality Industry. Industrial Food Courts, Cafeterias & Canteens serving a large population pursue a policy of “Be Safe Than Sorry” with food quality & hygiene, which impact productivity and also aids in projecting good Corporate Image.</p>
                        <a href="services-food.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-food.jpg')}}">
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center mt-5 ser-col-rev">
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-water.jpg')}}">
                    </div>
                </div>
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Water</h3>
                        <p class="card-text2 mb-3">If you go with the figures, number of people dying because of drinking
                            water are comparable to those dying because of water scarcity. The reason is Contamination!
                            Hence you must be sure about the Quality of your water Radiant in its testing profile has got
                            covered Water for its every possible application and hence along with the analysis of the
                            parameters asked, our team of experts would be happy to offer you the solutions to your
                            queries.</p>
                        <p class="card-text2 mb-3">Pure and properly maintained water not only represents good environment but is essential for good health. Besides drinking, water is used for various industrial & recreational purposes and understanding the composition of water plays an important role in the such cases.Various types of waters analyzed at Radiant Laboratories include</p>
                        <a href="services-water.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center mt-5">
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Soil</h3>
                        <p class="card-text2 mb-3">It is estimated that in order to feed the world by 2050, the agricultural output
                            has to be doubled with the same amount of water and land available. Increasing the crop yield
                            is one major solution to achieve this. Soil analysis provides valuable inputs &amp; helps the
                            farmer, the Government bodies and other stakeholders to take appropriate decisions.</p>
                        <p class="card-text2 mb-3">We carry out Soil testing for agricultural purpose for more than 100 chemical parameters including the 12 parameters covered under Soil Health Card (SHC) Scheme. Soil Health Card is a Government of India’s scheme promoted by the Department of Agriculture & Co-operation under the Ministry of Agriculture and Farmers' Welfare. </p>
                        <a href="services-soil.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-soil.jpg')}}">
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center mt-5 ser-col-rev">
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-building.jpg')}}">
                    </div>
                </div>
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Building Materials</h3>
                        <div class="w-100 ul-li-style p-lr-35 t-center">
                            <p class="card-text2 mb-3">Construction activities can range from building of roads, highways,
                                buildings, towers, dams. Foundation of any construction work hinges on the use of Building
                                Materials which include cement, steel, concrete, aggregates, soil, fly ash, ceramics, bricks etc.
                                The quality of such materials used is of paramount importance in ensuring toughness and
                                durability of the constructed part, besides ensuring safety for all those people who would use
                                it. We at Radiant provide analysis of different such materials including the ones listed above.</p>
                            <p class="w-100 testing-subtitle">Construction &amp; Building Materials </p>
                            <ul class="mb-3">
                                <li>Functional Areas in Infrastructure sector</li>
                                <li>Feasibility studies for the construction of Building / Roads etc </li>
                                <li>Pre and Post construction survey activities</li>
                            </ul>
                        </div>
                        <a href="services-building-material.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center mt-5">
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Audit & Inspection</h3>
                        <p class="card-text2 mb-3">Audits and Inspection help either to identify the level of compliance as
                            per the laid standards or to obtain insights for process optimization, process improvement,
                            quality control and help in decision making. These help to establish the trust and gain
                            credibility. We at Radiant provide different types of Audit Services to various fields
                            including Water Audits, Energy Audit, Hygiene Rating, HACCP, Food safety Audits etc.</p>
                        <div class="w-100 text-justify ul-li-style p-lr-35 t-center">
                            <ul class="mb-3">
                                <li>Water Audits</li>
                                <li>Energy Audits</li>
                                <li>Hygiene Rating Audits</li>
                                <li>HACCP Audit</li>
                            </ul>
                        </div>
                        <a href="services-audit-and-inspection.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-audit.jpg')}}">
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center mt-5 ser-col-rev">
                <div class="col-lg-6">
                    <div class="ser-img">
                        <img src="{{ asset('assets/images/bg/ser-consultancy.jpg')}}">
                    </div>
                </div>
                <div class="col-lg-6 ser-flex-center mb-4">
                    <div class="card-block text-justify">
                        <h3 class="card-title2">Consultancy</h3>
                        <p class="card-text2 mb-3">We at Radiant firmly believe the old saying that there is a solution to every
                            problem. Our team of experts would be more than happy to assist our clients in different
                            usual and unique technical and non-technical issues that arise during the course of work for
                            which we offer range of Consultancy services in different streams.</p>
                        <div class="w-100 text-justify ul-li-style p-lr-35 t-center">
                            <ul class="mb-3">
                                <li>Product Development</li>
                                <li>Packaging Consultancy</li>
                                <li>Quality assurance and food safety on products, ISO management system consultancy</li>
                            </ul>
                        </div>
                        <a href="services-consultancy.php" class="btn btn-custom">Know More</a>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--====== service PART ENDS ======-->

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
