@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Sign In';
@endphp

@extends('frontend.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    <!-- Login / Register start -->
    <div class="contact-section ptb-100">
        <div class="container ptb-100">
            @include('layouts.success_error')

            <div class="row">
                <div class="col-lg-6">
                    <form class="contactForm" method="post" action="{{ route($mainUri.'.login_submit') }}" data-parsley-validate>

                        @csrf

                        <div class="form-wrapper">
                            <div class="row">
                                <div class="col-lg-12 mb-2"><h4>Login</h4></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" id="email"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Email id field is required."
                                               placeholder="Email Id *">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" id="password"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Password field is required."
                                               placeholder="Password *">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" class="default-btn">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <form class="contactForm" method="post" action="{{ route($mainUri.'.register_submit') }}"
                          autocomplete="off" data-parsley-validate>

                        @csrf

                        <div class="form-wrapper">
                            <div class="row">
                                <div class="col-lg-12 mb-2"><h4>Register</h4></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="f_name" name="f_name"
                                               data-parsley-required="true"
                                               data-parsley-required-message="First name field is required."
                                               placeholder="First Name *">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="l_name" name="l_name"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Last name field is required."
                                               placeholder="Last Name *">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="company_name" name="company_name"
                                               placeholder="Company Name">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address1" name="address1"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="Address line 1 field is required."
                                                  placeholder="Address Line 1 *"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address2" name="address2"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="Address line 2 field is required."
                                                  placeholder="Address Line 2 *"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control" id="country_code" name="country_code"
                                                data-parsley-required="true"
                                                data-parsley-required-message="Country field is required.">
                                            <option value="">Select Country *</option>
                                            <option value="91">India</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control" id="state" name="state" onchange="fetchCity(this)"
                                                data-parsley-required="true"
                                                data-parsley-required-message="State field is required.">
                                            <option value="">Select State *</option>
                                            @foreach($state_master as $state)
                                                <option value="{{ $state->state_id }}">
                                                    {{ $state->state_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control" id="city" name="city"
                                                data-parsley-required="true"
                                                data-parsley-required-message="City field is required.">
                                            <option value="">Select City *</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="pincode" name="pincode"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Pincode field is required."
                                               placeholder="Pincode *">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control" id="gender" name="gender"
                                                data-parsley-required="true"
                                                data-parsley-required-message="Gender field is required.">
                                            <option value="">Select Gender *</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile_no" name="mobile_no"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Mobile number field is required."
                                               placeholder="Mobile Number *">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Email id field is required."
                                               placeholder="Email Id *">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Password field is required."
                                               placeholder="Password *">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" class="default-btn">Register</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Login / Register end -->

    {{-- Slogan File --}}
    {{-- @include('frontend.section_slogan') --}}
    <!-- Slogan start -->
    <div class="contact-section ptb-100" style="padding-top:228px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-details" style="background:transparent">
                        <div class="article-content p-0">
                            <blockquote class="blockquote text-center">
                                <p style="font-size:30px">Always use Eco- Friendly Services & Save our Nature</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slogan end -->

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
    <script type="text/javascript" src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>

    <script>
        var url_role = '{{ route($mainUri.'.fetch_city') }}';

        function fetchCity(obj) {
            var html = '<option value="">Select City *</option>';
            if (obj.value != '' && obj.value.length > 0) {
                var parameters = {};
                parameters.id = obj.value;
                masterJS.hitAjax(url_role + '/' + obj.value, parameters, 'GET', function (success) {
                    // console.log('fetchCity', success);
                    var response = JSON.parse(success);
                    $.each(response['data'], function (index, data) {
                        // console.log('each', data, index);
                        html += '<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>';
                    });

                    $('#city').html(html);
                    // toastr.success('City list fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchCity', error);
                    $('#city').html(html);
                    $('#city').prev('label span').removeClass('red');
                    var error = JSON.parse(error['responseText']);
                    toastr.error(error['data']['message'], 'Error', toastr.options);
                });
            } else {
                $('#city').html(html);
                toastr.error('Invalid state option selected', 'Error', toastr.options);
            }
        }
    </script>
@endsection
