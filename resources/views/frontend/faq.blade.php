@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'FAQ';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!--====== PREALOADER PART START ======-->

    <!-- Start FAQ Area -->
    {{--<section class="faqs-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="faq-image wow fadeInLeft" data-wow-delay=".4s">
                        <img src="{{ asset('assets/img/about.jpg') }}" alt="Faq Image">
                    </div>
                </div>

                <div class="col-lg-7 col-md-12">
                    <div class="faq-content">

                        @forelse($faqs as $key => $faq)
                            <div class="faq-panel">
                                <h5 class="faq-title">
                                    <span>{{ $srNo = $key+1 }}.</span> {{ $faq->faq_que ?? '' }}
                                    <i class="fa fa-plus"></i>
                                </h5>
                                <div class="faq-textarea">
                                    {!! $faq->faq_ans ?? '' !!}
                                </div>
                            </div>
                        @empty
                            No FAQ's Found...!
                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <!-- End FAQ Area -->

    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-200 pb-150 bg_cover" style="background-image: url({{ asset('assets/images/bg/faq.jpg)')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-content">
                        <h3>FAQ'S</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->

    <section id="services" class="pb-30">
        <div class="container py-3">
            <h3 class="mb-1 text-center mt-4">General Questions</h3>
            <div class="row">
                <div class="col-lg-12">
                    <div class="faq-page-content pt-30">
                        <div class="accordion" id="accordionExample">

                            @forelse($faqs as $key => $faq)
                            <div class="card faq-card">
                                <div class="card-header" id="heading{!! $faq->id ?? '' !!}">
                                    <h5>
                                        <a href="#" class="" data-toggle="collapse" data-target="#collapse{!! $faq->id ?? '' !!}" aria-expanded="true" aria-controls="collapse{!! $faq->id ?? '' !!}">
                                            <span>{{ $faq->faq_que ?? '' }}</span>
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse{!! $faq->id ?? '' !!}" class="collapse" aria-labelledby="heading{!! $faq->id ?? '' !!}" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>{!! $faq->faq_ans ?? '' !!}</p>
                                    </div>
                                </div>
                            </div>
                            @empty
                                No FAQ's Found...!
                            @endforelse

                            <h4 class="text-center mt-5 mb-5">SAMPLE SUBMISSION</h4>

                            @forelse($faqs_2 as $key => $faq2)
                            <div class="card faq-card">
                                <div class="card-header" id="heading{!! $faq2->id ?? '' !!}">
                                    <h5>
                                        <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse{{ $faq2->id ?? '' }}" aria-expanded="false" aria-controls="collapse{!! $faq2->id ?? '' !!}">
                                            <span>{{ $faq2->faq_que ?? '' }}</span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse{{ $faq2->id ?? '' }}" class="collapse" aria-labelledby="heading{!! $faq2->id ?? '' !!}" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>{!! $faq2->faq_ans ?? '' !!}</p>
                                    </div>
                                </div>
                            </div>
                                @empty
                                    No FAQ's Found...!
                                @endforelse

                            <h4 class="text-center mt-5 mb-5">ANALYSIS/RESULTS QUESTIONS</h4>

                                @forelse($faqs_3 as $key => $faq3)
                            <div class="card faq-card">
                                <div class="card-header" id="heading{!! $faq3->id ?? '' !!}">
                                    <h5>
                                        <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse{!! $faq3->id ?? '' !!}" aria-expanded="false" aria-controls="collapse{!! $faq3->id ?? '' !!}">
                                            <span>{!! $faq3->faq_que ?? '' !!}</span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse{!! $faq3->id ?? '' !!}" class="collapse" aria-labelledby="heading{!! $faq3->id ?? '' !!}" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>{!! $faq3->faq_ans ?? '' !!}</p>
                                    </div>
                                </div>
                            </div>
                                @empty
                                    No FAQ's Found...!
                                @endforelse


                            <h4 class="text-center mt-5 mb-5">ACCOUNTING QUESTIONS</h4>

                                @forelse($faqs_4 as $key => $faq4)
                            <div class="card faq-card">
                                <div class="card-header" id="heading{!! $faq4->id ?? '' !!}">
                                    <h5>
                                        <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse{!! $faq4->id ?? '' !!}" aria-expanded="false" aria-controls="collapse{!! $faq4->id ?? '' !!}">
                                            <span>{!! $faq4->faq_que ?? '' !!}</span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse{!! $faq4->id ?? '' !!}" class="collapse" aria-labelledby="heading{!! $faq4->id ?? '' !!}" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>{!! $faq4->faq_ans ?? '' !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                                @empty
                                    No FAQ's Found...!
                                @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
    </section>

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
