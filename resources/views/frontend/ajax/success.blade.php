@if(!empty($success))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul style="padding: 0px;margin:0px;">
            @foreach ($success as $value)
                <li>{{ $value }}</li>
            @endforeach
        </ul>
    </div>
@endif
