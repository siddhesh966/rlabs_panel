<!-- Start Testimonials Area -->
{{--<section class="testimonials-section ptb-100 mb-minus-200">
    <div class="container">
        <div class="testimonial-wrapper owl-carousel owl-theme">

            @forelse($testimonials as $key => $testimonial)
                <div class="testimonial-single-item">
                    <div class="icon">
                        <i class="flaticon-quote"></i>
                    </div>
                    <h3>{{ $testimonial->emp_name ?? '' }}</h3>
                    <span>{{ $testimonial->emp_designation ?? '' }}</span>

                    <p>{{ $testimonial->emp_desc ?? '' }}</p>

                    <div class="rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                </div>
            @empty
                No Testimonials Found...!
            @endforelse

        </div>
    </div>
</section>--}}

<!--====== CLIENT PART START ======-->

<section id="client-part" class="pb-30 pt-30">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title text-center">
                    <img src="{{ asset('assets/images/client/c.png')}}" alt="icon">
                    <h2>Our Exhort Happy Clients say !</h2>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <p>An organisation’s greatest asset is the positive assessment from their clientele. We are honoured to receive healthy feedbacks that bring us a notch closer to serving our customers with commitment and competence. Here are a few testimonials below.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="client-slied owl-carousel">
                @forelse($testimonials as $key => $testimonial)
                <div class="col-lg-12">
                    <div class="singel-client mt-50">
                        <div class="client-thum">
                            <!-- <div class="client-img">
                                <img src="images/client/c-2.jpg" alt="Client">
                            </div> -->
                            <div class="client-head">
                                <h5>{{ $testimonial->emp_name ?? '' }} </h5>
                                <span>{{ $testimonial->emp_designation ?? '' }}</span>
                            </div>
                        </div>
                        <div class="client-text mt-35">
                            <div class="star mb-2">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                            </div>
                            <p class="text-justify">{{ $testimonial->emp_desc ?? '' }}</p>
                        </div>
                    </div>
                </div>
                @empty
                    No Testimonials Found...!
                @endforelse
            </div>
        </div>
    </div>
</section>

<!--====== CLIENT PART ENDS ======-->
<!-- End Testimonials Area -->
