@php
    $mainUri = Request::segment(1);
    if($mainUri == ""){ $mainUri = "website"; }
    $prefixUri = Request::segment(2);
    $prefixUriId = Request::segment(3);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }

    $sub_services_list = \App\Models\SubService::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

    $services_list = \App\Models\ServiceMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

    $cart = Session::get('cart');
    if(!empty($cart)){ $cart_counter = sizeof($cart); }
@endphp

<!-- Start Header Area -->
<!-- Authenticity login start -->
<div class="modal fade" id="auth-login">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- <div class="modal-header">
              <h4 class="modal-title">Authenticity Login</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div> -->

            <div class="modal-body text-center">
                <button type="button" class="btn btn-custom btn-circle" data-dismiss="modal">
                    <i class="fa fa-times"></i></button>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="login-form text-center">
                            <div class="logo mb-4">
                                <a href="javascript:void(0)"><img src="{{ asset('assets/images/logo.svg')}}" alt="Logo"></a>
                            </div>
                            {{--<form action="#">--}}
                            <form class="contactForm" method="post" action="{{ route($mainUri.'.login_submit') }}" data-parsley-validate>
                                @csrf
                                <div class="singel-form">
                                    <input type="email" name="email" id="email" placeholder="Enter your email....">
                                </div>
                                <div class="singel-form">
                                    <input type="password" name="password" id="password" placeholder="Enter your Password....">
                                </div>
                                <div class="singel-form pt-3">
                                    <button type="submit">Login</button>
                                </div>
                                <div class="singel-form pt-3">
                                    <ul class="remember">
                                        <li>
                                            <input type="checkbox" name="checkbox" id="checkbox">
                                            <label for="checkbox"><span></span>Remember Me</label>
                                        </li>
                                        <li>
                                            <p>Forgot <a href="#">password?</a></p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="singel-form pt-2">
                                    <ul class="remember">
                                        <li class="w-100 text-center">
                                            <p><a href="#">Register</a></p>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="login-form">
                            <p class="mb-2">
                                Today, where technology may lead to
                                deception, it is very important for the customer
                                to know about the Authenticity of his "Test
                                Report".
                            </p>
                            <p class="mb-2">To make this easier for the customer, Radiant
                                has come up with a unique <i><b>Online Client portal</b></i>.
                                Once logged in, the customer can check the
                                Authenticity of the reports along with their
                                entire history of Analysis with Radiant.</p>
                            <p class="mb-2">
                                In case of any queries / discrepancies, email us
                                at support@radiantlab.in for any queries.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--Authenticity login end-->

<!--Left right float icon start-->

<div id="myFeedback">
    <button class="feedback" data-toggle="modal" data-target="#exampleModal1">Feedback</button>
    <button class="request" data-toggle="modal" data-target="#exampleModal2">Enquire Now</button>
</div>

<!--====== FEEDBACK START ======-->
<div class="modal fade pr-0" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideout modal-md" role="document">
        <div class="modal-content" style="background: #f7f7f7;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="mb-1 font-weight-bold" style="font-size: 14px;">Dear Customer,</p>
                <p class="mb-3 text-justify" style="font-size: 14px;">It is always our endeavor to continuously improve the quality of services we provide to you. Kindly spare a few minutes to fill this feedback form. Your feedback is very important to us and will help us to serve you better. Thank you.
                </p>
                <div class="container p-0">
                    <form class="" name="feedLeads" id="feedLeads" method="post"
                          action="{{ route($mainUri.'.save_feedback') }}" autocomplete="off">
                        @csrf
                        <input type="hidden" id="rating" name="rating" value="-1">
                        <div class="row">

                            <div class="col-lg-6 form-group mb-3">
                                <input type="text" class="form-control" id="feed_name"
                                       placeholder="Enter Name" name="feed_name">
                            </div>
                            <div class="col-lg-6 form-group mb-3">
                                <input type="text" class="form-control" id="feed_desg"
                                       placeholder="Enter Designation" name="feed_desg">
                            </div>
                            <div class="col-lg-6 form-group mb-3">
                                <input type="text" class="form-control" id="feed_org"
                                       placeholder="Enter Organization name" name="feed_org">
                            </div>
                            <div class="col-lg-6 form-group mb-3">
                                <input type="text" class="form-control" id="feed_address" placeholder="Enter Address" name="feed_address">
                            </div>
                            <div class="col-lg-6 form-group mb-3">
                                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                            </div>
                            <div class="col-lg-6 form-group mb-3">
                                <input type="email" class="form-control" id="feed_phone" placeholder="Enter Phone" name="feed_phone">
                            </div>
                            <div class="col-lg-12 form-group mb-3">
                                <input type="text" class="form-control" id="feed_pcrl" placeholder="Point of contact from RL" name="feed_pcrl">
                            </div>

                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">Promptness of response to your enquiry / service request
                                    </div>
                                    <div class="star rating">
                                        <span class="ratings_stars fa fa-star" data-rating="1"></span>
                                        <span class="ratings_stars fa fa-star" data-rating="1"></span>
                                        <span class="ratings_stars fa fa-star" data-rating="1"></span>
                                        <span class="ratings_stars fa fa-star" data-rating="1"></span>
                                        <span class="ratings_stars fa fa-star" data-rating="1"></span>
                                    </div>
                                                                        {{--<style>.star-fg .glyphicon-heart {
                                            color: red !important;
                                        }</style>
                                    <div data-role="ratingbar" data-steps="3" style="font-size: 10px">
                                        <ul>
                                            <li><a href="#"><span class="fa fa-star"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star"></span></a></li>
                                        </ul>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">Staff politeness with you while providing the necessary information
                                    </div>
                                    <div class="star">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">How satisfied are you with the timeliness of getting Test Certificate?
                                    </div>
                                    <div class="star">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">Did the report contain all parameters as requested by you
                                    </div>
                                    <div class="star">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio2">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">How satisfied are you with the customer support?
                                    </div>
                                    <div class="star">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">How satisfied are you with the Grievance Redressal
                                    </div>
                                    <div class="star">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="font-weight-bold ml-2">NA</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">How would you rate your overall experience with our service?
                                    </div>
                                    <div class="star">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading mb-2">Since how long have you been seeking RL’s services
                                    </div>
                                    <div class="star">
                                        <div class="w-100 form-group">
                                            <input type="text" class="form-control" id="feed_long" placeholder="Enter Number" name="feed_long">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">Would you prefer to use RL’s services again
                                    </div>
                                    <div class="star">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio3" name="example2" value="customEx">
                                            <label class="custom-control-label" for="customRadio3">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio4" name="example2" value="customEx">
                                            <label class="custom-control-label" for="customRadio4">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading">Would you recommend our services to other people?
                                    </div>
                                    <div class="star">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio5" name="example3" value="customEx">
                                            <label class="custom-control-label" for="customRadio5">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="customRadio6" name="example3" value="customEx">
                                            <label class="custom-control-label" for="customRadio6">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading mb-2">Please mention any other service that you would like RL to add to its porfolio
                                    </div>
                                    <div class="star">
                                        <div class="w-100 form-group">
                                            <input type="text" class="form-control" id="feed_oserv" placeholder="Enter Name" name="feed_oserv">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading mb-2">Any RL employee you would like to make special mention of
                                    </div>
                                    <div class="star">
                                        <div class="w-100 form-group">
                                            <input type="text" class="form-control" id="feed_emp" placeholder="RL employee" name="feed_emp">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mb-3">
                                <div class="card-star text-center">
                                    <div class="star-heading mb-2">Please mention any other addional comments / suggestions you have on our service
                                    </div>
                                    <div class="star">
                                        <div class="w-100 form-group">
                                            <textarea type="textarea" class="form-control" id="feed_comm" placeholder="Enter Comments" name="feed_comm"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">SUBMIT</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>




<!--====== REQUEST QUOTE START ======-->
<div class="modal fade pr-0" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideout modal-md" role="document">
        <div class="modal-content" style="background: #f7f7f7;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabe2">Enquire Now</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="contact-form p-lr-35">
                    <!-- <h6>Leave Reply</h6> -->
                    {{--<form id="contact-form" action="contact.php" data-toggle="validator">--}}
                    <form class="" name="formLeads" id="formLeads" method="post"
                          action="{{ route($mainUri.'.save_leads') }}" autocomplete="off">
                        <input type="hidden" name="category_name" value="Enquiry Now">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="singel-form form-group mt-0">
                                    <!-- <label>Full name :<span style="color: red;">*</span></label> -->
                                    <input name="lead_name" id="lead_name" type="text"
                                           data-error="Name is required." placeholder="Enter Full Name">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="singel-form form-group">
                                    <!-- <label>Email Address :<span style="color: red;">*</span></label> -->
                                    <input type="email" name="lead_email" id="lead_email"
                                           data-error="Valid email is required." placeholder="Email Address">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="singel-form form-group">
                                    <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                    <input type="text" name="lead_mobile" id="lead_mobile"
                                           data-error="Valid mobile no. is required." placeholder="Mobile Number">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="singel-form form-group">
                                    <!-- <label>Write a message :</label> -->
                                    <select id="lead_type" name="lead_type">
                                        <option value="" selected>Please Select</option>
                                        <option value="Individual">Individual</option>
                                        <option value="Company">Company</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12 comp" style="display:none">
                                <div class="singel-form form-group">
                                    <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                    <input type="email" name="company" data-error="Valid Company is required." required="required" placeholder="Enter Company Name">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12 comp" style="display:none">
                                <div class="singel-form form-group">
                                    <!-- <label>Mobile Number :<span style="color: red;">*</span></label> -->
                                    <input type="email" name="designation" data-error="Valid designation is required." required="required" placeholder="Enter Designation">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="singel-form form-group">
                                    <!-- <label>Write a message :</label> -->
                                    <select id="services_id" name="services_id">
                                        <option value="" selected>Please Select Service</option>
                                        <option value="Food Testing">Food Testing</option>
                                        <option value="Water Testing">Water Testing</option>
                                        <option value="Soil Testing">Soil Testing</option>
                                        <option value="Building Materials">Building Materials</option>
                                        <option value="Audit Inspection">Audit Inspection</option>
                                        <option value="Consultancy">Consultancy</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="singel-form form-group">
                                    <!-- <label>Write a message :</label> -->
                                    <textarea style="height: 100px;" name="lead_message" id="lead_message"
                                              data-error="Please,leave us a message." placeholder="Write a message"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">SUBMIT</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====== SOCIAL ICON PART START ======-->

<div id="fixed-social">
    <div>
        <a href="#" class="fixed-facebook" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a>
    </div>
    <!-- <div>
      <a href="#" class="fixed-twitter" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a>
    </div> -->
    <!-- <div>
      <a href="#" class="fixed-gplus" target="_blank"><i class="fa fa-google"></i> <span>Google+</span></a>
    </div> -->
    <div>
        <a href="#" class="fixed-linkedin" target="_blank"><i class="fa fa-linkedin"></i> <span>LinkedIn</span></a>
    </div>
    <div>
        <a href="#" class="fixed-instagrem" target="_blank"><i class="fa fa-instagram"></i> <span>Instagram</span></a>
    </div>
    <div>
        <a href="#" class="fixed-tumblr" target="_blank"><i class="fa fa-whatsapp"></i> <span>WhatsApp</span></a>
    </div>
</div>

<!--====== SOCIAL ICON PART END ======-->

<!--Left right float icon end-->

<!--====== HEADER PART START ======-->
<header id="header-part" class="">
    <!--===== HEADER TOP START =====-->
    <div class="header-top pt-1 pb-1 d-none d-lg-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <div class="phone text-right text-lg-left">

                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 ">
                    <div class="opening text-right">

                    </div>
                </div>
                <div class="col-lg-5 col-xl-6">
                    <div class="address text-right text-lg-right d-flex justify-content-end pr-3">
                        <a href="mailto:info@radiantlab.in">
                            <p class="mr-4">Email : info@radiantlab.in</p>
                        </a>
                        <p>Phone : (+88 - 123456789)</p>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
    <!--===== HEADER TOP ENDS =====-->
    <!--===== NAVBAR START =====-->
    <div class="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">

                        <a class="navbar-brand" href="index.php">
                            <img src="{{ asset('assets/images/logo.svg')}}" alt="Logo">
                        </a> <!-- Logo -->

                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse sub-menu-bar justify-content-end"
                             id="navbarSupportedContent">
                            <ul id="navi" class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "index") ? "active" : "" }}" href="{{ route($mainUri.'.index') }}" role="button" >Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "about_us") ? "active" : "" }}" href="{{ route($mainUri.'.about_us') }}">Who We Are</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "services") ? "active" : "" }}" href="{{ route($mainUri.'.services') }}">What We Do</a>
                                    <ul id="ul-ac" class="sub-menu">
                                        @forelse($services_list as $key => $single_service)
                                        <li class="li"><a href="{{ route($mainUri.'.service_details', [$single_service->services_id]) }}">{{ $single_service->services_title }}</a></li>
                                        {{--<li class="li"><a href="services-water.php">Water</a></li>
                                        <li class="li"><a href="services-soil.php">Soil</a></li>
                                        <li class="li"><a href="services-building-material.php">Building Materials</a></li>
                                        <li class="li"><a href="services-audit-and-inspection.php">Audit & Inspection</a></li>
                                        <li class="li"><a href="services-consultancy.php">Consultancy</a></li>--}}

                                        @empty
                                            <li class="li"><a href="#">No Service</a></li>
                                        @endforelse
                                    </ul>
                                </li>
                                <!-- <li class="nav-item">
                                    <a href="career.php">Career</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "blog") ? "active" : "" }}" href="{{ route($mainUri.'.blog') }}">Articles</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "contact_us") ? "active" : "" }}" href="{{ route($mainUri.'.contact_us') }}">Contact Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ ($prefixUri == "faq") ? "active" : "" }}" href="{{ route($mainUri.'.faq') }}">FAQ</a>
                                </li>

                                @if(Session::has('userMasterSession'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route($mainUri.'.logout') }}">Logout</a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a data-toggle="modal" data-target="#auth-login" href="login.php">Log In</a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <!-- <div class="cart-search pl-2 pr-3"> -->
                        <!--<p class="d-none d-lg-block">info@radiantlab.in	<i class="fa fa-phone"></i> (+88 - 123456789)</p>-->
                        <!-- <ul class="text-right"> -->
                        <!--<li><a href="#"><i class="fa fa-shopping-basket"></i><span>0</span></a></li>-->
                        <!-- <li><a id="search" href="#"><i class="fa fa-search"></i></a> -->
                        <!-- <div class="search-box"> -->
                        <!-- <input type="search" placeholder="Search..."> -->
                        <!-- <button type="button"><i class="fa fa-search"></i></button> -->
                        <!-- </div> -->
                        <!-- </li> -->
                        <!-- </ul> -->
                        <!-- </div> -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--===== NAVBAR ENDS =====-->
</header>
<!-- End Header Area -->
