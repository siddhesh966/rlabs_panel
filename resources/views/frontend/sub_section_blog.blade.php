<div class="row">

    @forelse($blogs as $key => $blog)
        <div class="col-lg-4 col-md-6">
            <div class="single-blog-post">
                <div class="post-image">
                    <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}"><img src="{{ $blog->blogImage ?? '' }}" alt="No Preview Available"></a>
                </div>

                <div class="post-content">
                    <ul>
                        <li><i class="fas fa-user"></i> <a href="javascript:void(0);">Admin</a></li>
                        <li><i class="far fa-calendar-alt"></i> {{ date("F d, Y", strtotime($blog->created_at)) }}</li>
                    </ul>
                    <h3><a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}">{{ $blog->blogTitle ?? '' }}</a></h3>
{{--                    <p>Lorem ipsum dolor sit amet, consecttur adipiscing elit, sed do eiusmod tempor.</p>--}}

                    <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}" class="read-more-btn">Read More</a>
                </div>
            </div>
        </div>
    @empty
        No Blogs Found...!
    @endforelse

</div>

@if($blogs instanceof \Illuminate\Pagination\LengthAwarePaginator)
    <div class="pagination-area">
        {{ $blogs->appends(request()->query())->links() }}
    </div>
@endif
