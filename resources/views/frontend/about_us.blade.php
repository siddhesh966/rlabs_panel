@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'About Us';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!--====== PREALOADER PART START ======-->

    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-200 pb-150 bg_cover" style="background-image: url({{ asset('assets/images/bg/about.jpg)')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-content">
                        <h3>Who We Are</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->


    <!--====== ABOUT PART START ======-->

    <section id="about-part" class="pt-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="about-content">
                        <h3><span>Welcome To</span> Radiant Lab.</h3>
                        <p>Radiant Laboratoriers started its operations in the year 2000 in Nagpur with a vision to
                            offer a one stop solution to the Analytical Industry. Over the last 2 decades the company
                            has executed various projects spread across India. The company is currently operational from
                            Pune however it is now having a pan India presence based on variety of activities taken
                            across under various fields. </p>
                        <p>These activities are carried out for Clients, Industries, Government Agencies and other
                            Organizations.</p>

                        <p>The quality system of Radiant Laboratories is designed to comply with various National
                            and International standards so as to deliver results with complete traceability,
                            transparency and confidentiality.</p>

                        <div class="more-text">
                            <p>RL makes use of customized LIMS in all its Analytical Laboratory Divisions to automate &
                                control the key laboratory operations. The benefits of LIMS include: </p>
                            <p>Registration of samples digitally, Tracking of progress during analysis, Direct data
                                capture from instrumentation, calculations & validation of results, Minimize
                                transcription and double handling, Generate analytical and management reports
                            </p>
                        </div>
                        <!-- <p class="show-more" style="color: #2e4db9;cursor: pointer;font-weight: 600;">Read More</p> -->
                        <!-- <p class="show-less" style="color: #2e4db9;cursor: pointer;font-weight: 600;display: none;" >Read Less</p> -->
                    </div>
                </div>
            </div>
            <div class="row pt-30">
                <div class="col-lg-6">
                    <div class="about-content">
                        <h3><span>From Director’s Desk</span></h3>
                        <p>Recognizing the potential for phenomenal growth in the industry, we decided to create an
                            organization that prides itself on quality and integrity where commercial testing and contractual
                            research are provided to Clients, Industries, Government Agencies and other Organizations.</p>
                        <p>Our main goal has always been to satisfy our Customer’s needs by delivering services driven by
                            innovation & solution-oriented approach making use of the best appropriate technology and
                            methods.</p>
                        <p>Our team is a unique blend of young talented minds along with the experienced professionals
                            who bring out the best in each other and together contribute to the success of the organization.</p>
                        <p>Through our expertise, we at Radiant Laboratories look forward to bring value addition to your
                            business.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="video">
                        <img class="video-img" src="{{ asset('assets/images/director-desk.png')}}" alt="Video">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== ABOUT PART ENDS ======-->

    {{-- Counter File --}}
    @include('frontend.section_whom_we_serve')

    {{-- Our Vision --}}
    @include('frontend.section_our_vision')

    {{-- Our Mission --}}
    @include('frontend.section_our_mission')

    {{-- Core Values --}}
    @include('frontend.section_core_values')

    {{-- Testimonial File --}}
    @include('frontend.section_testimonial')


@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
