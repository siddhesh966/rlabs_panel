<!-- Start Contact-Us Form -->
<div class="contact-section ptb-100 pt-0">
    <div class="container">
        <div class="form-wrapper">
            <form class="contactForm" name="formLeads" id="formLeads" method="post"
                  action="{{ route($mainUri.'.save_leads') }}" autocomplete="off">

                @csrf

                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="form-group">
                            <input type="text" name="lead_name" id="lead_name"
                                   class="form-control" placeholder="Name" />
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="form-group">
                            <input type="email" name="lead_email" id="lead_email"
                                   class="form-control" placeholder="Email Id" />
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <input type="text" name="lead_mobile" id="lead_mobile"
                                   class="form-control" placeholder="Mobile Number" />
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                            <textarea name="lead_message" id="lead_message"
                                      class="form-control" rows="6" placeholder="Message"></textarea>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <button type="submit" class="default-btn">Send Message</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Contact-Us Form -->
