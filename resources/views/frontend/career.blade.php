@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'CAREER';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!--====== PREALOADER PART START ======-->

    {{--<section class="blog-section ptb-100">
        <div class="container">
            @include('frontend.sub_section_blog')
        </div>
    </section>--}}

    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-200 pb-150 bg_cover" style="background-image: url({{ asset('assets/images/bg/career.jpg)')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-content">
                        <h3>Career</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->

    <!--====== RUNNING SCRIPT PART START ======-->
    <section>
        <div class="container">
            <div class="row example-result pb-5 pt-5">
                <div class="col-lg-12">
                    <div class="as-cta-desc">
                        <h2 class="font-weight-normal text-white w-100 text-center">Career</h2>
                        <p class="mb-2 mt-2 text-white w-100 text-center">"For future opportunities with us, Please send us your CV/Resume and we will connect with you."</p>
                    </div>
                </div>
                <div class="w-100 d-flex justify-content-center">
                    <div class="col-lg-4 mt-3">
                        <form class="" action="">
                            <!-- <p>Custom file:</p> -->
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="filename">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>

                            <div class="w-100 mt-2 text-center">
                                <button style="font-weight: 600;" type="submit" class="btn btn-custom">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== RUNNING SCRIPT PART ENDS ======-->

    {{-- Testimonial File --}}
    @include('frontend.section_testimonial')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
