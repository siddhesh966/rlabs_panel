@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Checkout';

    $cartPriceDetails = calculateCartPriceDetails($cart);
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    <!-- Start Account / Delivery Address Details Area -->
    <div class="contact-section ptb-100">
        <div class="container">

            @include('layouts.success_error')

            <div class="row">
                <div class="col-lg-12">
                    <div id="errorDiv"></div>
                    <div id="successDiv"></div>
                    <form class="contactForm" name="formUpdateDetails" id="formUpdateDetails" method="post" action="{{ route($mainUri.'.update_account_details') }}">

                        @csrf

                        <input type="hidden" name="user_id" id="user_id" value="{{ $userMasterSession['user_id'] ?? 0 }}" />
                        <input type="hidden" name="user_info_id" id="user_info_id" value="{{ $userMasterSession['user_info_id'] ?? 0 }}" />

                        <div class="form-wrapper mb-0">
                            <div class="row">
                                <div class="col-lg-12 mb-3">
                                    <h4>Delivery Address Details</h4>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="f_name" name="f_name" placeholder="First Name *" value="{{ $userMasterSession['f_name'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="l_name" name="l_name" placeholder="Last Name *" value="{{ $userMasterSession['l_name'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{ $userMasterSession['company_name'] ?? '' }}" />
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address1" name="address1" placeholder="Address Line 1 *">{{ $userMasterSession['address1'] ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address2" name="address2" placeholder="Address Line 2 *">{{ $userMasterSession['address2'] ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="country_code" name="country_code">
                                            <option value="">Select Country *</option>
                                            <option value="91">India</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="state" name="state" onchange="fetchCity(this.value, '{{ route($mainUri.'.fetch_city') }}', '0')">
                                            <option value="">Select State *</option>
                                            @foreach($state_master as $state)
                                                <option value="{{ $state->state_id }}">
                                                    {{ $state->state_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="city" name="city">
                                            <option value="">Select City *</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode *"  value="{{ $userMasterSession['pincode'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="">Select Gender *</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Mobile Number *" value="{{ $userMasterSession['mobile_no'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" class="default-btn">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Account / Delivery Address Details Area -->

    <!-- Start Coupon Details Area -->
    <div class="container pb-100">
        <form name="formApplyCoupon" id="formApplyCoupon">
            @csrf
            <input type="hidden" name="c_user_id" id="c_user_id" value="{{ $userMasterSession['user_id'] ?? 0 }}" />
            <input type="hidden" id="c_finalTotal" name="c_finalTotal" value="{{ $cartPriceDetails['finalTotal'] }}" />
            <select name="coupon_code" id="coupon_code">
                <option value="">Select Coupon</option>
                @foreach($promo_code_master as $promo_code)
                    <option value="{{ $promo_code->promo_code_id }}">
                        {{ $promo_code->promo_code_name }}
                    </option>
                @endforeach
            </select>
            <button name="action" type="submit" id="apply_code" value="apply_code">Apply</button>
            <button style="display:none;" name="action" type="submit" id="remove_code" value="remove_code">Remove</button>
        </form>
    </div>
    <!-- End Coupon Details Area -->

    <!-- Start Order Details Area -->
    <div class="container">
        <h4 class="mb-4">Order Details</h4>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Service</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cart as $key => $singleService)
                <tr>
                    <td>
                        <div class="cleaning-details">
                            <ul>
                                <li class="mb-2">{{ $singleService['sub_services_title'] ?? '' }}{{--: <span>*{{ $singleService['service_quantity'] ?? '' }}</span>--}}</li>
                                <li class="mb-2">House Type: <span>{{ $singleService['services_type_name'] ?? '' }}</span></li>
{{--                                <li class="mb-2">Service Type:--}}
{{--                                    @if($singleService['service_category'] == 1)--}}
{{--                                        <span>Single Service</span>--}}
{{--                                    @elseif($singleService['service_category'] == 2)--}}
{{--                                        <span>4 Services Per Year</span>--}}
{{--                                    @elseif($singleService['service_category'] == 3)--}}
{{--                                        <span>12 Services Per Year</span>--}}
{{--                                    @else--}}
{{--                                        ---}}
{{--                                    @endif--}}
{{--                                </li>--}}
                                <li class="mb-2">Service Date:<span>{{ date("d-m-Y", strtotime($singleService['service_date'])) }}</span></li>
                                <li>Service Time : <span>{{ $singleService['service_time'] ?? '' }}</span></li>
                            </ul>
                        </div>
                    </td>
                    <td style="vertical-align: middle;"><div>₹{{ $totalPrice = $singleService['services_price'] * $singleService['service_quantity'] }}</div></td>
                </tr>
            @endforeach

            <tr>
                <th>Subtotal</th>
                <th>₹{{ $cartPriceDetails['subTotal'] }}</th>
            </tr>
            <tr>
                <th>GST 18%</th>
                <th>₹{{ $cartPriceDetails['gst'] }}</th>
            </tr>
            <tr>
                <th>Total</th>
                <th id="th_finalTotal">₹{{ $cartPriceDetails['finalTotal'] }}</th>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- End Order Details Area -->

    <!-- Start Payment Details Area -->
    <div class="container mt-4 pb-100">
        <form name="formCheckout" id="formCheckout" method="post">

            @csrf

            {{-- For Offline Payment - Start --}}
            <input type="hidden" name="p_user_id" id="p_user_id" value="{{ $userMasterSession['user_id'] ?? 0 }}" />
            <input type="hidden" name="p_user_info_id" id="p_user_info_id" value="{{ $userMasterSession['user_info_id'] ?? 0 }}" />
            <input type="hidden" id="p_subTotal" name="p_subTotal" value="{{ $cartPriceDetails['subTotal'] }}" />
            <input type="hidden" id="p_gst" name="p_gst" value="{{ $cartPriceDetails['gst'] }}" />
            <input type="hidden" id="p_finalTotal" name="p_finalTotal" value="{{ $cartPriceDetails['finalTotal'] }}" />
            <input type="hidden" id="p_coupon_code" name="p_coupon_code" />
            {{-- For Offline Payment - End --}}

            {{-- For Online Payment - Start --}}
            <input type="hidden" id="key" name="key" placeholder="Merchant Key" value="9yRtPtx1" />
            <input type="hidden" id="txnid" name="txnid" placeholder="Transaction ID" value="{{ getGUIDnoHash() }}" />
            <input type="hidden" id="amount" name="amount" placeholder="Amount" value="1" />
{{--            <input type="hidden" id="amount" name="amount" placeholder="Amount" value="{{ $cartPriceDetails['finalTotal'] }}" />--}}
            <input type="hidden" id="productinfo" name="productinfo" placeholder="Product Info" value="NoInfo" />
            <input type="hidden" id="firstname" name="firstname" placeholder="First Name" value="{{ $userMasterSession['f_name'].' '.$userMasterSession['l_name'] ?? '' }}" />
            <input type="hidden" id="email" name="email" placeholder="Email ID" value="{{ $userMasterSession['email'] ?? '' }}" />
            <input type="hidden" id="phone" name="phone" placeholder="Mobile Number" value="{{ $userMasterSession['mobile_no'] ?? '' }}" />

            <input type="hidden" id="udf1" name="udf1" value="{{ $userMasterSession['user_id'] ?? 0 }}" />
            <input type="hidden" id="udf2" name="udf2" value="{{ $userMasterSession['user_info_id'] ?? 0 }}" />
            <input type="hidden" id="udf3" name="udf3" value="{{ $cartPriceDetails['subTotal'] }}" />
            <input type="hidden" id="udf4" name="udf4" value="{{ $cartPriceDetails['gst'] }}" />
            <input type="hidden" id="udf5" name="udf5" value="" /> {{-- Promo Code Id --}}
            <input type="hidden" id="hash" name="hash" placeholder="Hash" value="" />
            <input type="hidden" id="surl" name="surl" value="{{ route($mainUri.'.checkout_online') }}" />
            {{-- For Online Payment - End --}}


            <div class="form-check mb-3">
                <label class="form-check-label" for="radio1">
                    <input type="radio" class="form-check-input" id="radio1" name="optradio" value="cod" checked>
                    Cash on Delivery
                </label>
                <div class="cash-pay" style="">Pay with cash upon delivery.</div>
            </div>
            <div class="form-check">
                <label class="form-check-label" for="radio2">
                    <input type="radio" class="form-check-input" id="radio2" name="optradio" value="online">
                    Credit & Debit Cards / Netbanking / UPI <img class="ml-2" src="{{ asset('assets/img/logo_payu-money.png') }}" alt="Payu-Logo">
                </label>
                <div class="upi-pay" style="display:none">
                    <ul>
                        <li>Pay securely with:</li>
                        <li>– Credit or Debit Cards</li>
                        <li>– Internet Banking</li>
                        <li>– UPI</li>
                        <li>Powered by PayUindia.</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12 mt-4">
                <button type="button" name="btnPlaceOrder" id="btnPlaceOrder" class="default-btn border-0">Place Order</button>
            </div>
        </form>
    </div>
    <!-- End Payment Details Area -->

    {{-- Slogan File --}}
    @include('frontend.section_slogan')

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--}}
    {{--BOLT Sandbox/Test--}}
    {{--<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>--}}
    {{--BOLT Production/Live--}}
    <script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="ED193F" bolt-logo="{{ asset('assets/img/logo.png') }}"></script>

    <script>
        $("#country_code").val("{{ $userMasterSession['country_code'] ?? '' }}");
        $("#state").val("{{ $userMasterSession['state'] ?? '' }}");
        fetchCity("{{ $userMasterSession['state'] ?? '' }}", '{{ route($mainUri.'.fetch_city') }}', '1');
        $("#gender").val("{{ $userMasterSession['gender'] ?? '' }}");

        $("#btnPlaceOrder").click(function(){
            var route = "";
            if($('input[name="optradio"]:checked').val() == "cod"){
                ajaxFetch("{{ route($mainUri.'.check_coupon_code') }}", "#formCheckout", "", "", ".preloader", true, "post", true,
                    function (response) {
                        if(response.code == 200){
                            if(response.type == 'error'){
                                toastr.error(response.message, 'Error', toastr.options);
                            }else if(response.type == 'success'){
                                // toastr.success(response.message, 'Success', toastr.options);
                                route = "{{ route($mainUri.'.checkout_cod') }}";
                                $('#formCheckout').attr('action', route).submit();
                            }
                        }else{
                            toastr.error('Something went wrong...!', 'Error', toastr.options);
                        }
                        $('.preloader').hide();
                    });
            }else if($('input[name="optradio"]:checked').val() == "online"){
                ajaxFetch("{{ route($mainUri.'.check_coupon_code') }}", "#formCheckout", "", "", ".preloader", true, "post", true,
                    function (response) {
                        if(response.code == 200){
                            if(response.type == 'error'){
                                toastr.error(response.message, 'Error', toastr.options);
                            }else if(response.type == 'success'){
                                // toastr.success(response.message, 'Success', toastr.options);
                                $('.preloader').hide();
                                launchBOLT(); return false;
                            }
                        }else{
                            toastr.error('Something went wrong...!', 'Error', toastr.options);
                        }
                        $('.preloader').hide();
                    });
            }
        });

        function launchBOLT()
        {
            bolt.launch({
                key: $('#key').val(),
                txnid: $('#txnid').val(),
                hash: $('#hash').val(),
                amount: $('#amount').val(),
                firstname: $('#firstname').val(),
                email: $('#email').val(),
                phone: $('#phone').val(),
                productinfo: $('#productinfo').val(),
                udf1: $('#udf1').val(),
                udf2: $('#udf2').val(),
                udf3: $('#udf3').val(),
                udf4: $('#udf4').val(),
                udf5: $('#udf5').val(),
                surl : $('#surl').val(),
                furl: $('#surl').val(),
                mode: 'dropout'
            },{ responseHandler: function(BOLT){
                    console.log( BOLT.response.txnStatus );
                    if(BOLT.response.txnStatus != 'CANCEL')
                    {
                        var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
                            {{--'<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\" />' +--}}
                            '<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
                            '<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
                            '<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
                            '<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
                            '<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
                            '<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
                            '<input type=\"hidden\" name=\"udf1\" value=\"'+BOLT.response.udf1+'\" />' +
                            '<input type=\"hidden\" name=\"udf2\" value=\"'+BOLT.response.udf2+'\" />' +
                            '<input type=\"hidden\" name=\"udf3\" value=\"'+BOLT.response.udf3+'\" />' +
                            '<input type=\"hidden\" name=\"udf4\" value=\"'+BOLT.response.udf4+'\" />' +
                            '<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
                            '<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
                            '<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
                            '<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
                            '</form>';
                        var form = jQuery(fr);
                        jQuery('body').html(form);
                        form.submit();
                    }
                },
                catchException: function(BOLT){
                    alert( BOLT.message );
                }
            });
        }

        $("#formApplyCoupon").validate({
            submitHandler: function(form) {
                ajaxFetch("{{ route($mainUri.'.apply_coupon_code') }}", "#formApplyCoupon", "", "", ".preloader", true, "post", true,
                    function (response) {
                        if(response.code == 200){
                            if(response.type == 'error'){
                                $("#coupon_code").val("");
                                $("#p_coupon_code").val("");
                                $("#udf5").val("");
                                toastr.error(response.message, 'Error', toastr.options);
                            }else if(response.type == 'success'){
                                toastr.success(response.message, 'Success', toastr.options);
                                $("#p_finalTotal").val(response.data.finalTotal);
                                $("#amount").val(response.data.finalTotal);
                                $("#th_finalTotal").html("₹"+response.data.finalTotal);
                                if(response.data.action == "apply_code")
                                {
                                    $("#p_coupon_code").val(response.data.coupon_code);
                                    $("#udf5").val(response.data.coupon_code);
                                    $("#coupon_code").prop("disabled", true);
                                    $("#apply_code").hide();
                                    $("#remove_code").show();
                                }
                                else if(response.data.action == "remove_code")
                                {
                                    $("#p_coupon_code").val(response.data.coupon_code);
                                    $("#udf5").val(response.data.coupon_code);
                                    $("#coupon_code").prop("disabled", false);
                                    $("#coupon_code").val("");
                                    $("#apply_code").show();
                                    $("#remove_code").hide();
                                }
                            }
                        }else{
                            toastr.error('Something went wrong...!', 'Error', toastr.options);
                        }
                        generateHash();
                        $('.preloader').hide();
                    });
            }
        });

        $("#formUpdateDetails").validate({
            rules: {
                f_name: "required",
                l_name: "required",
                address1: "required",
                address2: "required",
                country_code: "required",
                state: "required",
                city: "required",
                pincode: "required",
                gender: "required",
                mobile_no: "required",
            },
            messages: {

            },
            submitHandler: function(form) {
                ajaxFetch("{{ route($mainUri.'.update_account_details') }}", "#formUpdateDetails", "#successDiv", "#errorDiv", ".preloader", true, "post", true,
                    function (response) {
                        if(response.code == 200){
                            if(response.type == 'error'){
                                toastr.error(response.message, 'Error', toastr.options);
                                $("#errorDiv").show();
                                $("#errorDiv").html(response.view);
                                $("#successDiv").hide();
                            }else if(response.type == 'success'){
                                toastr.success(response.message, 'Success', toastr.options);
                                $("#firstname").val(response.data.firstname);
                                $("#phone").val(response.data.phone);
                                $("#successDiv").show();
                                $("#successDiv").html(response.view);
                                $("#errorDiv").hide();
                            }
                        }else{
                            toastr.error('Something went wrong...!', 'Error', toastr.options);
                        }
                        generateHash();
                        $('.preloader').hide();
                    });
            }
        });

        function fetchCity(value, url_role, flag) {
            var html = '<option value="">Select City *</option>';
            if (value != '' && value.length > 0) {
                var parameters = {};
                parameters.id = value;
                masterJS.hitAjax(url_role + '/' + value, parameters, 'GET', function (success) {
                    console.log('fetchCity', success);
                    var response = JSON.parse(success);
                    $.each(response['data'], function (index, data) {
                        // console.log('each', data, index);
                        html += '<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>';
                    });

                    $('#city').html(html);
                    if(flag == 1){
                        $("#city").val("{{ $userMasterSession['city'] ?? '' }}");
                    }
                    // toastr.success('City list fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchCity', error);
                    $('#city').html(html);
                    $('#city').prev('label span').removeClass('red');
                    var error = JSON.parse(error['responseText']);
                    toastr.error(error['data']['message'], 'Error', toastr.options);
                });
            } else {
                $('#city').html(html);
                toastr.error('Invalid state option selected', 'Error', toastr.options);
            }
        }

        $('#radio2').on('click',function(){
            $('.cash-pay').hide("slow");
            $('.upi-pay').show("slow");
            generateHash();
        });
        $('#radio1').on('click',function(){
            $('.cash-pay').show("slow");
            $('.upi-pay').hide("slow");
        });

        function generateHash()
        {
            ajaxFetch("{{ route($mainUri.'.generate_hash') }}", "#formCheckout", "", "", ".preloader1", true, "post", true,
                function (response) {
                    if(response.code == 200){
                        if(response.type == 'error'){
                            $("#hash").val(response.data.hash);
                            toastr.error(response.message, 'Error', toastr.options);
                        }else if(response.type == 'success'){
                            $("#hash").val(response.data.hash);
                            toastr.success(response.message, 'Success', toastr.options);;
                        }
                    }else{
                        toastr.error('Something went wrong...!', 'Error', toastr.options);
                    }
                    $('.preloader1').hide();
                });
        }
    </script>
@endsection
