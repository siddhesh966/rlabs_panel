@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Article';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    <!--====== PREALOADER PART START ======-->

    <div class="preloader">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>

    <!--====== PREALOADER PART START ======-->

    {{--<section class="blog-section ptb-100">
        <div class="container">
            @include('frontend.sub_section_blog')
        </div>
    </section>--}}

    <!--====== BLOG PART START ======-->

    <section id="blog-part" class="pt-65 pb-65">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center pb-15">
                        <h2>Our Articles</h2>
                        <ul>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                @forelse($blogs as $key => $blog)
                <div class="col-lg-4 col-md-6">
                    <div class="singel-blog mt-30">
                        <div class="blog-thum">
                            <img src="{{ $blog->blogImage}}" alt="Blog">
                            <div class="date text-center">
                                {{--<h3>22</h3>--}}
                                <span>{{ date("F d, Y", strtotime($blog->created_at)) }}</span>
                            </div>
                        </div>
                        <div class="blog-cont pt-25">
                            <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}"><h5>{{ $blog->blogTitle ?? '' }}</h5></a>
                            <p>{{ $blog->blogSubTitle ?? '' }}</p>
                            <a href="{{ route($mainUri.'.blog_details', [$blog->blog_id]) }}">Read More</a>
                        </div>
                    </div>
                </div>
                @empty
                    No Articles Found...!
                @endforelse
            </div>
        </div>
    </section>

    <!--====== BLOG PART ENDS ======-->

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
