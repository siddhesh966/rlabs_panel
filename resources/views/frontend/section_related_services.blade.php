<!-- Related Services start -->
<div class="estimate-section ptb-100 pt-0">
    <div class="container form-wrapper">

        <div class="row">

            <div class="col-lg-12 section-title text-left">
                <span>Related</span>
                <h2>Services</h2>
            </div>

            <div class="col-lg-12">
                <div class="row">

                    @forelse($related_services as $key => $sub_service)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-services">
                                <div class="services-img">
                                    <img src="{{ $sub_service->sub_services_image ?? '' }}" alt="{{ $sub_service->sub_services_title ?? 'No Preview Available' }}">
                                </div>

                                <div class="services-content-wrapper">
                                    <h3><a href="{{ route($mainUri.'.service_details', [$sub_service->sub_service_id]) }}">{{ $sub_service->sub_services_title ?? '' }}</a></h3>
                                    <p>Starting Form Rs. {{ $sub_service->min_services_price ?? '' }}</p>

                                    <div class="services-icon">
                                        <a href="{{ route($mainUri.'.service_details', [$sub_service->sub_service_id]) }}"><i class="fas fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        No Related Services Found...!
                    @endforelse

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Related Services start -->
