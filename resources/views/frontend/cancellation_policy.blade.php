@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'Cancellation Policy';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    <!-- Cancellation Policy start -->
    <div class="container ptb-100">
        <div class="row">
            <div class="col-lg-12 border p-5 rounded">
                <div class="points">
                    <ul>
                        <li>
                            In the event Client needs to cancel a scheduled cleaning appointment, forty-eight (48) hours’ notice to KHFM Hospitality & Facility Management Services Ltd is required. Notice may be given by Email, SMS (text) or Phone. Should Client fail to give forty-eight (48) hours’ notice on more than one (1) occasion, Client must pay 50% for the cancelled cleaning first offence and 100% of the fee for cancelled cleanings thereafter. Cancelling more than three (3) consecutive cleanings or more than seven (7) total scheduled cleanings, without prior approval of KHFM Hospitality & Facility Management Services Ltd, will be deemed a material breach and allow KHFM Hospitality & Facility Management Services Ltd to cancel them contract and/or pricing agreement or to seek legal remedies. In the event that KHFM Hospitality & Facility Management Services Ltd needs to cancel a scheduled cleaning appointment 24 hours’ notice will be given to client.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Cancellation Policy end -->

    {{-- Slogan File --}}
    @include('frontend.section_slogan')

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
@endsection
