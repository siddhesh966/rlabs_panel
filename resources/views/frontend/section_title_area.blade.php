<!-- Start Page Title Area -->
<section class="page-title-area">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="page-title-content">
                    <h1>{{ $mainTitle }}</h1>

                    <ul class="back-to-home">
                        <li><a href="{{ route($mainUri.'.index') }}">Home</a></li>
                        <li>{{ $mainTitle }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Page Title Area -->
