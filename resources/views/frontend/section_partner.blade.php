<!-- Start Partner Area -->
<section class="partner-section ptb-100 gray-bg">
    <div class="container">
        <div class="section-title">
            <span>Support by</span>
            <h2>Business Partner</h2>
            <p>There are variations available majoritaey suffered alteration words which look believable dolor sit amet consectetuer adipiscing.</p>
        </div>

        <div class="partner-wrapper owl-carousel owl-theme">
            <div class="partner-logo">
                <a href="#"><img src="{{ asset('assets/img/brand/brand-1.png') }}" alt="image"></a>
            </div>

            <div class="partner-logo">
                <a href="#"><img src="{{ asset('assets/img/brand/brand-2.png') }}" alt="image"></a>
            </div>

            <div class="partner-logo">
                <a href="#"><img src="{{ asset('assets/img/brand/brand-3.png') }}" alt="image"></a>
            </div>

            <div class="partner-logo">
                <a href="#"><img src="{{ asset('assets/img/brand/brand-4.png') }}" alt="image"></a>
            </div>

            <div class="partner-logo">
                <a href="#"><img src="{{ asset('assets/img/brand/brand-5.png') }}" alt="image"></a>
            </div>
        </div>
    </div>
</section>
<!-- End Partner Area -->
