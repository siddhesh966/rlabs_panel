@php
    $mainUri = Request::segment(1);
    $prefixUri = Request::segment(2);
    if (is_null($prefixUri)) {
        $prefixUri = 'index';
    }
    $mainTitle = 'My Account';
@endphp

@extends('frontend.app')

@section('extraStyles')
@endsection

@section('content')

    {{-- Title Area File --}}
    @include('frontend.section_title_area')

    <!-- Start Account Details Area -->
    <div class="contact-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="errorDiv"></div>
                    <div id="successDiv"></div>
                    <form class="contactForm" name="formUpdateDetails" id="formUpdateDetails" method="post" action="{{ route($mainUri.'.update_account_details') }}">

                        @csrf

                        <input type="hidden" name="user_id" id="user_id" value="{{ $userMasterSession['user_id'] ?? 0 }}" />
                        <input type="hidden" name="user_info_id" id="user_info_id" value="{{ $userMasterSession['user_info_id'] ?? 0 }}" />

                        <div class="form-wrapper mb-0">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="f_name" name="f_name" placeholder="First Name *" value="{{ $userMasterSession['f_name'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="l_name" name="l_name" placeholder="Last Name *" value="{{ $userMasterSession['l_name'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{ $userMasterSession['company_name'] ?? '' }}" />
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address1" name="address1" placeholder="Address Line 1 *">{{ $userMasterSession['address1'] ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="address2" name="address2" placeholder="Address Line 2 *">{{ $userMasterSession['address2'] ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="country_code" name="country_code">
                                            <option value="">Select Country *</option>
                                            <option value="91">India</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="state" name="state" onchange="fetchCity(this.value, '{{ route($mainUri.'.fetch_city') }}', '0')">
                                            <option value="">Select State *</option>
                                            @foreach($state_master as $state)
                                                <option value="{{ $state->state_id }}">
                                                    {{ $state->state_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="city" name="city">
                                            <option value="">Select City *</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode *"  value="{{ $userMasterSession['pincode'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="">Select Gender *</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Mobile Number *" value="{{ $userMasterSession['mobile_no'] ?? '' }}" />
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" class="default-btn">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Account Details Area -->

    {{-- Slogan File --}}
    @include('frontend.section_slogan')

    {{-- Contact-Us Form File --}}
    @include('frontend.section_contact_us')

@endsection

@section('extraHtml')
@endsection

@section('extraScripts')
    <script>
        $("#country_code").val("{{ $userMasterSession['country_code'] ?? '' }}");
        $("#state").val("{{ $userMasterSession['state'] ?? '' }}");
        fetchCity("{{ $userMasterSession['state'] ?? '' }}", '{{ route($mainUri.'.fetch_city') }}', '1');
        $("#gender").val("{{ $userMasterSession['gender'] ?? '' }}");

        $("#formUpdateDetails").validate({
            rules: {
                f_name: "required",
                l_name: "required",
                address1: "required",
                address2: "required",
                country_code: "required",
                state: "required",
                city: "required",
                pincode: "required",
                gender: "required",
                mobile_no: "required",
            },
            messages: {

            },
            submitHandler: function(form) {
                ajaxFetch("{{ route($mainUri.'.update_account_details') }}", "#formUpdateDetails", "#successDiv", "#errorDiv", ".preloader", true, "post");
            }
        });

        function fetchCity(value, url_role, flag) {
            var html = '<option value="">Select City *</option>';
            if (value != '' && value.length > 0) {
                var parameters = {};
                parameters.id = value;
                masterJS.hitAjax(url_role + '/' + value, parameters, 'GET', function (success) {
                    console.log('fetchCity', success);
                    var response = JSON.parse(success);
                    $.each(response['data'], function (index, data) {
                        // console.log('each', data, index);
                        html += '<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>';
                    });

                    $('#city').html(html);
                    if(flag == 1){
                        $("#city").val("{{ $userMasterSession['city'] ?? '' }}");
                    }
                    // toastr.success('City list fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchCity', error);
                    $('#city').html(html);
                    $('#city').prev('label span').removeClass('red');
                    var error = JSON.parse(error['responseText']);
                    toastr.error(error['data']['message'], 'Error', toastr.options);
                });
            } else {
                $('#city').html(html);
                toastr.error('Invalid state option selected', 'Error', toastr.options);
            }
        }
    </script>
@endsection
