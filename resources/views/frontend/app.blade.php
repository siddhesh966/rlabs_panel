<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--====== Title ======-->
    {{--<title>Radiant Laboratories</title>--}}
    <title>{{ $title ?? config('app_config.app_name') }}</title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png')}}" type="image/png">

    <!--====== Owl Carousel css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">

    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css')}}">

    <!--====== Slick css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css')}}">

    <!--====== Nice Select css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css')}}">

    <!--====== Nice Number css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.nice-number.min.css')}}">

    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}">

    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">

    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css')}}">

    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">

    <!--====== Custom css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">

    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">

    <!--====== aos css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">


</head>
    <body>

        {{-- Header File --}}
        @include('frontend.header')

        @yield('content')

        @yield('extraHtml')

        {{-- Footer File --}}
        @include('frontend.footer')

        <script src="{{ asset('assets/js/vendor/modernizr-3.6.0.min.js')}}"></script>
        <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{ asset('assets/js/custome.js')}}"></script>

        <!--====== Bootstrap js ======-->
        <script src="{{ asset('assets/js/popper.min.js')}}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>

        <!--====== Owl Carousel js ======-->
        <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>

        <!--====== Magnific Popup js ======-->
        <script src="{{ asset('assets/js/jquery.magnific-popup.min.js')}}"></script>

        <!--====== Slick js ======-->
        <script src="{{ asset('assets/js/slick.min.js')}}"></script>

        <!--====== Nice Number js ======-->
        <script src="{{ asset('assets/js/jquery.nice-number.min.js')}}"></script>

        <!--====== Nice Select js ======-->
        <script src="{{ asset('assets/js/jquery.nice-select.min.js')}}"></script>

        <!--====== Validator js ======-->
        <script src="{{ asset('assets/js/validator.min.js')}}"></script>

        <!--====== Ajax Contact js ======-->
        <script src="{{ asset('assets/js/ajax-contact.js')}}"></script>

        <!--====== Main js ======-->
        <script src="{{ asset('assets/js/main.js')}}"></script>

        <!--====== Google Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="{{ asset('assets/js/map-script.js')}}"></script>


        <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script src="{{ asset('js/toastr.min.js') }}"></script>
        <script src="{{ asset('js/toastr_options.js') }}"></script>
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

        @yield('extraScripts')

        <script>
            (function ($) {
                $.fn.countTo = function (options) {
                    options = options || {};

                    return $(this).each(function () {
                        // set options for current element
                        var settings = $.extend({}, $.fn.countTo.defaults, {
                            from:            $(this).data('from'),
                            to:              $(this).data('to'),
                            speed:           $(this).data('speed'),
                            refreshInterval: $(this).data('refresh-interval'),
                            decimals:        $(this).data('decimals')
                        }, options);

                        // how many times to update the value, and how much to increment the value on each update
                        var loops = Math.ceil(settings.speed / settings.refreshInterval),
                            increment = (settings.to - settings.from) / loops;

                        // references & variables that will change with each update
                        var self = this,
                            $self = $(this),
                            loopCount = 0,
                            value = settings.from,
                            data = $self.data('countTo') || {};

                        $self.data('countTo', data);

                        // if an existing interval can be found, clear it first
                        if (data.interval) {
                            clearInterval(data.interval);
                        }
                        data.interval = setInterval(updateTimer, settings.refreshInterval);

                        // initialize the element with the starting value
                        render(value);

                        function updateTimer() {
                            value += increment;
                            loopCount++;

                            render(value);

                            if (typeof(settings.onUpdate) == 'function') {
                                settings.onUpdate.call(self, value);
                            }

                            if (loopCount >= loops) {
                                // remove the interval
                                $self.removeData('countTo');
                                clearInterval(data.interval);
                                value = settings.to;

                                if (typeof(settings.onComplete) == 'function') {
                                    settings.onComplete.call(self, value);
                                }
                            }
                        }

                        function render(value) {
                            var formattedValue = settings.formatter.call(self, value, settings);
                            $self.text(formattedValue);
                        }
                    });
                };

                $.fn.countTo.defaults = {
                    from: 0,               // the number the element should start at
                    to: 0,                 // the number the element should end at
                    speed: 1000,           // how long it should take to count between the target numbers
                    refreshInterval: 100,  // how often the element should be updated
                    decimals: 0,           // the number of decimal places to show
                    formatter: formatter,  // handler for formatting the value before rendering
                    onUpdate: null,        // callback method for every time the element is updated
                    onComplete: null       // callback method for when the element finishes updating
                };

                function formatter(value, settings) {
                    return value.toFixed(settings.decimals);
                }
            }(jQuery));

            jQuery(function ($) {

                // start all the timers
                $('.timer').each(count);

                // restart a timer when a button is clicked
                $( window ).scroll(function () {console.log($(window).scrollTop());
                    if($(window).scrollTop() > 300 && $(window).scrollTop() < 850)
                    {
                        $('.timer').each(count);
                    }
                });

                function count(options) {
                    var $this = $(this);
                    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                    $this.countTo(options);
                }
            });
        </script>

        <script>
            $("#formLeads").validate({
                rules: {
                    lead_name: "required",
                    lead_email: "required",
                    lead_mobile: "required",
                    lead_message: "required",
                },
                messages: {

                },
                submitHandler: function(form) {
                    ajaxFetch("{{ route($mainUri.'.save_leads') }}", "#formLeads", "", "", ".preloader", true, "post", true,
                        function (response) {
                            if(response.code == 200){
                                if(response.type == 'error'){
                                    toastr.error(response.message, 'Error', toastr.options);
                                }else if(response.type == 'success'){
                                    toastr.success(response.message, 'Success', toastr.options);
                                    $("#lead_name").val("");
                                    $("#lead_email").val("");
                                    $("#lead_mobile").val("");
                                    $("#lead_message").val("");
                                }
                            }else{
                                toastr.error('Something went wrong...!', 'Error', toastr.options);
                            }
                            $('.preloader').hide();
                        });
                }
            });

            //feedback form
            $("#feedLeads").validate({
                rules: {
                    feed_name: "required",
                    feed_desg: "required",
                    feed_org: "required",
                    feed_address: "required",
                    email: "required",
                    feed_phone: "required",
                    example: "required",
                    feed_long: "required",
                    example2: "required",
                    example3: "required",
                    feed_oserv: "required",
                    feed_emp: "required",
                    feed_comm: "required",
                    feed_pcrl: "required",
                },
                messages: {

                },
                submitHandler: function(form) {
                    ajaxFetch("{{ route($mainUri.'.save_feedback') }}", "#feedLeads", "", "", ".preloader", true, "post", true,
                        function (response) {
                            if(response.code == 200){
                                if(response.type == 'error'){
                                    toastr.error(response.message, 'Error', toastr.options);
                                }else if(response.type == 'success'){
                                    toastr.success(response.message, 'Success', toastr.options);
                                    $("#feed_name").val("");
                                    $("#feed_desg").val("");
                                    $("#feed_org").val("");
                                    $("#feed_address").val("");
                                    $("#email").val("");
                                    $("#feed_phone").val("");
                                    $("#example").val("");
                                    $("#feed_long").val("");
                                    $("#example2").val("");
                                    $("#example3").val("");
                                    $("#feed_oserv").val("");
                                    $("#feed_emp").val("");
                                    $("#feed_comm").val("");
                                    $("#feed_pcrl").val("");
                                }
                            }else{
                                toastr.error('Something went wrong...!', 'Error', toastr.options);
                            }
                            $('.preloader').hide();
                        });
                }
            });

            // Subscribe form
            $(".newsletter-form").validator().on("submit", function (event) {
                if (event.isDefaultPrevented()) {
                    // handle the invalid form...
                    formErrorSub();
                    submitMSGSub(false, "Please enter your email correctly.");
                } else {
                    // everything looks good!
                    event.preventDefault();
                    ajaxFetch("{{ route($mainUri.'.save_email_subscription') }}", "#formEmailSubscription", "", "", ".preloader", true, "post", true,
                        function (response) {
                            if(response.code == 200){
                                if(response.type == 'error'){
                                    formErrorSub();
                                    toastr.error(response.message, 'Error', toastr.options);
                                }else if(response.type == 'success'){
                                    formSuccessSub();
                                    toastr.success(response.message, 'Success', toastr.options);
                                }
                            }else{
                                formErrorSub();
                                toastr.error('Something went wrong...!', 'Error', toastr.options);
                            }
                            $('.preloader').hide();
                        });
                }
            });

            function submitMSGSub(valid, msg){
                if(valid){
                    var msgClasses = "validation-success";
                } else {
                    var msgClasses = "validation-danger";
                }
                $("#validator-newsletter").removeClass().addClass(msgClasses).text(msg);
            }

            function formSuccessSub(){
                $(".newsletter-form")[0].reset();
                submitMSGSub(true, "Thank you for subscribing!");
                setTimeout(function() {
                    $("#validator-newsletter").addClass('hide');
                }, 4000)
            }

            function formErrorSub(){
                $(".newsletter-form").addClass("animated shake");
                setTimeout(function() {
                    $(".newsletter-form").removeClass("animated shake");
                }, 1000)
            }
        </script>

        <script>
            $(function(){
                $(".ratings_stars").on("click", function(){
                    var $this = $(this); //  assign $(this) to $this
                    var ratingValue = $this.val();  // use '.val()' as shown here
                    alert(ratingValue);
                });
            });
        </script>

    </body>
</html>

