@php

    $mainPath = Request::segment(1);

    $page_title = 'App User';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraScripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$detail->user_id]) }}" method="post"
                                  data-parsley-validate enctype="multipart/form-data">

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="f_name">First Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="f_name" name="f_name"
                                               value="{{ $detail->f_name }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User first name field is required."
                                               placeholder="Enter first name">
                                    </div>

                                    <div class="form-group">
                                        <label for="l_name">Last Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="l_name" name="l_name"
                                               value="{{ $detail->l_name }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User last name field is required."
                                               placeholder="Enter last name">
                                    </div>

                                    <div class="form-group">
                                        <label for="company_name">Company Name
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="text" class="form-control"
                                               id="company_name" name="company_name"
                                               value="{{ $detail->company_name }}"
                                               {{--data-parsley-required="true"--}}
                                               {{--data-parsley-required-message="User name field is required."--}}
                                               placeholder="Enter company name">
                                    </div>

                                    <div class="form-group">
                                        <label for="mobile_no">Mobile Number
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="mobile_no" name="mobile_no"
                                               value="{{ $detail->mobile_no }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User mobile number field is required."
                                               placeholder="Enter mobile number">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email
                                            <span class="red">*</span>
                                        </label>
                                        <input type="email" class="form-control"
                                               id="email" name="email"
                                               value="{{ $detail->email }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User email id field is required."
                                               placeholder="Enter user email id">
                                    </div>

                                    <div class="form-group">
                                        <label for="gender">Gender
                                            <span class="red">*</span>
                                        </label>
                                        <select id="gender" name="gender" class="form-control"
                                                data-parsley-required="true"
                                                data-parsley-required-message="User gender field is required.">

                                            <option value="">Select gender option</option>
                                            <option value="Male" @if ($detail->gender == 'Male') selected @endif>Male
                                            </option>
                                            <option value="Female" @if ($detail->gender == 'Female') selected @endif>
                                                Female
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control"
                                               id="image" name="image" accept="image/*"
                                                {{--data-parsley-required="true"--}}
                                                {{--data-parsley-required-message="User profile image field is required."--}}
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Profile Image Preview
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <div>
                                            <img src="{{ $detail->image }}" alt="user profile"
                                                 style="width: 200px;height: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
