@php

    $mainPath = Request::segment(1);

    $page_title = 'App User';
    $title = Config::get('app_config.app_name') . " | $page_title List";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} List
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">{{ $page_title }} List</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                                @include('layouts.success_error')

                                <div class="box-tools">
                                    <div class="input-group-btn">
                                        @permission('create.'.$mainPath)
                                        <a href="{{ route($mainPath.'.create') }}"
                                           class="btn btn-md btn-default btn-outline-primary">
                                            <i class="fa fa-plus"></i> Add {{ $page_title }}
                                        </a>
                                        @endpermission
                                    </div>
                                </div>

                            </div>

                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Full Name</th>
                                        <th>Company Name</th>
                                        <th>Mobile Number</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                    @forelse($details as $key => $detail)
                                        <tr>
                                            <td>{{ ($details->currentpage()-1) * $details->perpage() + $key + 1 }}</td>
                                            <td>{{ $detail->f_name.' '.$detail->l_name }}</td>
                                            <td>{{ $detail->company_name }}</td>
                                            <td>{{ $detail->mobile_no }}</td>
                                            <td>{{ $detail->email }}</td>
                                            <td>{{ $detail->gender }}</td>
                                            <td>
                                                @permission('update.'.$mainPath)
                                                <select id="{{ $detail->user_id }}" class="btn btn-default status-change">
                                                    <option value="1" @if ($detail->status == 1) selected @endif>
                                                        Active
                                                    </option>
                                                    <option value="0" @if ($detail->status == 0) selected @endif>
                                                        Inactive
                                                    </option>
                                                </select>
                                                @else
                                                    @if ($detail->status == 1) Active
                                                    @else Inactive
                                                    @endif
                                                    @endpermission
                                            </td>
                                            <td>
                                                @permission('update.'.$mainPath)
                                                <a href="{{ route($mainPath.'.edit',[$detail->user_id]) }}"
                                                   class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Edit</a>
                                                @endpermission

                                                @permission('delete.'.$mainPath)
                                                <a href="javascript:void(0);"
                                                   id="{{ $detail->user_id }}"
                                                   class="btn btn-sm btn-danger delete-record">
                                                    <i class="fa fa-trash-o"></i>
                                                    Delete</a>
                                                @endpermission
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="8" class="text-center">No Record Found!</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>

                            <div class="box-footer clearfix">
                                {{ $details->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <script type="text/javascript">
        var delete_url = '{{ route($mainPath.'.index') }}';
        $('.box-footer').find('.pagination').addClass('pagination-md no-margin pull-right');
    </script>
    <script src="{{ asset('js/status_change.js') }}"></script>
@endsection
