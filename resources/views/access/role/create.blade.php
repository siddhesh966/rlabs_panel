@php

    $mainPath = Request::segment(1);

    $page_title = 'Role';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <form role="form" action="{{ route($mainPath.'.store') }}" method="post"
                                  data-parsley-validate>

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="name" name="name"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Role name field is required."
                                               placeholder="Enter role name">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Roles</label>

                                        @forelse($menus as $menu)
                                            <p>
                                                <label style="width: 250px;">
                                                    <input type="checkbox" name="slugs[{{$menu['id']}}]"
                                                           id="menuitem_{{ $menu['id'] }}"
                                                           value="{{ $menu['route_prefix'] }}">
                                                    {{ $menu['menu_name'] }}
                                                </label>

                                                <label class="padding-right-20">
                                                    <input class="roles_checkbox"
                                                           value="1" type="checkbox"
                                                           name="create[{{$menu['id']}}]"
                                                           id="create_{{ $menu['id'] }}">
                                                    Add
                                                </label>

                                                <label class="padding-right-20">
                                                    <input class="roles_checkbox"
                                                           value="2" type="checkbox"
                                                           name="update[{{$menu['id']}}]"
                                                           id="update_{{ $menu['id'] }}">
                                                    Edit
                                                </label>

                                                <label class="padding-right-20">
                                                    <input class="roles_checkbox"
                                                           value="3" type="checkbox"
                                                           name="delete[{{$menu['id']}}]"
                                                           id="delete_{{ $menu['id'] }}">
                                                    Delete
                                                </label>
                                            </p>
                                        @empty
                                            <label for="">No menu found!</label>
                                        @endforelse
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script>
        $("input[id^='menuitem_']").click(function () {
            var id = $(this).attr('id').split('_')[1];
            $('#create_' + id).prop('checked', $(this).prop('checked'));
            $('#update_' + id).prop('checked', $(this).prop('checked'));
            $('#delete_' + id).prop('checked', $(this).prop('checked'));
        });

        $('.roles_checkbox').click(function () {
            var ids = $(this).attr('id').split('_')[1];
            var checkedCount = [];
            $("input[id$='_" + ids + "']").each(function () {
                if ($(this).attr('id').indexOf('menuitem_') === -1) {
                    checkedCount.push($(this).prop('checked'));
                }
            });

            $('#menuitem_' + ids).prop('checked', ($.inArray(false, checkedCount) !== -1 ? false : true));
        });
    </script>
@endsection
