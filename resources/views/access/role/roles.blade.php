<label for="">Permissions</label>

@forelse($menus as $menu)
    <p>
        <label style="width: 150px;">
            <input class="roles_checkbox" disabled
                   type="checkbox" name="slugs[{{$menu['id']}}]"
                   id="menuitem_{{ $menu['id'] }}"
                   {{ (isset($permissions[$menu['route_prefix']])) ? 'checked' : '' }}
                   value="{{ $menu['route_prefix'] }}">
            {{ $menu['menu_name'] }}
        </label>

        <label>
            <input class="roles_checkbox" disabled
                   value="1" type="checkbox"
                   name="create[{{$menu['id']}}]"
                   {{ (isset($permissions[$menu['route_prefix']]['create']) && $permissions[$menu['route_prefix']]['create'] == true) ? 'checked' : '' }}
                   id="create_{{ $menu['id'] }}">
            Add
        </label>

        <label>
            <input class="roles_checkbox" disabled
                   value="2" type="checkbox"
                   name="update[{{$menu['id']}}]"
                   {{ (isset($permissions[$menu['route_prefix']]['update']) && $permissions[$menu['route_prefix']]['update'] == true) ? 'checked' : '' }}
                   id="update_{{ $menu['id'] }}">
            Edit
        </label>

        <label>
            <input class="roles_checkbox" disabled
                   value="3" type="checkbox"
                   name="delete[{{$menu['id']}}]"
                   {{ (isset($permissions[$menu['route_prefix']]['delete']) && $permissions[$menu['route_prefix']]['delete'] == true) ? 'checked' : '' }}
                   id="delete_{{ $menu['id'] }}">
            Delete
        </label>
    </p>
@empty
    <label for="">No menu found!</label>
@endforelse
