@php

    $mainPath = Request::segment(1);

    $page_title = 'User';
    $title = Config::get('app_config.app_name') . " | $page_title Edit";

@endphp

@extends('layouts.app')

@section('extraScripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Edit
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Edit</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route($mainPath.'.update',[$user->id]) }}" method="post"
                                  data-parsley-validate>

                                @method('PUT')
                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Name
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="name" name="name" value="{{ $user->name }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User name field is required."
                                               placeholder="Enter user name">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email
                                            <span class="red">*</span>
                                            (
                                            <small><input type="checkbox" name="except_email">Select checkbox to
                                                not validate email id unique check
                                            </small>
                                            )
                                        </label>
                                        <input type="email" class="form-control"
                                               id="email" name="email" value="{{ $user->email }}"
                                               data-parsley-required="true"
                                               data-parsley-required-message="User email id field is required."
                                               placeholder="Enter user email id">
                                    </div>

                                    <div class="form-group">
                                        <label for="role">Role
                                            <span class="red">*</span>
                                        </label>
                                        <select id="role" name="role" class="form-control"
                                                data-parsley-required="true" onchange="fetchRoles(this)"
                                                data-parsley-required-message="User role field is required.">
                                            <option value="">Select role option</option>
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}"
                                                        @if ($user->role_id == $role->id) selected @endif>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group" id="user_roles"></div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>

    <script type="text/javascript">
        var url_role = '{{ route('role.roles') }}';

        function fetchRoles(obj) {
            var html = '';
            if (obj.value != '' && obj.value.length > 0) {
                var parameters = {};
                parameters.id = obj.value;
                masterJS.hitAjax(url_role, parameters, 'POST', function (success) {
                    console.log('fetchRoles', success);

                    var response = JSON.parse(success);
                    html = response.data.html;
                    $('#user_roles').html(html);
                    toastr.success('Role details fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchRoles', error);

                    $('#user_roles').html(html);
                    toastr.error('Invalid role option selected', 'Error', toastr.options);
                });
            } else {
                $('#user_roles').html(html);
                toastr.error('Invalid role option selected', 'Error', toastr.options);
            }
        }
    </script>
@endsection
