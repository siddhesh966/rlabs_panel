@extends('layouts.app')

@section('extraStyles')
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <div class="form-box" id="login-box">
        <div class="header">Sign In</div>
        <form action="{{ route('login-submit') }}"
              method="post" data-parsley-validate>

            @csrf

            @include('layouts.success_error')

            <div class="body bg-gray">

                <div class="form-group">
                    <label for="email" class="col-form-label text-md-right">E-Mail Address</label>
                    <input type="email" id="email" name="email" value="{{ old('email') }}"
                           data-parsley-required="true"
                           data-parsley-required-message="Email address field is required."
                           class="form-control" placeholder="User ID"/>
                </div>

                <div class="form-group">
                    <label for="email" class="col-form-label text-md-right">Password</label>
                    <input type="password" id="password" name="password"
                           data-parsley-required="true"
                           data-parsley-required-message="Password field is required."
                           class="form-control" placeholder="Password"/>
                </div>

                <div class="form-group col-sm-6 pull-left" style="padding-left: 0px;">
                    <label for="remember_me">
                        <input type="checkbox" id="remember_me" name="remember_me"/> Remember me
                    </label>
                </div>

                <div class="form-group col-sm-6 pull-right">
                    <a href="{{ route('password.request') }}">I forgot my password</a>
                </div>
            </div>

            <div class="footer">
                <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
            </div>

        </form>
    </div>

@endsection

@section('extraScripts')
    <script type="text/javascript" src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
