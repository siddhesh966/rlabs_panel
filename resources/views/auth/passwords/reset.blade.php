@extends('layouts.app')

@section('extraStyles')
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <div class="form-box" id="login-box">
        <div class="header">Reset Password</div>

        <form action="{{ route('password.update') }}"
              method="post" data-parsley-validate>

            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="body bg-gray">

                <div class="form-group">
                    <label for="email" class="col-form-label text-md-right">E-Mail Address</label>
                    <input type="email" id="email"
                           name="email" class="form-control"
                           data-parsley-required="true"
                           data-parsley-required-message="Email address field is required."/>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password" class="col-form-label text-md-right">Password</label>
                    <input type="password" id="password"
                           name="password" class="form-control"
                           data-parsley-required="true"
                           data-parsley-required-message="Password field is required."/>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password" class="col-form-label text-md-right">Confirm Password</label>
                    <input type="password" id="password_confirmation"
                           name="password_confirmation" class="form-control"
                           data-parsley-required="true"
                           data-parsley-required-message="Confirm password field is required."/>
                </div>

            </div>

            <div class="footer">
                <button type="submit" class="btn bg-olive btn-block">Reset Password</button>
            </div>

        </form>
    </div>

@endsection

@section('extraScripts')
    <script type="text/javascript" src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
