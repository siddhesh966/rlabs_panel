@php

    $page_title = 'Change Password';
    $title = Config::get('app_config.app_name') . " | $page_title";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }}
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">{{ $page_title }}</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <form role="form" action="{{ route('password.change.request') }}" method="post"
                                  data-parsley-validate>

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="old_password">Old Password
                                            <span class="red">*</span>
                                        </label>
                                        <input type="password" class="form-control"
                                               id="old_password" name="old_password"
                                               data-parsley-required="true"
                                               data-parsley-trigger="keyup focusout"
                                               data-parsley-required-message="Old password field is required."
                                               placeholder="Enter Old Password">
                                    </div>

                                    <div class="form-group">
                                        <label for="new_password">New Password
                                            <span class="red">*</span>
                                        </label>
                                        <input type="password" class="form-control"
                                               id="new_password" name="new_password"
                                               data-parsley-required="true"
                                               data-parsley-trigger="keyup focusout"
                                               data-parsley-required-message="New password field is required."
                                               placeholder="Enter New Password">
                                    </div>

                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password
                                            <span class="red">*</span>
                                        </label>
                                        <input type="password" class="form-control"
                                               id="confirm_password" name="confirm_password"
                                               data-parsley-required="true"
                                               data-parsley-trigger="keyup focusout"
                                               data-parsley-equalto="#new_password"
                                               data-parsley-required-message="Confirm password field is required."
                                               data-parsley-equalto-message="Confirm password field is not matched with new password field."
                                               placeholder="Enter Confirm Password">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>

    <script type="text/javascript">
        var url_role = '{{ route('role.roles') }}';

        function fetchRoles(obj) {
            var html = '';
            if (obj.value != '' && obj.value.length > 0) {
                var parameters = {};
                parameters.id = obj.value;
                masterJS.hitAjax(url_role, parameters, 'POST', function (success) {
                    console.log('fetchRoles', success);

                    var response = JSON.parse(success);
                    html = response.data.html;
                    $('#user_roles').html(html);
                    toastr.success('Role details fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchRoles', error);

                    $('#user_roles').html(html);
                    toastr.error('Invalid role option selected', 'Error', toastr.options);
                });
            } else {
                $('#user_roles').html(html);
                toastr.error('Invalid role option selected', 'Error', toastr.options);
            }
        }
    </script>
@endsection
