@extends('layouts.app')

@section('extraStyles')
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <div class="form-box" id="login-box">
        <div class="header">Reset Password</div>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form action="{{ route('password.email') }}"
              method="post" data-parsley-validate>

            @csrf

            <div class="body bg-gray">

                <div class="form-group">
                    <label for="email" class="col-form-label text-md-right">E-Mail Address</label>
                    <input type="email" id="email"
                           name="email" class="form-control"
                           data-parsley-required="true"
                           data-parsley-required-message="Email address field is required."/>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="footer">
                <button type="submit" class="col-md-7 btn bg-olive pull-left">Send Password Reset Link</button>
                <a href="{{ route('login') }}" class="col-md-4 btn bg-teal pull-right">Back To Login</a>
            </div>

        </form>
    </div>
@endsection

@section('extraScripts')
    <script type="text/javascript" src="{{ asset('js/parsley.min.js') }}"></script>
@endsection
