@php

    $mainPath = Request::segment(1);

    $page_title = 'Orders';
    $title = Config::get('app_config.app_name') . " | $page_title List";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} List
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">{{ $page_title }} List</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                                @include('layouts.success_error')

                                <div class="box-tools">
                                    {{--<div class="input-group-btn">--}}
                                    {{--@permission('create.'.$mainPath)--}}
                                    {{--<a href="{{ route($mainPath.'.create') }}"--}}
                                    {{--class="btn btn-md btn-default btn-outline-primary">--}}
                                    {{--<i class="fa fa-plus"></i> Add {{ $page_title }}--}}
                                    {{--</a>--}}
                                    {{--@endpermission--}}
                                    {{--</div>--}}
                                </div>

                            </div>

                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Order No</th>
                                        <th>Payment Type</th>
                                        <th>Payment Amount</th>
                                        <th>Payment Date</th>
                                        <th>Action</th>
                                    </tr>

                                    @forelse($details as $key => $detail)
                                        <tr>
                                            <td>{{ ($details->currentpage()-1) * $details->perpage() + $key + 1 }}</td>
                                            <td>{{ $detail->order_number ?? '' }}</td>
                                            <td>{{ $detail->order_type == 1 ? 'Cash On Delivery' : 'Online Payment' }}</td>
                                            <td>{{ $detail->total_price ?? '' }}</td>
                                            <td>{{ \Carbon\Carbon::parse($detail->created_at)->format('d-M-Y') ?? '' }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success view_details"
                                                   data-json="{{ base64_encode(json_encode($detail)) }}"><i
                                                        class="fa fa-info-circle"></i> View</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-center">No Record Found!</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>

                            <div class="box-footer clearfix">
                                {{ $details->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>

    <!-- Modal -->
    <div id="viewDetails" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order Details</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive-md">
                        <table class="table table-bordered">
                            <tr>
                                <td>User Name</td>
                                <td id="user_name"></td>
                            </tr>
                            <tr>
                                <td>Company Name</td>
                                <td id="company_name"></td>
                            </tr>
                            <tr>
                                <td>Order Number</td>
                                <td id="order_no"></td>
                            </tr>
                            <tr>
                                <td>Order Amount</td>
                                <td id="order_amt"></td>
                            </tr>
                            <tr>
                                <td>Service Date</td>
                                <td id="order_execution_date"></td>
                            </tr>
                            <tr>
                                <td>Service Time</td>
                                <td id="order_execution_time"></td>
                            </tr>
                            <tr>
                                <td>Service Address</td>
                                <td id="order_execution_address"></td>
                            </tr>
                            <tr>
                                <td>Service Contact Number</td>
                                <td id="order_execution_contact"></td>
                            </tr>
                            <tr>
                                <td>Order Created Date Time</td>
                                <td id="order_date_time"></td>
                            </tr>
                            <tr>
                                <td>Order Payment Type</td>
                                <td id="order_payment_type"></td>
                            </tr>
                            <tr>
                                <td>Order Service</td>
                                <td id="order_service"></td>
                            </tr>
                            <tr>
                                <td>Order Sub-Service</td>
                                <td id="order_sub_service"></td>
                            </tr>
                            <tr>
                                <td>Order Service Type</td>
                                <td id="order_service_type"></td>
                            </tr>
                        </table>
                    </div>

                </div>
                {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                {{--</div>--}}
            </div>

        </div>
    </div>

@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <script type="text/javascript">
        var delete_url = '{{ route($mainPath.'.index') }}';
        $('.box-footer').find('.pagination').addClass('pagination-md no-margin pull-right');

        $('.view_details').click(function (e) {
            e.preventDefault();

            var data = JSON.parse(window.atob($(this).attr('data-json')));
            if (data.length <= 0) {
                toastr.error('Unable to fetch order details.', 'Error', toastr.options);
                return false;
            }

            var address = data.user_infos[0].address1 + ", " + data.user_infos[0].address2 + ", " + data.user_infos[0].city_master.city_name;
            address += " " + data.user_infos[0].state_master.state_name + " " + data.user_infos[0].pincode;

            $('#user_name').text(data.user_master.f_name + ' ' + data.user_master.l_name);
            $('#company_name').text(data.user_master.company_name);
            $('#order_no').text(data.order_number);
            $('#order_amt').text(data.total_price);
            $('#order_date_time').text(data.created_at);
            $('#order_execution_date').text(data.order_detail_masters[0].order_date);
            $('#order_execution_time').text(data.order_detail_masters[0].order_time);
            $('#order_execution_address').text(data.address === null ? 'NA' : address);
            $('#order_execution_contact').text(data.user_master.mobile_no);
            $('#order_payment_type').text((data.order_type == 1) ? 'Cash On Delivery' : 'Online Payment');
            $('#order_service').text(data.order_detail_masters[0].service_master.services_title);
            $('#order_sub_service').text(data.order_detail_masters[0].sub_service.sub_services_title);
            $('#order_service_type').text(data.order_detail_masters[0].services_area_price.services_type);

            $('#viewDetails').modal('show');
        })
    </script>
    <script src="{{ asset('js/status_change.js') }}"></script>
@endsection
