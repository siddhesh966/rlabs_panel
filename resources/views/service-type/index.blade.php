@php

    $mainPath = Request::segment(1);

    $page_title = 'Service Type';
    $title = Config::get('app_config.app_name') . " | $page_title List";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} List
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">{{ $page_title }} List</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                                @include('layouts.success_error')

                                <div class="box-tools" style="width: 25%;float: left;">
                                    {{--<div class="input-group-btn">--}}
                                        @permission('create.'.$mainPath)
                                        <a href="{{ route($mainPath.'.create') }}"
                                           class="btn btn-md btn-default btn-outline-primary">
                                            <i class="fa fa-plus"></i> Add {{ $page_title }}
                                        </a>
                                        @endpermission
                                    {{--</div>--}}
                                </div>
                                <div class="box-tools">

                                    <form action="{{ route($mainPath.'.index') }}" method="GET"
                                          style="width: 75%;float: right;display: flex;justify-content: flex-end;">

                                        <input type="text" name="services_title" id="services_title"
                                               class="form-control" style="width: 30%;float: left;margin-right: 10px;"
                                               value="{{ $_REQUEST['services_title'] ?? null }}"
                                               placeholder="Search By Service Title">

                                        <input type="text" name="sub_services_title" id="sub_services_title"
                                               class="form-control" style="width: 30%;float: left;margin-right: 10px;"
                                               value="{{ $_REQUEST['sub_services_title'] ?? null }}"
                                               placeholder="Search By Sub-Service Title">

                                        <input type="text" name="services_type" id="services_type"
                                               class="form-control" style="width: 30%;float: left;margin-right: 10px;"
                                               value="{{ $_REQUEST['services_type'] ?? null }}"
                                               placeholder="Search By Service Type">

                                        <input type="submit" style="float: left;" class="btn btn-danger">
                                        <a href="{{ route($mainPath.'.index') }}" style="float: left;margin-left: 10px;"
                                           class="btn btn-primary">Reset</a>
                                    </form>
                                </div>

                            </div>

                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Service Name</th>
                                        <th>Sub-Service Name</th>
                                        <th>Service Type Name</th>
                                        <th>Service Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                    @forelse($details as $key => $detail)
                                        <tr>
                                            <td>{{ ($details->currentpage()-1) * $details->perpage() + $key + 1 }}</td>
                                            <td>{{ $detail->service_master->services_title ?? '' }}</td>
                                            <td>{{ $detail->sub_service->sub_services_title ?? '' }}</td>
                                            <td>{{ $detail->services_type }}</td>
                                            <td>{{ $detail->services_price }}</td>
                                            <td>
                                                @permission('update.'.$mainPath)
                                                <select id="{{ $detail->services_type_id }}" class="btn btn-default status-change">
                                                    <option value="1" @if ($detail->status == 1) selected @endif>
                                                        Active
                                                    </option>
                                                    <option value="0" @if ($detail->status == 0) selected @endif>
                                                        Inactive
                                                    </option>
                                                </select>
                                                @else
                                                    @if ($detail->status == 1) Active
                                                    @else Inactive
                                                    @endif
                                                    @endpermission
                                            </td>
                                            <td>
                                                @permission('update.'.$mainPath)
                                                <a href="{{ route($mainPath.'.edit',[$detail->services_type_id]) }}"
                                                   class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Edit</a>
                                                @endpermission

                                                @permission('delete.'.$mainPath)
                                                <a href="javascript:void(0);"
                                                   id="{{ $detail->services_type_id }}"
                                                   class="btn btn-sm btn-danger delete-record">
                                                    <i class="fa fa-trash-o"></i>
                                                    Delete</a>
                                                @endpermission
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">No Record Found!</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>

                            <div class="box-footer clearfix">
                                {{ $details->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <script type="text/javascript">
        var delete_url = '{{ route($mainPath.'.index') }}';
        $('.box-footer').find('.pagination').addClass('pagination-md no-margin pull-right');
    </script>
    <script src="{{ asset('js/status_change.js') }}"></script>
@endsection
