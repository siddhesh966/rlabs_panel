@php

    $mainPath = Request::segment(1);

    $page_title = 'Service Type';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <form role="form" action="{{ route($mainPath.'.store') }}" method="post"
                                  data-parsley-validate enctype="multipart/form-data">

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="services_id">Service
                                            <span class="red">*</span>
                                        </label>
                                        <select class="form-control" name="services_id" id="services_id"
                                                data-parsley-required="true" onchange="fetchSubService(this)"
                                                data-parsley-required-message="Service field is required"
                                        >

                                            <option value="">select service option</option>
                                            @foreach($services as $service)
                                                <option value="{{ $service->services_id }}">{{ $service->services_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="sub_services_id">Sub-Service
                                            <span class="red">*</span>
                                        </label>
                                        <select class="form-control" name="sub_services_id" id="sub_services_id"
                                                data-parsley-required="true"
                                                data-parsley-required-message="Sub-service field is required"
                                        >

                                            <option value="">select sub-service option</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="services_type">Service Type
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="services_type" name="services_type"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Service type field is required."
                                               placeholder="Enter service type">
                                    </div>

                                    <div class="form-group">
                                        <label for="services_price">Service Price
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="services_price" name="services_price"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Service price field is required."
                                               placeholder="Enter service price">
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>

    <script type="text/javascript">
        var url_role = '{{ route('fetch.sub-services') }}';

        function fetchSubService(obj) {
            var html = '<option value="">select sub-service option</option>';
            if (obj.value != '' && obj.value.length > 0) {
                var parameters = {};
                parameters.id = obj.value;
                masterJS.hitAjax(url_role + '/' + obj.value, parameters, 'GET', function (success) {
                    console.log('fetchSubService', success);

                    var response = JSON.parse(success);
                    $.each(response['data'], function (index, data) {
                        console.log('each', data, index);
                        html += '<option value="' + data['sub_service_id'] + '">' + data['sub_services_title'] + '</option>';
                    });

                    $('#sub_services_id').html(html);
                    toastr.success('Sub-Service list fetch successfully.', 'Success', toastr.options);
                }, function (error) {
                    console.error('fetchSubService', error);

                    $('#sub_services_id').html(html);
                    $('#sub_services_id').prev('label span').removeClass('red');
                    var error = JSON.parse(error['responseText']);
                    toastr.error(error['data']['message'], 'Error', toastr.options);
                });
            } else {
                $('#sub_services_id').html(html);
                toastr.error('Invalid sub-service option selected', 'Error', toastr.options);
            }
        }
    </script>
@endsection
