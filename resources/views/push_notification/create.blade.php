@php

    $mainPath = Request::segment(1);

    $page_title = 'Title';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dropdown.min.css') }}">--}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <form role="form" action="{{ route($mainPath.'.store') }}" method="post"
                                  enctype="multipart/form-data" data-parsley-validate>

                                @csrf

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Title
                                            <span class="red">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                               id="title" name="title"
                                               data-parsley-required="true"
                                               data-parsley-required-message="Notification title field is required."
                                               placeholder="Enter notification title">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Body
                                            <span class="red">*</span>
                                        </label>
                                        <textarea class="form-control" id="body"
                                                  name="body" cols="30" rows="2"
                                                  data-parsley-required="true"
                                                  data-parsley-required-message="Notification body field is required."
                                                  placeholder="Enter notification body"
                                        ></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Image
                                            {{--<span class="red">*</span>--}}
                                        </label>
                                        <input type="file" class="form-control"
                                               accept="image/*"
                                               data-parsley-fileextension="jpeg,jpg,png"
                                               data-parsley-trigger="change"
                                               id="image" name="image">
                                    </div>

                                    <div class="form-group">
                                        <label for="all_app_users">Send To All Users
                                            <input type="checkbox" id="all_users" name="all_users"
                                                   style="margin-left: 10px !important;"></label>

                                        <select class="form-control users select2" name="users[]"
                                                multiple="multiple" style="width: 100% !important;">
                                            <option value="">Please select users</option>
                                            @foreach($userMaster as $key => $data)
                                                <option value="{{$data['user_id']}}">{{$data['f_name']." ".$data['l_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="box-footer">
                                        <a href="{{ route($mainPath.'.index') }}" class="btn btn-default">Cancel</a>
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    {{--    <script src="{{ asset('js/jquery.dropdown.min.js') }}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2();
        });

        $(document).on('click', '#all_users', function () {
            let allUsersChecked = $('input[name="all_users"]:checked').val();
            console.log(allUsersChecked);
            if (allUsersChecked == 'on') {
                $('.users').attr('disabled', 'true');
            } else {
                $('.users').removeAttr('disabled');
            }
        })

        window.Parsley
            .addValidator('fileextension', function (value, requirement) {
                return requirement.split(',').indexOf(value.split('.').pop()) !== -1;
            })
            .addMessage('en', 'fileextension', 'The extension doesn\'t match the required');
    </script>

@endsection
