@php

    $mainPath = Request::segment(1);

    $page_title = 'Push notification';
    $title = Config::get('app_config.app_name') . " | $page_title List";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} List
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">{{ $page_title }} List</li>
                </ol>
            </section>

            <section class="contentTable">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                                @include('layouts.success_error')

                                <div class="box-tools">
                                    <div class="row">
                                        <div class="col-xs-4 col-md-3">
                                            {{--                                            @permission('create.'.$mainPath)--}}
                                            <a href="{{ route($mainPath.'.create') }}"
                                               class="btn btn-md btn-default btn-outline-primary" style="width:100%">
                                                <i class="fa fa-plus"></i> Add {{ $page_title }}
                                            </a>
                                            {{--                                            @endpermission--}}
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="box-body table-responsive no-padding table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Title</th>
                                        <th>Body</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                    @forelse($details as $key => $data)
                                        <tr class="tr">
                                            <td>{{ ($details->currentpage()-1) * $details->perpage() + $key + 1 }}</td>
                                            <td>{{ $data->title }}</td>
                                            <td>{{ $data->body }}</td>
                                            <td>
                                                @permission('update.'.$mainPath)
                                                <select id="{{ $data->title_id }}"
                                                        class="btn btn-default status-change">
                                                    <option value="1" @if ($data->status == 1) selected @endif>
                                                        Active
                                                    </option>
                                                    <option value="0" @if ($data->status == 0) selected @endif>
                                                        Inactive
                                                    </option>
                                                </select>
                                                @else
                                                    @if ($data->status == 1) Active
                                                    @else Inactive
                                                    @endif
                                                    @endpermission
                                            </td>
                                            <td>
                                                <div style="width: 100px;">
                                                    <a href="{{ route($mainPath.'.show',[$data->id]) }}"
                                                       data-toggle="tooltip" title="View Notification"
                                                       class="btn btn-sm btn-success"><i class="fa fa-eye"></i>
                                                    </a>
                                                </div>

                                                {{--@permission('update.'.$mainPath)
                                                <a href="{{ route($mainPath.'.edit',[$data->title_id]) }}"
                                                   class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Edit</a>
                                                @endpermission--}}
                                                {{--@permission('delete.'.$mainPath)
                                                <a href="javascript:void(0);"
                                                   id="{{ $data->title_id }}"
                                                   class="btn btn-sm btn-danger delete-record">
                                                    <i class="fa fa-trash-o"></i>Delete
                                                </a>
                                                @endpermission--}}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">No Record Found!</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>

                            <div class="box-footer clearfix">
                                {{ $details->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/toastr_options.js') }}"></script>
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript">
        var delete_url = '{{ route($mainPath.'.index') }}';
        $('.box-footer').find('.pagination').addClass('pagination-md no-margin pull-right');
    </script>
    <script src="{{ asset('js/status_change.js') }}"></script>
@endsection
