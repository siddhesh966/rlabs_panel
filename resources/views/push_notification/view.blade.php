@php

    $mainPath = Request::segment(1);

    $page_title = 'Title';
    $title = Config::get('app_config.app_name') . " | $page_title Create";

@endphp

@extends('layouts.app')

@section('extraStyles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dropdown.min.css') }}">--}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')

    @include('layouts.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                @include('layouts.user_panel')

                @include('layouts.navigation')
            </section>
        </aside>


        <aside class="right-side">
            <section class="content-header">
                <h1>
                    {{ $page_title }} Create
                    <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('home.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route($mainPath.'.index') }}"><i class="fa fa-user"></i> {{ $page_title }} List</a>
                    </li>
                    <li class="active">{{ $page_title }} Create</li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-primary">

                            @include('layouts.success_error')

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Title: </label>
                                    {{$data->title}}
                                </div>

                                <div class="form-group">
                                    <label for="email">Body: </label>
                                    {{$data->body}}
                                </div>

                                @if(!is_null($data->image))
                                    <div class="form-group">
                                        <label for="name">Image: </label>
                                        <img src="{{$data->image}}" alt="" height="100" width="auto">
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="all_app_users">User List: </label>
                                    @foreach($notificationUsers as $key => $data)
                                        <span class="badge badge-secondary">{{$data['f_name']." ".$data['l_name']}}</span>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    <a href="{{ route($mainPath.'.index') }}"
                                       data-toggle="tooltip" title="Back"
                                       class="btn btn-sm btn-danger">Back
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </section>

            @include('layouts.footer')
        </aside>
    </div>
@endsection

@section('extraScripts')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    {{--    <script src="{{ asset('js/jquery.dropdown.min.js') }}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script type="text/javascript">
    </script>

@endsection
