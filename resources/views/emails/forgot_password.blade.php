<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-sm-offset-2">
            <h3>Welcome, {{ $full_name }}</h3>
            <br><br>
            <p class="desc">Your six-digit forgot password verification code is <strong>{{ $code }}</strong>. </p>
            <p class="desc">Use this code to reset your account password.</p>
            <br><br>
            <p><b>Thanks & Regards,</b></p>
            <p>{{ $company_name }}</p>
        </div>
    </div>
</div>
