<?php

return [
    'app_name' => 'Radiant Laboratories',
    'app_version' => '0.0.1',
    'app_company' => 'Radiant Laboratories',
    'app_company_website' => '#',
    'powered_by' => 'Appectual IT Solutions',
    'powered_by_website' => 'http://www.appectual.com',
    'fcm_key' => [
        'user' => 'AAAAziSvUIA:APA91bHnZtTlXqo5Z_FFBLVp6qFztL9TgoJEPFYoVzuD2U--uex9ukA5F_96CYvnertdLuEypNuTx3aYwMG9_-FnbQ__htOhXJe49DlBt-qnNyprpBAR_4d0OCBUDu0J_K8HCWRP6PaB'
    ],
];
