<?php

function responseJson($code = 200, $message = 'success', $data = [])
{
    return response()->json([
        'code' => $code,
        'message' => $message,
        'data' => $data
    ], $code);
}

function redirectBackWithErrors($errors)
{
    return redirect()->back()->withInput()->withErrors($errors);
}

function redirectBackWithSuccess($success)
{
    return redirect()->back()->with('success', ucfirst($success));
}

function routeWithSuccess($route, $success)
{
    return redirect()->route($route)->with('success', ucfirst($success));
}

function routeWithErrors($route, $error)
{
    return redirect()->route($route)->withErrors(ucfirst($error));
}

function responseJsonAjax($code = 200, $type = "success", $message = "", $view = "", $errors = [], $success = [], $data = [])
{
    return response()->json([
        'code' => $code,
        'type' => $type,
        'message' => $message,
        'view' => $view,
        'errors' => $errors,
        'success' => $success,
        'data' => $data,
    ], $code);
}

