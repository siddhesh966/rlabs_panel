<?php


function beginTransaction()
{
    \DB::beginTransaction();
}

function commit()
{
    \DB::commit();
}

function rollback()
{
    \DB::rollBack();
}

function generateMenusContent()
{
    if (Auth::user()->role_id == 1) {

        $newMenu = \App\Models\Menu::whereStatus(1)
            ->get()->toArray();

    } else {
        $permission = array_keys(Auth::user()->getPermissions());

        $parentId = \App\Models\Menu::whereStatus(1)
            ->whereIn('route_prefix', $permission)
            ->distinct('parent_id')
            ->pluck('parent_id')->toArray();

        array_push($parentId, 1);

        $newMenu = \App\Models\Menu::whereStatus(1)
            ->whereNested(function ($q) use ($permission, $parentId) {
                $q->whereIn('id', $parentId);
                $q->orWhereIn('route_prefix', $permission);
            })
            ->get()->toArray();
    }

    $newMenuArray = array();
    foreach ($newMenu as $menu) {
        if ($menu['parent_id'] == 0) {
            $newMenuArray[$menu['id']] = [
                "menu_name" => $menu['menu_name'],
                "route_name" => $menu['route_name'],
                "route_prefix" => $menu['route_prefix'],
                "icon" => $menu['icon'],
                "sub_menu" => []
            ];
        } else {
            array_push($newMenuArray[$menu['parent_id']]['sub_menu'], [
                "menu_name" => $menu['menu_name'],
                "route_name" => $menu['route_name'],
                "route_prefix" => $menu['route_prefix'],
                "icon" => $menu['icon'],
            ]);
        }
    }

    return $newMenuArray;
}

function getUserPermision()
{
    $role = [];
    if (!empty(Auth::user()->getPermissions())) {
        foreach (Auth::user()->getPermissions() as $key => $value) {
            array_push($role, $key);
        }
    }

    return $role;
}

function checkBelongsToMenusActive($route_prefix, $sub_menus)
{
    return searcharray($route_prefix, 'route_prefix', $sub_menus);
}

function searcharray($value, $key, $array)
{
    foreach ($array as $k => $val) {
        if ($val[$key] == $value) {
            return $k;
        }
    }
    return null;
}

function getGUIDnoHash()
{
    mt_srand((double)microtime() * 10000);
    $charid = md5(uniqid(rand(), true));
    $c = unpack("C*", $charid);
    $c = implode("", $c);
    return substr($c, 0, 15);
}

function time_elapsed_string($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function in_range($number, $min, $max, $inclusive = FALSE)
{
    if (is_int($number) && is_int($min) && is_int($max))
    {
        return $inclusive
            ? ($number >= $min && $number <= $max)
            : ($number > $min && $number < $max) ;
    }

    return FALSE;
}

function calculateCartPriceDetails($cart)
{
    if(!empty($cart))
    {
        $subTotal = 0;
        foreach($cart as $key => $singleService)
        {
            $totalPrice = $singleService['services_price'] * $singleService['service_quantity'];
            $subTotal = $subTotal + $totalPrice;
        }

        $gst = round($subTotal * 0.18);
        $finalTotal = $subTotal + $gst;

        $data = [
            'subTotal' => $subTotal,
            'gst' => $gst,
            'finalTotal' => $finalTotal,
        ];

        return $data;
    }

    return false;
}

function checkNull($value) {
    if ($value == null or $value == "") {
        return '';
    } else {
        return $value;
    }
}


