<?php

namespace App\Helpers\Classes;

use Illuminate\Support\Facades\Storage;

class Storages
{
    public static function getDisk()
    {
        return Storage::disk('s3');
    }

    public static function getS3DiskPath()
    {
        return 'https://s3.' . config('filesystems.disks.s3.region') . '.amazonaws.com/' . config('filesystems.disks.s3.bucket') . '/';
    }

    public static function checkDirectoryCreate($directory)
    {
        $s3 = self::getDisk();
        if (!$s3->exists($directory)) {
            $s3->makeDirectory($directory);
        }
    }

    public static function uploadFile($file, $file_name, $file_path)
    {
        $s3 = self::getDisk();
        $filePath = $file_path . '/' . $file_name;

        self::checkDirectoryCreate($file_path);
        if (!$s3->put($filePath, fopen($file, 'r+'), 'private')) {
            return ['error' => true, 'message' => 'Error occurred on uploading file to s3 bucket.'];
        }

        return ['error' => false];
    }

    public static function getFilePath($file_name)
    {
        if (!self::getDisk()->exists(self::getS3DiskPath() . $file_name)) {
            return ['error' => true, 'message' => 'File not found on s3 bucket.'];
        }

        return ['error' => false, 'data' => self::getS3DiskPath() . $file_name];
    }

    public static function getSignedFilePath($resource, $expire_minutes)
    {
        $disk = self::getDisk();
        if ($disk->exists($resource)) {
            $command = $disk->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
                'Bucket' => config('filesystems.disks.s3.bucket'),
                'Key' => $resource,
                //'ResponseContentDisposition' => 'attachment;'//for download
            ]);

            return (string)$disk->getDriver()->getAdapter()->getClient()
                ->createPresignedRequest($command, "+{$expire_minutes} minutes")->getUri();
        }

    }

    public static function deleteFile($file_path)
    {
        $disk = self::getDisk();
        if ($disk->exists($file_path)) {
            if ($disk->delete($file_path)) {
                return ['error' => false];
            }

            return ['error' => true, 'message' => 'Error has occurred to delete s3 bucket file.'];
        }

        return ['error' => true, 'message' => 'File not available in s3 bucket.'];
    }

}
