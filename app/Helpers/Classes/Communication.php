<?php

namespace App\Helpers\Classes;


use Mail;

class Communication
{
    public static function fcm_send($api_key, $notification_ids, $title, $body, $image = '', $data = [], $icon = '', $sound = '', $color = '')
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $notification = [
            'title' => $title,
            'body' => $body,
            'icon' => $icon != '' ? $icon : 'logo',
            'sound' => $sound != '' ? $sound : 'default',
            'color' => $color != '' ? $color : '#ffffff'
        ];

        if ($image != '' && strlen($image)) {
            $notification['image'] = $image;
        }

        $extra_data = [
            'click_action' => "PUSH_INTENT"
        ];

        foreach ($data as $key => $value) {
            $extra_data[$key] = $value;
        }

        $fields = [
            'registration_ids' => is_string($notification_ids) ? [$notification_ids] : $notification_ids,
            'notification' => $notification,
            'data' => $extra_data,
            'priority' => 'normal'
        ];

        $headers = ['Authorization: key=' . $api_key, 'Content-Type: application/json'];

        $ch = self::init_curl();
        if ($ch === false) {

        }

        self::hit_curl($ch, $url, $fields, $headers);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $info;
    }

    public static function sms_send($mobile_no, $message)
    {
        $user = "appectual";
        $password = "Appectual@11";
        $mobilenumbers = "91" . $mobile_no;
//        $mobilenumbers = "919167581946";
        $senderid = "SMSCountry";
        $messagetype = "N";
        $DReports = "Y";
        $url = "http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
        $message = urlencode($message);

        $ch = self::init_curl();
        if (!$ch) {
            return ['http_code' => 500, 'message' => 'Couldn\'t initialize a cURL handle.'];
        }

        $fields = "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports";
        self::hit_curl($ch, $url, $fields);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $info;
    }

//    public static function email_send($email, $name, $message)
//    {
//        $details = [
//            'name' => $name,
//            'subject' => 'OTP Verification Code',
//            'body' => $message,
//        ];
//
//        Mail::to($email)->send(new \App\Mail\MailToCustomer($details));
//        if (Mail::failures()) {
//            return responseJson(500, 'Failed to send mail.');
//        }
//
//        return responseJson(200, 'Successfully mail sent.');
//    }

    public static function email_send($email_id, $subject, $body, $view, $arrAttachmentFiles = [], $arrCcEmailIds = [])
    {
        $details = [
            'subject' => $subject,
            'body' => $body,
            'view' => $view,
            'arrAttachmentFiles' => $arrAttachmentFiles,
            'arrCcEmailIds' => $arrCcEmailIds,
        ];

        try{
            Mail::to($email_id)->send(new \App\Mail\SendMail($details));
        }catch(\Exception $e){

        }

        if (Mail::failures()) {
            return responseJson(500, 'Failed to send mail.');
        }

        return responseJson(200, 'Mail sent successfully.');
    }

    public static function init_curl()
    {
        $ch = curl_init();
        if (!$ch) {
            return false;
        }

        return $ch;
    }

    public static function hit_curl($ch, $url, $fields, $headers = [])
    {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        if (is_string($fields)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }
        if (is_array($fields) && !empty($fields)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        }

        return curl_exec($ch);
    }

    public static function save_logs($table, $user_id, $type, $params)
    {
        if (class_exists('\App\Models\LogsMaster')) {
            \App\Models\LogsMaster::create([
                'table_name' => $table,
                'user_id' => $user_id,
                'type' => $type,
                'params_json' => $params,
            ]);
        }
    }
}
