<?php

namespace App\Helpers\Classes;


class Functional
{
    public static function fetchLatLongByAddress($key, $address)
    {
        $prepAddr = str_replace(' ', '+', $address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false&key=' . $key);
        $output = json_decode($geocode);
        $newData['latitude'] = $output->results[0]->geometry->location->lat;
        $newData['longitude'] = $output->results[0]->geometry->location->lng;

        return $newData;
    }

    public static function fetchTwoPointDistance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public static function insertDataToTable($models, $array, $json_column = '', $json_data = [])
    {
//        dd('insert', $array);
        $array['status'] = 1;
        if ($json_column !== '') {
            $array[$json_column] = !empty($json_data) ? $json_data : null;
        }

        $insert = $models::create($array);

        if (!$insert) {
            return false;
        }

        return true;
    }

    public static function updateDataToTable($models, $conditions, $array, $status, $json_column = '', $json_data = [])
    {
//        dd('update', $array);
        if (empty($conditions)) {
            return false;
        }
        $update = $models::query();
        foreach ($conditions as $key => $condition) {
            $update = $update->where($condition, $array[$condition]);
        }
        $array['status'] = $status;
        if ($json_column !== '') {
            $array[$json_column] = !empty($json_data) ? $json_data : null;
        }
        $update = $update->update($array);
        if (!$update) {
            return false;
        }

        return true;
    }
}
