<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthJWTRefreshToken extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        try {
            $newToken = $this->auth->setRequest($request)->parseToken()->refresh();
        } catch (TokenExpiredException $e) {
            return $this->respond('tymon.jwt.expired', 'token expired', $e->getStatusCode(), [$e], 'error');
        } catch (JWTException $e) {
            return $this->respond('tymon.jwt.invalid', 'token invalid', $e->getStatusCode(), [$e], 'access denied');
        }

        // send the refreshed token back to the client
        $response->headers->set('Authorization', 'Bearer '.$newToken);

        return $response;
    }

    protected function respond($event, $error, $status, $payload = [], $status_message = '')
    {
        $response = $this->events->fire($event, $payload, true);

//        return $response ?: $this->response->json(['error' => $error], $status);
        return $response ?: responseJson($status, $status_message, [
            'message' => ucfirst($error)
        ]);
    }
}
