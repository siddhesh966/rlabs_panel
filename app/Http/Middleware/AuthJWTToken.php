<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$token = $this->auth->setRequest($request)->getToken()) {
            return $this->respond('tymon.jwt.absent', 'token not provided', 400, [], 'bad request');
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return $this->respond('tymon.jwt.expired', 'token expired', $e->getStatusCode(), [$e], 'error');
        } catch (JWTException $e) {
            return $this->respond('tymon.jwt.invalid', 'token invalid', $e->getStatusCode(), [$e], 'access denied');
        }

        if (!$user) {
            return $this->respond('tymon.jwt.user_not_found', 'user not found', 404, [], 'Not Found');
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }

    protected function respond($event, $error, $status, $payload = [], $status_message = '')
    {
        $response = $this->events->fire($event, $payload, true);

//        return $response ?: $this->response->json(['error' => $error], $status);
        return $response ?: responseJson($status, $status_message, [
            'message' => ucfirst($error)
        ]);
    }
}
