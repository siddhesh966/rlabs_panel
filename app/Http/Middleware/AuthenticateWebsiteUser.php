<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class AuthenticateWebsiteUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userMasterSession = \Session::get('userMasterSession');
        if (is_null($userMasterSession)) {
            return Redirect::to('website/login');
        }

        return $next($request);
    }
}
