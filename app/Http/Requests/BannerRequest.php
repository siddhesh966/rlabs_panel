<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BannerRequest extends FormRequest
{
    private $table = 'banner_master';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $banner_title = 'required';
        if ($this->method() == 'PUT') {
            $banner_title = [
                'required',
                Rule::unique($this->table)->ignore($request->segment(2), 'banner_id')
            ];
        } else {
            $banner_title .= '|unique:' . $this->table;
        }

        return [
            'banner_title' => $banner_title,
            'banner_description' => '',
            'banner_image' => 'required|mimes:jpeg,jpg,png',
        ];
    }
}
