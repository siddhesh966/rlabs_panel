<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AppUserRequest extends FormRequest
{
    private $table = 'user_master';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $email_rule = 'required';
        $mobile_rule = 'required|numeric';
        $password = 'required';
        $image = 'required';
        if ($this->method() == 'PUT') {
            $email_rule = [
                'required',
                'email',
                Rule::unique($this->table)->ignore($request->segment(2), 'user_id')
            ];
            $mobile_rule = [
                'required',
                'numeric',
                Rule::unique($this->table)->ignore($request->segment(2), 'user_id')
            ];
            $password = '';
            $image = '';
        } else {
            $email_rule .= '|unique:' . $this->table;
            $mobile_rule .= '|unique:' . $this->table;
        }

        return [
            'f_name' => 'required',
            'l_name' => 'required',
            'mobile_no' => $mobile_rule,
            'email' => $email_rule,
            'password' => $password,
            'gender' => 'required',
            'image' => $image,
        ];
    }
}
