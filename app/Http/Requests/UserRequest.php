<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    private $table = 'users';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $email_rule = 'required';
        $password_rule = 'required';
        if ($this->method() == 'PUT') {
            if ($request->has('except_email') && $request->input('except_email') == 'on') {
                $email_rule .= '|email|unique:' . $this->table . ',email,:id';
            } else {
                $email_rule = '';
            }
            $password_rule = '';
        } else {
            $email_rule .= '|email|unique:' . $this->table;
            $password_rule .= '|min:8';
        }

        return [
            'name' => 'required',
            'email' => $email_rule,
            'password' => $password_rule,
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'user name field is required.',
            'email.required' => 'user email id field is required.',
            'email.email' => 'user email id field is invalid email format.',
            'email.unique' => 'user email id field is already used.',
            'password.required' => 'user password field is required.',
            'password.min' => 'user password field must be 8 character in length.',
        ];
    }
}
