<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8|same:new_password',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.required' => 'Old password field is required.',
            'new_password.required' => 'New password field is required.',
            'new_password.min' => 'New password field must be 8 character in length.',
            'confirm_password.required' => 'Confirm password field is required.',
            'confirm_password.min' => 'Confirm password field must be 8 character in length',
            'confirm_password.same' => 'New password and confirm password should be same.',
        ];
    }
}
