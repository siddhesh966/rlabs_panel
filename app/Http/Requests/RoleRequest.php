<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    private $table = 'roles';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $name_rule = 'required';
        if ($this->method() == 'PUT') {
            $name_rule .= '|unique:' . $this->table . ',name,:id';
        } else {
            $name_rule .= '|unique:' . $this->table;
        }

        return [
            'name' => $name_rule,
            'slugs' => 'required|array',
            'create' => 'required|array',
            'update' => 'required|array',
            'delete' => 'required|array',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'role name field is required.',
            'name.unique' => 'role name is already used.',
            'slugs.required' => 'role menu name field is required.',
            'slugs.array' => 'role menu name is invalid data type.',
            'create.required' => 'role add of menu permission is required.',
            'create.array' => 'role add of menu permission is invalid data type.',
            'update.required' => 'role update of menu permission is required.',
            'update.array' => 'role update of menu permission is invalid data type.',
            'delete.required' => 'role delete of menu permission is required.',
            'delete.array' => 'role delete of menu permission is invalid data type.',
        ];
    }
}
