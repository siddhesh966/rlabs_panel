<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class WeServeRequest extends FormRequest
{
    private $table = 'we_serve_master';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $title = 'required';
        if ($this->method() == 'PUT') {
            $title = [
                'required',
                Rule::unique($this->table)->ignore($request->segment(2), 'we_serve_master_id')
            ];
        } else {
            $title .= '|unique:' . $this->table;
        }

        return [
            'title' => $title,
            'description' => '',
            'serve_image' => 'required|mimes:jpeg,jpg,png',
        ];
    }
}
