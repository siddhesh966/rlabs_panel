<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BlogRequest extends FormRequest
{
    private $table = 'blog_master';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $blogTitle = 'required';
        if ($this->method() == 'PUT') {
            $blogTitle = [
                'required',
                Rule::unique($this->table)->ignore($request->segment(2), 'blog_id')
            ];
        } else {
            $blogTitle .= '|unique:' . $this->table;
        }

        return [
            'blogTitle' => $blogTitle,
            'blogDesc' => '',
            'blogImage' => 'required|mimes:jpeg,jpg,png',
        ];
    }
}
