<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SubServiceRequest extends FormRequest
{
    private $table = 'sub_services';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $services_title = 'required';
        $image = 'required';
        if ($this->method() == 'PUT') {
            $services_title = [
                'required',
                Rule::unique($this->table)->ignore($request->segment(2), 'sub_service_id')
            ];
            $image = '';
        } else {
            $services_title .= '|unique:' . $this->table;
        }

        return [
            'services_id' => 'required|numeric',
            'sub_services_title' => $services_title,
            'sub_services_description' => 'required',
            'sub_services_image' => $image,
        ];
    }
}
