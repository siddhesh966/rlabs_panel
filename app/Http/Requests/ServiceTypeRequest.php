<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ServiceTypeRequest extends FormRequest
{
    private $table = 'services_area_price';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $service_type = 'required';
        if ($this->method() == 'PUT') {
            $service_type = [
                'required',
                Rule::unique($this->table)->ignore($request->segment(2), 'services_type_id')
                    ->ignore($request->input('services_type'), 'services_type')
            ];
        } else {
            $service_type = [
                'required',
                Rule::unique($this->table)->ignore($request->input('services_id'), 'services_id')
                    ->ignore($request->input('sub_services_id'), 'sub_services_id')
            ];
        }
        return [
            'services_id' => 'required|numeric',
            'sub_services_id' => 'required|numeric',
            'services_type' => $service_type,
            'services_price' => 'required|numeric',
        ];
    }
}
