<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PromoRequest extends FormRequest
{
    private $table = 'promo_code_master';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $name_rule = 'required';
        if ($this->method() == 'PUT') {
            $name_rule .= '|unique:' . $this->table . ',promo_code_name,:id,promo_code_id';
        } else {
            $name_rule .= '|unique:' . $this->table;
        }

        return [
            'promo_code_name' => $name_rule,
            'promo_code_discount' => 'required|numeric',
            'promo_code_discount_type' => 'required|in:1,2',
            'start_date' => 'required|date_format:d-m-Y',
            'end_date' => 'required|date_format:d-m-Y|after:start_date',
        ];
    }
}
