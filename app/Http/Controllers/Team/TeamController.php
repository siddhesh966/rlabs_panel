<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Team;
use Validator;
use DB;
use Image;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DB::enableQueryLog(); // Enable Query Log...
        $details = Team::whereNull('deleted_at')
            ->orderBy('sequence', 'asc')
            ->paginate(10);
        // dd(DB::getQueryLog()); // Show Results Of Query Log...

        return view('team.index', compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'emp_name' => 'required',
            'emp_designation' => 'required',
            'emp_desc' => 'required',
            //'emp_profile_filename' => 'required',
            'sequence' => 'nullable|numeric'
        ];

        $messages = [
            'emp_name.required' => 'Name field is required.',
            'emp_designation.required' => 'Designation field is required.',
            'emp_desc' => 'Description required',
            //'emp_profile_filename.required' => 'Profile image field is required.',
            'sequence.numeric' => 'Sequence field must be numeric.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $data = $request->except('emp_profile_filename');

        if($request->hasFile('emp_profile_filename')){
            if(!$request->file('emp_profile_filename')->isValid()){
                return redirect()->back()->withInput()->withErrors('Invalid file.');
            }
            $file = $request->file('emp_profile_filename');
            $path = 'team_images';

            $fileName = save_file($file, $path);

            if(!$fileName){
                return redirect()->back()->withInput()->withErrors('Unable to save file.');
            }

            $data['emp_profile_filename'] = $fileName;
        }

        $data['status'] = 1;

        $create = Team::create($data);
        if (!$create) {
            //delete_file($fileName, $path);
            return redirectBackWithErrors('Unable to create details.');
        }

        return routeWithSuccess('team.index', 'details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Team::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }
        $status->status = $status->status === 0 ? 1 : 0;
        if (!$status->save()) {
            return responseJson(500, 'Unable to update status of this record.');
        }

        return responseJson(200, 'Status updated successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = Team::find($id);
        if (is_null($details)) {
            return redirectBackWithErrors('Record not found.');
        }

        return view('team.edit', compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'emp_name' => 'required',
            'emp_designation' => 'required',
            'emp_desc' => 'required',
            'sequence' => 'nullable|numeric'
        ];

        $messages = [
            'emp_name.required' => 'Name field is required.',
            'emp_designation.required' => 'Designation field is required.',
            'emp_desc' => 'Description required',
            'sequence.numeric' => 'Sequence field must be numeric.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $details = Team::find($id);
        if (is_null($details)) {
            return redirectBackWithErrors('Record not found.');
        }

        $details->emp_name = $request->input('emp_name');
        $details->emp_designation = $request->input('emp_designation');
        $details->required = $request->input('emp_desc');
        $details->sequence = $request->input('sequence');

        $oldFile=$details->emp_profile_filename;
        $path = 'team_images';

        if($request->hasFile('emp_profile_filename')){
            if(!$request->file('emp_profile_filename')->isValid()){
                return redirect()->back()->withInput()->withErrors('Invalid file.');
            }

            $file = $request->file('emp_profile_filename');
            $fileName = save_file($file, $path);

            if(!$fileName){
                return redirect()->back()->withInput()->withErrors('Unable to save file.');
            }

            $details->emp_profile_filename = $fileName;
            delete_file($oldFile, $path);
        }

        if (!$details->save()) {
            delete_file($fileName, $path);
            return redirectBackWithErrors('Unable to update team member details.');
        }
        return routeWithSuccess('team.index', 'Team member details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Team::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Record deleted successfully.');
    }
}
