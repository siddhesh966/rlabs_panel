<?php

namespace App\Http\Controllers\PushNotification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PushNotificationLog;
use App\Models\UserMaster;

class PushNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = PushNotificationLog::paginate(10);
        return view('push_notification.index', compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userMaster = UserMaster::select(['user_id', 'f_name', 'l_name', 'device_token'])
            ->whereStatus(1)
            ->whereNull('deleted_at')
            ->get()->toArray();

        return view('push_notification.create', compact('userMaster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $body = $request->input('body');
        $allUsers = $request->input('all_users');
        $userIds = !is_null($request->input('users')) ? $request->input('users') : [];

        $storeData = [
            'title' => $title,
            'body' => $body,
        ];

        $allUsersData = UserMaster::select(['user_id', 'device_token'])
            ->whereStatus(1)
            ->whereNull('deleted_at')
            ->where(function ($q) use ($allUsers, $userIds) {
                if ($allUsers != 'on') {
                    $q->whereIn('user_id', !is_null($userIds) ? $userIds : []);
                }
            })
            ->get()->toArray();

        $allUsersIds = array_column($allUsersData, 'user_id');
        $allUsersToken = array_column($allUsersData, 'device_token');

        if (!empty($allUsersToken)) {
            $storeData['user_ids'] = "|".implode("|",$allUsersIds)."|";
            // CommunicationHelper::fcm_send(config('app_config.fcm_key.user'), $allUsersToken, $title, $body);
        }

        if ($request->has('image')) {
            $file = $request->file('image');
            $file_name = mt_rand('1111', '9999') . '_' . time() . '.' . $file->getClientOriginalExtension();

            $response = StorageHelper::uploadFile($file, $file_name, 'push_notification');
            if ($response['error']) {
                //return redirectBackWithErrors('Unable to store push notification image.');
            } else {
                $storeData['image'] = 'push_notification/' . $file_name;
            }
        }

        $createLog = PushNotificationLog::create($storeData);

        if (!$createLog) {
            return redirectBackWithErrors('Unable to store push notification data.');
        }

        return routeWithSuccess('push-notification.index', 'Push notification send successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PushNotificationLog::where('id', $id)
            ->first();

        if (is_null($data)) {
            return redirectBackWithErrors('Data not found.');
        }

        $notificationUsers = [];

        if (!is_null($data->user_ids)) {
            $arrUserIds=array_filter(explode("|", $data->user_ids));
            $notificationUsers = UserMaster::select(['user_id', 'f_name', 'l_name'])
                ->whereIn('user_id', $arrUserIds)
                ->get()->toArray();
        }

        return view('push_notification.view', [
            'data' => $data,
            'notificationUsers' => $notificationUsers,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
