<?php

namespace App\Http\Controllers\Access;

use Carbon\Carbon;
use App\Models\UserMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppUserRequest;
use App\Http\Controllers\ImageUploadController;

class AppUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = UserMaster::whereNull('deleted_at')
            ->paginate(15);

        return view('access.app-user.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('access.app-user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppUserRequest $request)
    {
        $file_name = null;
        $file_path = 'users/profile';
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store user profile image.');
            }
        } else {
            return redirectBackWithErrors('Invalid user profile image.');
        }

        $data = $request->except(['_token', 'image']);
        $data['password'] = bcrypt($request->input('password'));
        $data['image'] = $file_path . '/' . $file_name;

        $create = UserMaster::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to store user details.');
        }

        return routeWithSuccess('app-user.index', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = UserMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = UserMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit app-user details.");
        }

        return view('access.app-user.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AppUserRequest $request, $id)
    {
        $detail = UserMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit app-user details.");
        }

        $detail->f_name = $request->input('f_name');
        $detail->l_name = $request->input('l_name');
        $detail->company_name = $request->input('company_name');
        $detail->mobile_no = $request->input('mobile_no');
        $detail->email = $request->input('email');
        $detail->gender = $request->input('gender');

        $old_image = $detail->image;
        $file_name = '';
        $file_path = 'users/profile';
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store user profile image.');
            }

            $detail->image = $file_path . '/' . $file_name;
        }

        if (!$detail->save()) {
            if ($file_name != '') {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return redirectBackWithErrors('Unable to update user details.');
        }

        ImageUploadController::deleteFile($file_path, str_replace($file_path . '/', '', $old_image));
        return routeWithSuccess('app-user.index', 'User details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = UserMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
