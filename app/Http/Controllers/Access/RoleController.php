<?php

namespace App\Http\Controllers\Access;

use Auth;
use Carbon\Carbon;
use App\Models\Menu;
use App\RoleModels\Role;
use App\RoleModels\Permission;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = Role::whereNotNull('user_id')
            ->whereNull('deleted_at')
            ->paginate(15);

        return view('access.role.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = getUserPermision();

        if (empty($role) || !is_array($role)) {
            return redirectBackWithErrors('No roles found.');
        }

        $menus = Menu::whereStatus(1)
            ->wherePermission(1)
            ->whereIn('route_prefix', $role)
            ->get()->toArray();

        return view('access.role.create', compact(
            'menus'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $roles = $request->only(['name']);
        $roles['slug'] = str_replace(' ', '_', strtolower($roles['name']));
        $roles['description'] = $roles['name'] . ' Roles';
        $roles['user_id'] = Auth::user()->id;

        $slugs = $request->input('slugs');
        $permission_create = $request->input('create');
        $permission_update = $request->input('update');
        $permission_delete = $request->input('delete');
        $permission_ids = [];

        beginTransaction();

        $role = new Role();
        $roleCreate = $role->create($roles);
        if (!$roleCreate) {
            rollback();
            return redirectBackWithErrors("Unable to create role please try again");
        }

        foreach ($slugs as $key => $value) {

            $permission_data = [
                'name' => $value,
                'slug' => [
                    'create' => isset($permission_create[$key]),
                    'view' => true,
                    'update' => isset($permission_update[$key]),
                    'delete' => isset($permission_delete[$key]),
                ],
                'description' => $value . ' permission'
            ];

            $permission = new Permission();
            $permissionCreate = $permission->create($permission_data);
            if (!$permissionCreate) {
                rollback();
                return redirectBackWithErrors("Unable to add permission please try again");
            }

            $permission_ids[] = $permissionCreate->getKey();
        }

        $assignPermission = $roleCreate->assignPermission($permission_ids);

        if (!$assignPermission) {
            rollback();
            return redirectBackWithErrors("Unable to add permission to role. Please try again.");
        }

        commit();
        return routeWithSuccess('role.index', "Role added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Role::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        if (is_null($role)) {
            return redirectBackWithErrors("Unable to edit user permission please try again");
        }

        $permissions = $role->getPermissions();
        $userPer = getUserPermision();

        if (empty($userPer) || !is_array($userPer)) {
            return redirectBackWithErrors('user role not found');
        }

        $menus = Menu::whereStatus(1)
            ->wherePermission(1)
            ->whereIn('route_prefix', $userPer)
            ->get();

        return view('access.role.edit', compact(
            'role',
            'permissions',
            'menus'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $role = Role::find($id);
        if (is_null($role)) {
            return redirectBackWithErrors("Unable to edit user permission please try again");
        }

        $slugs = $request->input('slugs');
        $permission_create = $request->input('create');
        $permission_update = $request->input('update');
        $permission_delete = $request->input('delete');

        $permission_ids = [];

        beginTransaction();

        $role->name = $request->input('name');
        $role->slug = str_replace(' ', '_', strtolower($request->input('name')));
        $role->description = $request->input('name') . ' Roles';

        if ($role->save()) {

            $role->revokeAllPermissions();

            foreach ($slugs as $key => $value) {

                $permission_data = [
                    'name' => $value,
                    'slug' => [
                        'create' => isset($permission_create[$key]),
                        'view' => true,
                        'update' => isset($permission_update[$key]),
                        'delete' => isset($permission_delete[$key]),
                    ],
                    'description' => $value . ' permission'
                ];

                $permission = new Permission();
                $permissionCreate = $permission->create($permission_data);

                if (!$permissionCreate) {
                    rollback();
                    return redirectBackWithErrors("Unable to add permission please try again");
                }

                $permission_ids[] = $permissionCreate->getKey();

            }

            $role->syncPermissions($permission_ids);

            commit();
            return routeWithSuccess('role.index', 'Role updated successfully');
        }

        rollback();
        return redirectBackWithErrors("Unable to edit role. Please try again");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Role::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    public function roles(Request $request)
    {
        $role = Role::find($request->input('id'));
        if (is_null($role)) {
            return responseJson(0, 'There was an unexpected error please try again');
        }

        $permissions = $role->getPermissions();
        $roles = getUserPermision();

        if (empty($roles) || !is_array($roles)) {
            return responseJson(0, 'Failed');
        }

        $menus = Menu::wherePermission(1)
            ->whereIn('route_prefix', $roles)
            ->get()->toArray();

        $response = [];
        $response['html'] = view('access.role.roles', compact('role', 'menus', 'permissions'))->render();
        $response['role'] = $role;

        return responseJson(200, 'Success', $response);
    }
}
