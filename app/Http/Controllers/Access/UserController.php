<?php

namespace App\Http\Controllers\Access;

use Auth;
use App\User;
use Carbon\Carbon;
use App\RoleModels\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = User::with('roles')
            ->whereNotNull('parent_id')
            ->whereNull('deleted_at')
            ->paginate(15);

        return view('access.user.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereNotNull('user_id')
            ->whereUserId(Auth::user()->id)
            ->whereStatus(1)
            ->get();

        return view('access.user.create', compact(
            'roles'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $roleId = $request->input('role');
        $request->request->add(['password' => bcrypt($request->input('password'))]);
        $createUser = User::create(array_merge($request->except(['_token', 'role']), ['role_id' => $roleId, 'parent_id' => Auth::user()->id]));
        beginTransaction();
        if (!$createUser) {
            rollback();
            return redirectBackWithErrors('Unable to create new user.');
        }

        if (!$createUser->assignRole($roleId)) {
            rollback();
            return redirectBackWithErrors('Unable to assign permission to new user.');
        }

        commit();
        return routeWithSuccess('user.index', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = User::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return redirectBackWithErrors("Unable to edit user details.Please try again");
        }

        $roles = Role::whereNotNull('user_id')
            ->whereUserId(Auth::user()->id)
            ->whereStatus(1)
            ->get();

        return view('access.user.edit', compact(
            'user',
            'roles'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return redirectBackWithErrors('Unable to find user details.');
        }

        $user->name = $request->input('name');
        if ($request->input('role') != $user->role_id) {
            $user->role_id = $request->input('role');
        }
        if (!$request->has('except_email')) {
            $checkUniqueEmail = User::whereEmail($request->input('email'))
                ->where('id', '!=', $id)
                ->count();
            if ($checkUniqueEmail <= 0) {
                $user->email = $request->input('email');
            }
        }

        beginTransaction();
        if (!$user->save()) {
            rollback();
            return redirectBackWithErrors('Unable to update user details.');
        }

        if ($request->input('role') != $user->role_id) {
            if (!$user->assignRole($request->input('role'))) {
                rollback();
                return redirectBackWithErrors('Unable to update permission to user.');
            }
        }

        commit();
        return routeWithSuccess('user.index', 'User details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = User::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
