<?php

namespace App\Http\Controllers\Home;

use App\Models\ContactUsMaster;
use App\Models\OrderMaster;
use App\Models\UserMaster;
use Auth;
use Hash;
use App\User;
use App\Models\Menu;
use App\RoleModels\Role;
use App\RoleModels\Permission;
use App\Http\Controllers\Controller;

use App\Http\Requests\ChangePasswordRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $app_users = UserMaster::all()->count();
        $activate_app_users = UserMaster::whereStatus(1)->count();
        $total_orders = OrderMaster::all()->count();
        $enquiries = ContactUsMaster::all()->count();
        return view('home.home', compact(
            'app_users',
            'activate_app_users',
            'total_orders',
            'enquiries'
        ));
    }

    public function superAdminPermissions()
    {
        if (\Auth::user()->id != 1) {
            echo '<script>alert("Login with super user credentials to re-generate super user permissions.");</script>';
        }

        $id = 1;

        $role = Role::findOrFail($id);

        $slugs = Menu::whereStatus(1)
            ->wherePermission(1)
            ->pluck('route_prefix')->toArray();

        $permission_ids = [];

        $role->revokeAllPermissions();

        foreach ($slugs as $key => $value) {

            $permission_data = [
                'name' => $value,
                'slug' => [
                    'create' => true,
                    'view' => true,
                    'update' => true,
                    'delete' => true,
                ],
                'description' => $value . ' permission'
            ];

            $permission = new Permission();

            $permissionCreate = $permission->create($permission_data);

            if (!$permissionCreate) {
                return "Unable to create permission";
            }

            $permission_ids[] = $permissionCreate->getKey();

        }


        $role->syncPermissions($permission_ids);

        return 'success';
    }

    public function showChangePassword()
    {
        return view('auth.passwords.change');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        if (!Hash::check($request->input('old_password'), Auth::user()->password)) {
            return redirectBackWithErrors('Old password is not matching with user password.');
        }

        if ($request->input('old_password') == $request->input('new_password')) {
            return redirectBackWithErrors('Old and new password could not be same.');
        }

        if ($request->input('confirm_password') != $request->input('new_password')) {
            return redirectBackWithErrors('New and confirm password should be same.');
        }

        $user = User::find(Auth::user()->id);
        if (is_null($user)) {
            return redirectBackWithErrors('Invalid user access found.');
        }

        $user->password = bcrypt($request->input('new_password'));
        if (!$user->save()) {
            return redirectBackWithErrors('Unable to change user password. Please try again later.');
        }

        Auth::guard()->logout();
        return routeWithSuccess('login', 'User password changes successfully.');
    }
}
