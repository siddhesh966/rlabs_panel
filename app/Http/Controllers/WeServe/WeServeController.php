<?php

namespace App\Http\Controllers\WeServe;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WeServeMaster;
use App\Http\Controllers\Controller;
use App\Http\Requests\WeServeRequest;
use App\Http\Controllers\ImageUploadController;

class WeServeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $details = WeServeMaster::whereNull('deleted_at')
            ->where(function ($q) use ($request) {
                if ($request->has('title') && !is_null($request->input('title'))) {
                    $q->where('title', 'like', '%' . $request->input('title') . '%');
                }
            })
            ->paginate(15);

        return view('we_serve.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('we_serve.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WeServeRequest $request)
    {
        $file_name = null;
        $file_path = 'we_serve';
        if ($request->hasFile('serve_image') && $request->file('serve_image')->isValid()) {
            $file = $request->file('serve_image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store image.');
            }
        } else {
            return redirectBackWithErrors('Invalid image file format.');
        }

        $data = $request->except(['_token', 'serve_image']);
        $data['image_path'] = $file_path . '/' . $file_name;

        $create = WeServeMaster::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to store details.');
        }

        return routeWithSuccess('we_serve.index', 'details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = WeServeMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = WeServeMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit banner details.");
        }

        return view('we_serve.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail = WeServeMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit details.");
        }

        $detail->banner_title = $request->input('title');
        $detail->banner_description = $request->input('description');

        $old_image = $detail->image_path;
        $file_name = '';
        $file_path = 'we_serve';
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store image.');
            }

            $detail->image_path = $file_path . '/' . $file_name;
        }

        if (!$detail->save()) {
            if ($file_name != '') {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return redirectBackWithErrors('Unable to update details.');
        }

        ImageUploadController::deleteFile($file_path, ImageUploadController::checkPath($file_path, $old_image));
        return routeWithSuccess('we_serve.index', ' details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = WeServeMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
