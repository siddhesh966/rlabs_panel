<?php

namespace App\Http\Controllers\Promo;

use App\Models\PromoCodeMaster;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\PromoRequest;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = PromoCodeMaster::whereNull('deleted_at')
            ->paginate(15);

        return view('promo.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromoRequest $request)
    {
        $data = $request->except(['_token']);
        $data['start_date'] = Carbon::parse($data['start_date'])->format('Y-m-d');
        $data['end_date'] = Carbon::parse($data['end_date'])->format('Y-m-d');
        $create = PromoCodeMaster::create($data);
        if (!$create) {
            return redirectBackWithErrors("unable to save promo data");
        }

        return routeWithSuccess('promo.index', 'Promo data saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = PromoCodeMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit promo details.");
        }

        return view('promo.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PromoRequest $request, $id)
    {
        $detail = PromoCodeMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit promo details.");
        }

        $detail->promo_code_name = $request->input('promo_code_name');
        $detail->promo_code_discount = $request->input('promo_code_discount');
        $detail->promo_code_discount_type = $request->input('promo_code_discount_type');
        $detail->start_date = Carbon::parse($request->input('start_date'))->format('Y-m-d');
        $detail->end_date = Carbon::parse($request->input('end_date'))->format('Y-m-d');

        if (!$detail->save()) {
            return redirectBackWithErrors('Unable to update promo details.');
        }

        return routeWithSuccess('promo.index', 'Promo details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = PromoCodeMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
