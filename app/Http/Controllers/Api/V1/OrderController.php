<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\PromoCodeMaster;
use App\Models\UserRedeemPromoDetail;
use Validator;
use Carbon\Carbon;
use App\Models\UserInfo;
use App\Models\OrderMaster;
use Illuminate\Http\Request;
use App\Models\OrderDetailMaster;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function checkOrderAvailToCheckout(Request $request)
    {
        $rules = [
            'cart_data' => 'required|array',
            'cart_data.*.serviceId' => 'required|numeric',
            'cart_data.*.sub_services_id' => 'required|numeric',
            'cart_data.*.servicesTypeId' => 'required|numeric',
            'cart_data.*.qty' => 'required|numeric',
            'cart_data.*.price' => 'required|numeric',
            'cart_data.*.date' => 'required|date',
            'cart_data.*.time' => 'required',
            'flag_type' => 'required|in:1,2'
        ];

        $messages = [
            'cart_data.required' => 'cart details field is required.',
            'cart_data.array' => 'invalid cart details data.',
            'cart_data.*.serviceId.required' => 'cart data service id field is required',
            'cart_data.*.serviceId.numeric' => 'invalid cart data service id field',
            'cart_data.*.sub_services_id.required' => 'cart data sub-service id field is required',
            'cart_data.*.sub_services_id.numeric' => 'invalid cart sub-service id field data',
            'cart_data.*.servicesTypeId.required' => 'cart data service type id field is required',
            'cart_data.*.servicesTypeId.numeric' => 'invalid cart service type id field data',
            'cart_data.*.qty.required' => 'cart data service quantity field is required',
            'cart_data.*.qty.numeric' => 'invalid cart service quantity field data',
            'cart_data.*.price.required' => 'cart data service price field is required',
            'cart_data.*.price.numeric' => 'invalid cart service price field data',
            'cart_data.*.date.required' => 'cart data service date field is required',
            'cart_data.*.date.date' => 'invalid cart service date field data',
            'cart_data.*.time.required' => 'cart data service time field is required',
            'flag_type.required' => 'flag type field is required',
            'flag_type.in' => 'invalid flag type field data',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $checkOrder = OrderDetailMaster::whereDate('order_date', '>', Carbon::now()->format('Y-m-d'))
            ->get();

        $checkAvail = collect($request->input('cart_data'))->map(function ($data) use ($checkOrder) {
            if ($data['date'] > Carbon::today() && ($checkOrder->where('order_date', $data['date'] . ' 00:00:00')->count() <= 4)) {
                $data['return_flag'] = 1;
            } else {
                $data['return_flag'] = 0;
            }

            return $data;
        });

        if ($request->input('flag_type') == 2) {
            $filter_data = $checkAvail->reject(function ($data) {
                return $data['return_flag'] == 0;
            });

            $checkAvail = [];
            foreach ($filter_data as $filter) {
                array_push($checkAvail, $filter);
            }
        }

        return responseJson(200, 'success', $checkAvail);
    }

    public function saveAddress(Request $request)
    {
        $rules = [
            'country_code' => 'required|numeric',
            'address1' => 'required',
            'address2' => 'required',
            'city' => 'required|numeric',
            'state' => 'required|numeric',
            'pincode' => 'required|numeric',
        ];

        $messages = [
            'country_code.required' => 'country field is required',
            'country_code.numeric' => 'invalid country option selected',
            'address1.required' => 'address 1 field is required',
            'address2.required' => 'address 2 field is required',
            'city.required' => 'city name field is required.',
            'city.numeric' => 'invalid city option selected',
            'state.required' => 'state name field is required.',
            'state.numeric' => 'invalid state option selected',
            'pincode.required' => 'pincode field is required.',
            'pincode.numeric' => 'invalid pincode entered',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }
        $data = $request->only('country_code', 'address1', 'address2', 'city', 'state', 'pincode');
        $data['user_id'] = auth()->user()->user_id;

        $save_address = UserInfo::create($data);
        if (!$save_address) {
            return responseJson(500, 'error', [
                'message' => 'No orders history found for this user.',
            ]);
        }

        $fetch_address = UserInfo::whereUserId(auth()->user()->user_id)
            ->with([
                'state_master' => function ($d) {
                    $d->select(['state_id', 'state_name']);
                },
                'city_master' => function ($d) {
                    $d->select(['city_id', 'state_id', 'city_name']);
                }
            ])
            ->whereStatus(1)
            ->whereNull('deleted_at')
            ->get()->toArray();

        return responseJson(200, 'success', $fetch_address);
    }

    public function applyPromoCode(Request $request)
    {
        $rules = [
            'promo_code_id' => 'required|numeric',
        ];

        $messages = [
            'promo_code_id.required' => 'Promo code parameter is required.',
            'promo_code_id.numeric' => 'The selected promo code is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $fetchPromoCodeDetails = PromoCodeMaster::find($request->input('promo_code_id'));
        if (is_null($fetchPromoCodeDetails)) {
            return responseJson(500, 'error', [
                'message' => 'Invalid promo code selected',
            ]);
        }


        if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
            return responseJson(500, 'error', [
                'message' => 'Selected promo code is expired.',
            ]);
        }

        $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', auth()->user()->user_id)
            ->where('promo_code_id', $request->input('promo_code_id'))
            ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
            ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
            ->count();

        if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
            return responseJson(500, 'error', [
                'message' => 'Your redeem limit exceeded for this promo code.',
            ]);
        }

        $create = UserRedeemPromoDetail::create([
            'user_master_id' => auth()->user()->user_id,
            'promo_code_id' => $request->input('promo_code_id')
        ]);

        if (!$create) {
            return responseJson(500, 'error', [
                'message' => 'unable to save customer promo code details.',
            ]);
        }

        return responseJson(200, 'Promo code applied successfully.');
    }

    public function saveOrder(Request $request)
    {
        $rules = [
            'total_price' => 'required|numeric',
            'address' => 'required|numeric',
            'cart_data' => 'required|array',
            'cart_data.*.serviceId' => 'required|numeric',
            'cart_data.*.sub_services_id' => 'required|numeric',
            'cart_data.*.servicesTypeId' => 'required|numeric',
            'cart_data.*.qty' => 'required|numeric',
            'cart_data.*.price' => 'required|numeric',
            'cart_data.*.date' => 'required|date',
            'cart_data.*.time' => 'required',
            'isPromoCode' => 'required|in:0,1',
        ];

        $messages = [
            'total_price.required' => 'total amount field is required',
            'total_price.numeric' => 'invalid total amount field.',
            'address.required' => 'address field is required',
            'address.numeric' => 'invalid address field.',
            'cart_data.required' => 'cart details field is required.',
            'cart_data.array' => 'invalid cart details data.',
            'cart_data.*.serviceId.required' => 'cart data service id field is required',
            'cart_data.*.serviceId.numeric' => 'invalid cart data service id field',
            'cart_data.*.sub_services_id.required' => 'cart data sub-service id field is required',
            'cart_data.*.sub_services_id.numeric' => 'invalid cart sub-service id field data',
            'cart_data.*.servicesTypeId.required' => 'cart data service type id field is required',
            'cart_data.*.servicesTypeId.numeric' => 'invalid cart service type id field data',
            'cart_data.*.qty.required' => 'cart data service quantity field is required',
            'cart_data.*.qty.numeric' => 'invalid cart service quantity field data',
            'cart_data.*.price.required' => 'cart data service price field is required',
            'cart_data.*.price.numeric' => 'invalid cart service price field data',
            'cart_data.*.date.required' => 'cart data service date field is required',
            'cart_data.*.date.date' => 'invalid cart service date field data',
            'cart_data.*.time.required' => 'cart data service time field is required',
            'isPromoCode.required' => 'Is-Promocode field is required',
            'isPromoCode.in' => 'Invalid is-promocode option selected',
        ];

        if ($request->has('order_type') && !is_null($request->input('order_type'))) {
            $rules['order_type'] = 'required|numeric|in:1,2';

            $messages['order_type.required'] = 'order type field is required.';
            $messages['order_type.numeric'] = 'invalid order type field data.';
            $messages['order_type.in'] = 'invalid order type option selected.';
        }

        if ($request->has('promo_code_id') && !is_null($request->input('promo_code_id'))) {
            $rules['promo_code_id'] = 'required|numeric';

            $messages['promo_code_id.required'] = 'promo code id field is required.';
            $messages['promo_code_id.numeric'] = 'invalid promo code id field data.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        if($request->input('isPromoCode') == 1) {
            $rules = [
                'promo_code_id' => 'required|numeric',
            ];

            $messages = [
                'promo_code_id.required' => 'Promo code parameter is required.',
                'promo_code_id.numeric' => 'The selected promo code is invalid.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return responseJson(422, 'Parameter missing.', [
                    'message' => 'The data is invalid.',
                    'errors' => $validator->errors()->all(),
                ]);
            }

            $fetchPromoCodeDetails = PromoCodeMaster::find($request->input('promo_code_id'));
            if (is_null($fetchPromoCodeDetails)) {
                return responseJson(500, 'error', [
                    'message' => 'Invalid promo code selected.',
                ]);
            }

            if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
                return responseJson(500, 'error', [
                    'message' => 'Selected promo code is expired.',
                ]);
            }

            $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', auth()->user()->user_id)
                ->where('promo_code_id', $request->input('promo_code_id'))
                ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
                ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
                ->count();

            if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
                return responseJson(500, 'error', [
                    'message' => 'Your redeem limit exceeded for this promo code.',
                ]);
            }
        }

        beginTransaction();
        $order_generate = OrderMaster::create([
            'order_number' => getGUIDnoHash(),
            'user_id' => auth()->user()->user_id,
            'total_price' => $request->input('total_price'),
            'order_type' => ($request->has('order_type') && !is_null($request->input('order_type'))) ? $request->input('order_type') : null,
            'address' => $request->input('address'),
            'promo_code_id' => $request->input('promo_code_id') ?? null,
        ]);

        if (!$order_generate) {
            rollback();
            return responseJson(500, 'error', [
                'message' => 'unable to generate order.',
            ]);
        }

        $data = collect($request->input('cart_data'))->map(function ($data) use ($order_generate) {
            $return['order_id'] = $order_generate->order_id;
            $return['order_number'] = $order_generate->order_number;
            $return['services_id'] = $data['serviceId'];
            $return['sub_services_id'] = $data['sub_services_id'];
            $return['services_type_id'] = $data['servicesTypeId'];
            $return['order_date'] = $data['date'];;
            $return['order_time'] = $data['time'];
            $return['quantity'] = $data['qty'];
            $return['price'] = $data['price'];
            $return['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $return['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            return $return;
        })->toArray();

        $order_details = OrderDetailMaster::insert($data);
        if (!$order_details) {
            rollback();
            return responseJson(500, 'error', [
                'message' => 'unable to generate order.',
            ]);
        }

        if($request->input('isPromoCode') == 1) {
            $create = UserRedeemPromoDetail::create([
                'user_master_id' => auth()->user()->user_id,
                'promo_code_id' => $request->input('promo_code_id')
            ]);

            if (!$create) {
                rollback();
                return responseJson(500, 'error', [
                    'message' => 'Unable to save customer promo code details.',
                ]);
            }
        }

        commit();
        return responseJson(200, 'success', [
            'order_number' => $order_generate->order_number,
        ]);
    }

    public function updateOrder(Request $request)
    {
        OrderMaster::where('order_number', $request->input('order_number'))
            ->where('order_type', 2)
            ->update([
                'order_payment' => $request->input('status'),
                'order_payment_details' => $request->input('payment_info')
            ]);
    }

    public function fetchOrders()
    {
        $orders = OrderMaster::whereUserId(auth()->user()->user_id)
            ->with([
                'order_detail_masters' => function ($q) {
                    $q->with([
                        'service_master' => function ($q1) {
                            $q1->select(['services_id', 'services_title']);
                        },
                        'sub_service' => function ($q1) {
                            $q1->select(['sub_service_id', 'sub_services_title']);
                        },
                        'services_area_price' => function ($q1) {
                            $q1->select(['services_type_id', 'services_type']);
                        }
                    ]);
                },
                'user_infos'
            ])
            ->orderBy('created_at', 'desc')
            ->get()->map(function ($data) {
                $data['ago'] = time_elapsed_string($data['created_at']);

                return $data;
            })->toArray();

        if (empty($orders)) {
            return responseJson(500, 'error', [
                'message' => 'No orders history found for this user.',
            ]);
        }

        return responseJson(200, 'success', $orders);
    }

    public function updateOrders(Request $request)
    {
        $rules = [
            'txnid' => 'required',
        ];

        $messages = [
            'txnid.required' => 'Transaction id field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }
    }

    public function payuMoneyHash(Request $request)
    {
        $rules = [
            'key' => 'required',
            'txnid' => 'required',
            'amount' => 'required',
            'productinfo' => 'required',
            'firstname' => 'required',
            'email' => 'required',
        ];

        $messages = [
            'key.required' => 'Key field is required',
            'txnid.required' => 'Transaction id field is required',
            'amount.required' => 'Amount field is required',
            'productinfo.required' => 'Product info field is required',
            'firstname.required' => 'country field is required',
            'email.required' => 'country field is required',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $key = $request->input('key');
        $txnid = $request->input('txnid');
        $amount = $request->input('amount');
        $productinfo = $request->input('productinfo');
        $firstname = $request->input('firstname');
        $email = $request->input('email');

        $salt = "dwvmI5vZlP";
        $payhash_str = $key . '|' . $this->checkNull($txnid) . '|' . $this->checkNull($amount) . '|' . $this->checkNull($productinfo) . '|' . $this->checkNull($firstname) . '|' . $this->checkNull($email) . '|||||||||||' . $salt;
        $hash = strtolower(hash('sha512', $payhash_str));
        return responseJson(200, 'success', $hash);
    }

    public function checkNull($value)
    {
        if ($value == null) {
            return '';
        } else {
            return $value;
        }
    }

    public function checkPromoCode(Request $request)
    {
        $rules = [
            'promo_code_id' => 'required|numeric',
        ];

        $messages = [
            'promo_code_id.required' => 'Promo code parameter is required.',
            'promo_code_id.numeric' => 'The selected promo code is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'Parameter missing.', [
                'message' => 'The data is invalid.',
                'errors' => $validator->errors()->all(),
                'status' => false,
            ]);
        }

        $fetchPromoCodeDetails = PromoCodeMaster::find($request->input('promo_code_id'));
        if (is_null($fetchPromoCodeDetails)) {
            return responseJson(500, 'error', [
                'message' => 'Invalid promo code selected.',
                'status' => false,
            ]);
        }

        if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
            return responseJson(500, 'error', [
                'message' => 'Selected promo code is expired.',
                'status' => false,
            ]);
        }

        $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', auth()->user()->user_id)
            ->where('promo_code_id', $request->input('promo_code_id'))
            ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
            ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
            ->where('status', 1)
            ->count();

        if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
            return responseJson(500, 'error', [
                'message' => 'Your redeem limit exceeded for this promo code.',
                'status' => false,
            ]);
        }

        return responseJson(200, 'success', [
            'message' => 'Promo code is valid.',
            'status' => true,
        ]);
    }
}
