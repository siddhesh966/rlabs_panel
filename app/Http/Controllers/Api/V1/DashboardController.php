<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\UserMaster;
use App\Models\PushNotificationLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function fetchNotificationList(Request $request)
    {
        $userDetails = $request->user('api')->toArray();

        $notificationList = PushNotificationLog::where('status', 1)
            ->where('user_ids', 'like', '%|'.$userDetails['user_id'].'|%')
            ->orderBy('created_at', 'desc')
            ->get()->toArray();

        if (empty($notificationList)) {
            return responseJson(404, 'You dont have any notifications yet.');
        }

        foreach($notificationList as $key => $notification){
            $notificationList[$key]['created_at']=time_elapsed_string($notification['created_at']);
        }
        return responseJson(200, 'Your notification list has fetched successfully.', $notificationList);
    }
}
