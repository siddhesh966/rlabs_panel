<?php

namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;
use App\Models\AppVersionDetail;

class VersionController extends Controller
{
    public function AppVersion($platform = null)
    {
        $details = AppVersionDetail::whereStatus(1)
            ->orderBy('app_version_id', 'desc');

        if (is_null($platform)) {
            $details = $details->select(['app_version_id', 'android_version_code', 'ios_version_code', 'android_update', 'ios_update']);
        } else {
            $details = $details->select(['app_version_id', strtolower($platform) . "_version_code", strtolower($platform) . "_update"]);
        }

        $details = $details->first();
        if (is_null($details)) {
            return responseJson(404, 'error', [
                'message' => 'Unable to fetch version.',
                'status' => false,
            ]);
        }

        return responseJson(200, 'success', $details);
    }
}
