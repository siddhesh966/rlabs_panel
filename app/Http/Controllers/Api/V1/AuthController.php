<?php

namespace App\Http\Controllers\Api\V1;

use Hash;
use Illuminate\Validation\Rule;
use Validator;
use Carbon\Carbon;
use App\Models\UserMaster;
use Illuminate\Http\Request;
use App\Models\OtpVerification;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SMSController;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\ImageUploadController;

class AuthController extends Controller
{
    public function registration(Request $request)
    {
        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
//            'company_name' => 'required',
            'mobile_no' => 'required|numeric|unique:user_master',
            'email' => 'required|email|unique:user_master',
            'password' => 'required',
            'device_token' => 'required',
//            'gender' => 'required|in:Male,Female',
//            'image' => 'required|mimes:jpeg,jpg,png',
        ];

        $messages = [
            'f_name.required' => 'first name is required field',
            'l_name.required' => 'last name is required field',
//            'company_name.required' => 'company name is required field',
            'mobile_no.required' => 'mobile number is required field',
            'mobile_no.numeric' => 'invalid mobile number field',
            'mobile_no.unique' => 'mobile number is used by other user',
            'email.required' => 'email id is required field',
            'email.email' => 'invalid email id field',
            'email.unique' => 'email id is used by other user',
            'password.required' => 'password is required field',
            'device_token.required' => 'device token is required field',
//            'gender.required' => 'gender is required field',
//            'gender.in' => 'invalid gender field',
//            'image.required' => 'profile image is required field',
//            'image.mimes' => 'invalid profile image file format(jpeg,jpg,png)',
        ];

        if ($request->has('company_name') && !is_null($request->input('company_name'))) {
            $rules['company_name'] = 'required';
            $messages['company_name.required'] = 'company name is required field';
        }

        if ($request->has('gender') && !is_null($request->input('gender'))) {
            $rules['gender'] = 'required|in:Male,Female';
            $messages['gender.required'] = 'gender is required field';
            $messages['gender.in'] = 'invalid gender field';
        }

        if ($request->hasFile('image')) {
            $rules['image'] = 'required|mimes:jpeg,jpg,png';
            $messages['image.required'] = 'profile image is required field';
            $messages['image.mimes'] = 'invalid profile image file format(jpeg,jpg,png)';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $file_name = null;
        $file_path = 'users/profile';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return responseJson(500, 'error', [
                    'message' => 'Unable to upload user profile picture',
                ]);
            }
        }

        $data = $request->except(['image']);
        $data['password'] = bcrypt($data['password']);
        $data['image'] = !is_null($file_name) ? ($file_path . '/' . $file_name) : null;

        $createUser = UserMaster::create($data);
        if (!$createUser) {
            return responseJson(500, 'error', [
                'message' => 'Unable to register user',
            ]);
        }

        $token = JWTAuth::fromUser($createUser);
        return responseJson(200, 'success', [
            'user' => $createUser,
            'token' => $token
        ]);
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $messages = [
            'email.required' => 'email id is required field',
            'email.email' => 'invalid email id field',
            'password.required' => 'password is required field',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $checkLogin = UserMaster::whereEmail($request->input('email'))
            ->select([
                'user_id',
                'f_name',
                'l_name',
                'company_name',
                'mobile_no',
                'email',
                'password',
                'gender',
                'image',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ])
            ->with([
                'user_infos' => function ($q) {
                    $q->with([
                        'state_master' => function ($d) {
                            $d->select(['state_id', 'state_name']);
                        },
                        'city_master' => function ($d) {
                            $d->select(['city_id', 'state_id', 'city_name']);
                        }
                    ]);
                }
            ])
            ->first();

        if (is_null($checkLogin)) {
            return responseJson(404, 'failed', [
                'message' => 'User credentials is invalid',
            ]);
        }

        if ($checkLogin->status != 1 || !is_null($checkLogin->deleted_at)) {
            return responseJson(500, 'failed', [
                'message' => 'User id de-activated/deleted by admin. ',
            ]);
        }

        if (!Hash::check($request->input('password'), $checkLogin->password)) {
            return responseJson(500, 'failed', [
                'message' => 'User password is invalid',
            ]);
        }

        $token = JWTAuth::fromUser($checkLogin);
        return responseJson(200, 'success', [
            'user' => $checkLogin,
            'token' => $token
        ]);
    }

    public function forgotPassword(Request $request)
    {
        $rules = [
            'mobile_no' => 'required|numeric',
        ];

        $messages = [
            'mobile_no.required' => 'mobile number is required field',
            'mobile_no.numeric' => 'invalid mobile number field',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $checkMobileNumber = UserMaster::whereMobileNo($request->input('mobile_no'))
            ->first();
        if (is_null($checkMobileNumber)) {
            return responseJson(500, 'error', [
                'message' => 'Mobile number is not registered with any user.',
            ]);
        }

        if ($checkMobileNumber->status != 1 || !is_null($checkMobileNumber->deleted_at)) {
            return responseJson(500, 'error', [
                'message' => 'User is de-activated/deleted by admin.',
            ]);
        }

        $otp_code = str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
        $message = "Your otp code is $otp_code for reset password and is valid for 10 min.";

        beginTransaction();
        $save_otp = OtpVerification::create([
            'mobile_no' => $request->input('mobile_no'),
            'otp_code' => $otp_code,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        if (!$save_otp) {
            rollback();
            return responseJson(500, 'error', [
                'message' => 'Unable to generate otp code.',
            ]);
        }

        $sms = SMSController::send($request->input('mobile_no'), $message);
        if ($sms['http_code'] > 200) {
            rollback();
            return responseJson(500, 'error', [
                'message' => 'Unable to send otp code on user mobile number.',
            ]);
        }

        commit();
        return responseJson(200, 'success', [
            'otp' => $otp_code
        ]);
    }

    public function resetPassword(Request $request)
    {
        $rules = [
            'mobile_no' => 'required|numeric',
            'otp_code' => 'required|numeric',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ];

        $messages = [
            'mobile_no.required' => 'mobile number is required field',
            'mobile_no.numeric' => 'invalid mobile number field',
            'otp_code.required' => 'otp code is required field',
            'otp_code.numeric' => 'invalid otp code field',
            'new_password.required' => 'new password is required field',
            'confirm_password.required' => 'confirm password is required field',
            'confirm_password.same' => 'new and confirm password is not match',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $checkOtp = OtpVerification::orderBy('created_at', 'desc')
            ->where('mobile_no', $request->input('mobile_no'))
            ->where('otp_code', $request->input('otp_code'))
            ->first();
        if (is_null($checkOtp)) {
            return responseJson(500, 'error', [
                'message' => "Otp code not found for given mobile number."
            ]);
        }

        if (!is_null($checkOtp->verified_at)) {
            return responseJson(500, 'error', [
                'message' => "Otp code already used for reset-password."
            ]);
        }

        $getDiff = Carbon::parse($checkOtp->created_at)->diffInMinutes(Carbon::now());
        if ($getDiff > 10) {
            return responseJson(500, 'error', [
                'message' => "Otp code is expired."
            ]);
        }

        beginTransaction();
        $checkOtp->verified_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$checkOtp->save()) {
            rollback();
            return responseJson(500, 'error', [
                'message' => "unable to update verified time."
            ]);
        }

        $updatePassword = UserMaster::whereMobileNo($request->input('mobile_no'))
            ->update([
                'password' => bcrypt($request->input('new_password')),
            ]);
        if (!$updatePassword) {
            rollback();
            return responseJson(500, 'error', [
                'message' => "unable to update/reset user password."
            ]);
        }

        commit();
        return responseJson(200, 'success', [
            'message' => "user password updated successfully."
        ]);
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->input('token'));
            return responseJson(200, 'success', [
                'message' => "You have successfully logged out."
            ]);
        } catch (JWTException $e) {
            return responseJson(500, 'error', [
                'message' => "Failed to logout, please try again."
            ]);
        }
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'current_password' => 'required|different:new_password',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ];

        $messages = [
            'current_password.required' => 'current password is required field',
            'current_password.different' => 'current password should not same as new password',
            'new_password.required' => 'new password is required field',
            'confirm_password.required' => 'confirm password is required field',
            'confirm_password.same' => 'new and confirm password is not match',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $change_pass = UserMaster::find(auth()->user()->user_id);
        if (is_null($change_pass)) {
            return responseJson(500, 'error', [
                'message' => 'Unable to find user data.',
            ]);
        }

        if (!Hash::check($request->input('current_password'), $change_pass->password)) {
            return responseJson(500, 'failed', [
                'message' => 'User current password is invalid',
            ]);
        }

        $change_pass->password = bcrypt($request->input('new_password'));
        if (!$change_pass->save()) {
            return responseJson(500, 'error', [
                'message' => 'Unable to change user password.',
            ]);
        }

        return responseJson(200, 'success');
    }

    public function updateProfile(Request $request)
    {
        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
            'mobile_no' => [
                'required',
                'numeric',
                Rule::unique('user_master')->ignore(auth()->user()->user_id, 'user_id')
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('user_master')->ignore(auth()->user()->user_id, 'user_id')
            ],
        ];

        $messages = [
            'f_name.required' => 'first name is required field',
            'l_name.required' => 'last name is required field',
            'mobile_no.required' => 'mobile number is required field',
            'mobile_no.numeric' => 'invalid mobile number field',
            'mobile_no.unique' => 'mobile number is used by other user',
            'email.required' => 'email id is required field',
            'email.email' => 'invalid email id field',
            'email.unique' => 'email id is used by other user',
        ];

        if ($request->has('company_name') && !is_null($request->input('company_name'))) {
            $rules['company_name'] = 'required';
            $messages['company_name.required'] = 'company name is required field';
        }

        if ($request->has('gender') && !is_null($request->input('gender'))) {
            $rules['gender'] = 'required|in:Male,Female';
            $messages['gender.required'] = 'gender is required field';
            $messages['gender.in'] = 'invalid gender field';
        }

        if ($request->hasFile('image')) {
            $rules['image'] = 'required|mimes:jpeg,jpg,png';
            $messages['image.required'] = 'profile image is required field';
            $messages['image.mimes'] = 'invalid profile image file format(jpeg,jpg,png)';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $checkUser = UserMaster::find(auth()->user()->user_id);
        if (is_null($checkUser)) {
            return responseJson(404, 'error', [
                'message' => 'Invalid user profile update request.',
            ]);
        }

        $checkUser->f_name = $request->input('f_name');
        $checkUser->l_name = $request->input('l_name');
        $checkUser->mobile_no = $request->input('mobile_no');
        $checkUser->email = $request->input('email');

        if ($request->has('company_name') && !is_null($request->input('company_name'))) {
            $checkUser->company_name = $request->input('company_name');
        }

        if ($request->has('gender') && !is_null($request->input('gender'))) {
            $checkUser->gender = $request->input('gender');
        }

        $file_name = null;
        $file_path = 'users/profile';
        $old_image = $checkUser->image;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return responseJson(500, 'error', [
                    'message' => 'Unable to update user profile picture',
                ]);
            }

            $checkUser->image = $file_path . '/' . $file_name;
        }

        if (!$checkUser->save()) {
            if (!is_null($file_name)) {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return responseJson(500, 'error', [
                'message' => 'Unable to update user profile details.',
            ]);
        }

        if (!is_null($old_image)) {
            ImageUploadController::deleteFile($file_path, str_replace($file_path, '', $old_image));
        }
        return responseJson(200, 'success', [
            'token' => str_replace(['Bearer ','bearer '], '', $request->header('authorization')),
            'user' => $checkUser
        ]);
    }
}
