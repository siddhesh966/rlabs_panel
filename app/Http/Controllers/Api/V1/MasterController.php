<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\OrderDetailMaster;
use Validator;
use Carbon\Carbon;
use App\Models\CityMaster;
use App\Models\OrderMaster;
use App\Models\StateMaster;
use Illuminate\Http\Request;
use App\Models\ContentMaster;
use App\Models\PromoCodeMaster;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    public function fetchStates()
    {
        $states = StateMaster::whereStatus(1)
            ->with(['city_masters'])
            ->whereNull('deleted_at')
            ->get();

        if (empty($states->toArray())) {
            return responseJson(500, 'error', [
                'message' => 'No states found.'
            ]);
        }

        return responseJson(200, 'success', $states);
    }

    public function fetchCities()
    {
        $cities = CityMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        if (empty($cities->toArray())) {
            return responseJson(500, 'error', [
                'message' => 'No cities found.'
            ]);
        }

        return responseJson(200, 'success', $cities);
    }

    public function fetchContents($id)
    {
        $content = ContentMaster::find($id);
        if (is_null($content)) {
            return responseJson(500, 'error', [
                'message' => 'content not found',
            ]);
        }

        return responseJson(200, 'success', $content->toArray());
    }

    public function fetchPromoCodes()
    {
        $promo_codes = PromoCodeMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'))
            ->get();

        if (empty($promo_codes->toArray())) {
            return responseJson(500, 'error', [
                'message' => 'No promo-codes found.'
            ]);
        }

        return responseJson(200, 'success', $promo_codes);
    }

    public function checkOrderAvailable(Request $request)
    {
        $rules = [
            'order_date' => 'required|date|date_format:d-m-Y',
        ];

        $messages = [
            'order_date.required' => 'current password is required field',
            'order_date.date' => 'invalid order date field data',
            'order_date.date_format' => 'invalid order date field data format',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $order_data = Carbon::parse($request->input('order_date'))->format('Y-m-d');
        $check_order = OrderDetailMaster::whereDate('order_date', $order_data)
            ->count();

        $data = [];
        switch ($check_order) {
            case 0:
                $data['message'] = "Order Available";
                $data['available'] = 4;
                break;
            case 1:
                $data['message'] = "Three Orders are Available";
                $data['available'] = 3;
                break;
            case 2:
                $data['message'] = "Two Orders are Available";
                $data['available'] = 2;
                break;
            case 3:
                $data['message'] = "One Order are Available";
                $data['available'] = 1;
                break;
            case 4:
                $data['message'] = "No order available for today ";
                $data['available'] = 0;
                break;
            default:
                $data['message'] = "No order available for today ";
                $data['available'] = 0;
                break;
        }

        return responseJson(200, 'success', $data);
    }
}
