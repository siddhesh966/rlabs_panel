<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\BannerMaster;
use Validator;
use Illuminate\Http\Request;
use App\Models\ServiceMaster;
use App\Models\SpecialServiceMaster;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function fetchServices()
    {
        $services = ServiceMaster::whereStatus(1)
            ->with([
                'sub_services' => function ($q) {
                    $q->whereStatus(1)
                        ->whereNull('deleted_at')
                        ->with([
                            'services_area_prices' => function ($q) {
                                $q->whereStatus(1)
                                    ->whereNull('deleted_at');
                            }
                        ]);
                }
            ])
            ->whereNull('deleted_at')
            ->orderBy('display_order', 'asc')
            ->get();

        if (empty($services->toArray())) {
            return responseJson(500, 'error', [
                'message' => 'No services found.'
            ]);
        }

        return responseJson(200, 'success', $services);
    }

    public function fetchServiceSliders()
    {
        $banners = BannerMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        if (empty($banners->toArray())) {
            return responseJson(500, 'error', [
                'message' => 'No services found.'
            ]);
        }

        return responseJson(200, 'success', $banners);
    }

    public function saveSpecialService(Request $request)
    {
        $rules = [
            'member_name' => 'required',
            'member_email' => 'required|email',
            'member_mobile' => 'required|numeric|digits:10',
            'message' => 'required',
            'special_category' => 'required',
        ];

        $messages = [
            'member_name.required' => 'member name field is required',
            'member_email.required' => 'member email id field is required',
            'member_email.email' => 'invalid member email id field',
            'member_mobile.required' => 'member name field is required',
            'member_mobile.numeric' => 'invalid member mobile number field',
            'member_mobile.digits' => 'invalid member mobile field data.',
            'message.required' => 'member message field is required',
            'special_category.required' => 'member special category option field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $create = SpecialServiceMaster::create($request->only('member_name', 'member_email', 'member_mobile', 'message', 'special_category'));
        if (!$create) {
            return responseJson(500, 'error', [
                'message' => 'Unable to save special service data.'
            ]);
        }

        return responseJson(200, 'success');
    }
}
