<?php

namespace App\Http\Controllers\Api\V1;

use Validator;
use App\Models\ServiceMaster;
use Illuminate\Http\Request;
use App\Models\ContactUsMaster;
use App\Models\AppContactUsMaster;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function fetchContactUs()
    {
        $contact = AppContactUsMaster::whereStatus(1)
            ->with([
                'app_contact_us_mobile_masters' => function ($q) {
                    $q->whereStatus(1)
                        ->whereNull('deleted_at');
                },
                'app_contact_us_email_masters' => function ($q) {
                    $q->whereStatus(1)
                        ->whereNull('deleted_at');
                }
            ])
            ->whereNull('deleted_at')
            ->orderBy('app_contact_id', 'desc')
            ->first();

        if (is_null($contact)) {
            return responseJson(500, 'error', [
                'message' => 'Unable to fetch contact us data.',
            ]);
        }

        $services = ServiceMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get()->toArray();

        $categories = [
            [
                'category_id' => 1,
                'category_title' => 'cleaning services'
            ]
        ];

        return responseJson(200, 'success', [
            'category' => $categories,
            'services' => $services,
            'contacts' => $contact
        ]);
    }

    public function saveContactUs(Request $request)
    {
        $rules = [
            'category_name' => 'required',
            'services_id' => 'required|numeric',
            'name' => 'required',
            'mobile_no' => 'required|numeric',
        ];

        $messages = [
            'category_name.required' => 'category name is required field',
            'services_id.required' => 'service id iss required field',
            'services_id.numeric' => 'invalid service id field data',
            'name.required' => 'name is required field',
            'mobile_no.required' => 'mobile number is required field',
            'mobile_no.numeric' => 'invalid mobile number field data',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return responseJson(422, 'parameter missing', [
                'message' => 'The data was invalid',
                'errors' => $validator->errors()->all(),
            ]);
        }

        $create = ContactUsMaster::create($request->only('category_name', 'services_id', 'name', 'mobile_no'));
        if (!$create) {
            return responseJson(500, 'error', [
                'message' => 'Unable to save contact us data.',
            ]);
        }

        return responseJson(200, 'success');
    }
}
