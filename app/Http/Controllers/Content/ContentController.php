<?php

namespace App\Http\Controllers\Content;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ContentMaster;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = ContentMaster::whereNull('deleted_at')
            ->paginate(15);

        return view('content.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = ContentMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = ContentMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service details.");
        }

        return view('content.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content_title' => 'required',
            'content_body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->messages());
        }

        $detail = ContentMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service details.");
        }

        $detail->content_title = $request->input('content_title');
        $detail->content_body = $request->input('content_body');
        if (!$detail->save()) {
            return redirectBackWithErrors('Unable to update ' . $request->input('content_title') . ' content details.');
        }

        return routeWithSuccess('content.index', ucfirst($request->input('content_title')) . ' content details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = ContentMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
