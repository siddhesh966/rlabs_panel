<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    public static function checkFolderExistOrNot($file_path)
    {
        if (!is_dir(public_path(UPLOADS . $file_path))) {
            mkdir(public_path(UPLOADS . $file_path), 0755, true);
        }
    }

    public static function saveFile($file, $file_path, $file_name)
    {
        self::checkFolderExistOrNot($file_path);
        if ($file->move(public_path(UPLOADS . $file_path), $file_name)) {
            return true;
        }

        return false;
    }

    public static function checkPath($file_path, $old_path)
    {
        $path = '';
        if (strpos($old_path, 'public/' . UPLOADS) !== false) {
            $path = explode('/', substr($old_path, strpos($old_path, $file_path), strlen($old_path)))[1];
        } else if (strpos($old_path, UPLOADS) !== false) {
            $path = explode('/', substr($old_path, strpos($old_path, $file_path), strlen($old_path)))[1];
        } else {
            $path = str_replace($file_path . '/', '', $old_path);
        }

        return $path;
    }

    public static function deleteFile($file_path, $file_name)
    {
        if (File::exists(public_path(UPLOADS . $file_path) . '/' . $file_name)) {
            File::delete(public_path(UPLOADS . $file_path) . '/' . $file_name);
            return true;
        }

        return false;
    }
}
