<?php

namespace App\Http\Controllers\Blog;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\BlogMaster;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Http\Controllers\ImageUploadController;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $details = BlogMaster::whereNull('deleted_at')
            ->where(function ($q) use ($request) {
                if ($request->has('blogTitle') && !is_null($request->input('blogTitle'))) {
                    $q->where('blogTitle', 'like', '%' . $request->input('blogTitle') . '%');
                }
            })
            ->paginate(15);

        return view('blog.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $file_name = null;
        $file_path = 'blog';
        if ($request->hasFile('blogImage') && $request->file('blogImage')->isValid()) {
            $file = $request->file('blogImage');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store banner image.');
            }
        } else {
            return redirectBackWithErrors('Invalid blog image file format.');
        }

        $data = $request->except(['_token', 'blogImage']);
        $data['banner_image_path'] = $file_path . '/' . $file_name;

        $create = BlogMaster::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to store blog details.');
        }

        return routeWithSuccess('blog.index', 'Blog details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = BlogMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = BlogMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit banner details.");
        }

        return view('blog.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail = BlogMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit blog details.");
        }

        $detail->blogTitle = $request->input('blogTitle');
        $detail->blogDesc    = $request->input('blogDesc    ');

        $old_image = $detail->banner_image_path;
        $file_name = '';
        $file_path = 'blog';
        if ($request->hasFile('blogImage') && $request->file('blogImage')->isValid()) {
            $file = $request->file('blogImage');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store blog image.');
            }

            $detail->banner_image_path = $file_path . '/' . $file_name;
        }

        if (!$detail->save()) {
            if ($file_name != '') {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return redirectBackWithErrors('Unable to update blog details.');
        }

        ImageUploadController::deleteFile($file_path, ImageUploadController::checkPath($file_path, $old_image));
        return routeWithSuccess('blog.index', 'Blog details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = BlogMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
