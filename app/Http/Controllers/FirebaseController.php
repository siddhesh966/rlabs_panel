<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FirebaseController extends Controller
{
    public static function send($notification_ids, $title = '', $body = '', $icon = '', $sound = '', $color = '', $data = [])
    {
        $api_key = 'AAAAUOzBUyk:APA91bEKz74ysz63dvZCYMvA72N5BnL2k6jlVIgS8jqpQC4ger73aS1NoKw6BuF4M3P8BIC23WfgFjHzFhsSVEU2jzyv-ylO2Z7fewaf9FlRtwlBMJloXkNtx8_LkmvLk_f7ffP5hNCy';

        $notification = [
            'title' => $title != '' ? $title : 'title',
            'body' => $body != '' ? $body : 'body',
            'icon' => $icon != '' ? $icon : 'logo',
            'sound' => $sound != '' ? $sound : 'default',
            'color' => $color != '' ? $color : '#ffffff'
        ];

        $data = [
            'message' => $body != '' ? $body : 'body',
            'click_action' => "PUSH_INTENT"
        ];

        $fields = [
            'registration_ids' => is_string($notification_ids) ? [$notification_ids] : $notification_ids,
            'notification' => $notification,
            'data' => $data,
            'priority' => 'normal'
        ];

        $headers = ['Authorization: key=' . $api_key, 'Content-Type: application/json'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
