<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Classes\Communication;
use App\Models\StateMaster;
use App\Models\UserInfo;
use App\Models\UserMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;
use Session;
use DB;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        $state_master = StateMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();
        return view('frontend.login', compact('state_master'));
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $messages = [
            'email.required' => 'Email id field is required.',
            'password.required' => 'Password field is required.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $checkLogin = UserMaster::with('user_infos')
            ->whereNull('deleted_at')
            ->whereStatus(1)
            ->where('email', $request->input('email'))
            ->first();

        if (is_null($checkLogin)) {
            return redirectBackWithErrors('Email id not registered.');
        }

        $password = $request->input('password');
        if (!(Hash::check($password, $checkLogin->password))) {
            return redirectBackWithErrors('Invalid credentials.');
        }

        $request->session()->forget('userMasterSession');

        $userMasterSession = [
            'user_id' => $checkLogin->user_id,
            'f_name' => $checkLogin->f_name,
            'l_name' => $checkLogin->l_name,
            'company_name' => $checkLogin->company_name,
            'mobile_no' => $checkLogin->mobile_no,
            'email' => $checkLogin->email,
            'gender' => $checkLogin->gender,
            'user_info_id' => $checkLogin->user_infos[0]->user_info_id,
            'address1' => $checkLogin->user_infos[0]->address1,
            'address2' => $checkLogin->user_infos[0]->address2,
            'country_code' => $checkLogin->user_infos[0]->country_code,
            'state' => $checkLogin->user_infos[0]->state,
            'city' => $checkLogin->user_infos[0]->city,
            'pincode' => $checkLogin->user_infos[0]->pincode,
        ];

        Session::put('userMasterSession', $userMasterSession);

        $route = 'website.index';
        if(Session::has('cart'))
        {
            $route = 'website.cart';
        }

        return routeWithSuccess($route, 'Logged in successfully.');
    }

    public function logout()
    {
        Session::forget('userMasterSession');
        return routeWithSuccess('website.index', 'Logged out successfully.');
    }

    public function register(Request $request)
    {
        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'country_code' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required|digits:6',
            'gender' => 'required',
            'mobile_no' => 'required|digits:10',
            'email' => 'required|email',
            'password' => 'required',
        ];

        $messages = [
            'f_name.required' => 'First name field is required.',
            'l_name.required' => 'Last name field is required.',
            'address1.required' => 'Address line 1 field is required.',
            'address2.required' => 'Address line 2 field is required.',
            'country_code.required' => 'Country field is required.',
            'state.required' => 'State field is required.',
            'city.required' => 'City field is required.',
            'pincode.required' => 'Pincode field is required.',
            'pincode.digits' => 'Invalid pincode.',
            'gender.required' => 'Gender field is required.',
            'mobile_no.required' => 'Mobile number field is required.',
            'mobile_no.digits' => 'Invalid mobile number.',
            'email.required' => 'Email id field is required.',
            'email.email' => 'Invalid email id.',
            'password.required' => 'Password field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $checkLogin = UserMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('email', $request->input('email'))
            ->first();

        if (!is_null($checkLogin)) {
            return redirectBackWithErrors('Email id is already registered.');
        }

        $password = $request->input('password');
        $hashPassword = Hash::make($password);

        $data = [
            "f_name" => $request->input('f_name') ?? null,
            "l_name" => $request->input('l_name') ?? null,
            "company_name" => $request->input('company_name') ?? null,
            "mobile_no" => $request->input('mobile_no') ?? null,
            "email" => $request->input('email') ?? null,
            "gender" => $request->input('gender') ?? null,
            "password" => $hashPassword ?? null,
            "status" => 1,
        ];

        beginTransaction();

        $create = UserMaster::create($data);
        if (!$create) {
            rollback();
            return redirectBackWithErrors('Unable to register details. Please try again.');
        }

        $sub_data = [
            "user_id" => $create->user_id ?? null,
            "country_code" => $request->input('country_code') ?? null,
            "address1" => $request->input('address1') ?? null,
            "address2" => $request->input('address2') ?? null,
            "city" => $request->input('city') ?? null,
            "state" => $request->input('state') ?? null,
            "pincode" => $request->input('pincode') ?? null,
            "status" => 1,
        ];

        $sub_create = UserInfo::create($sub_data);
        if (!$sub_create) {
            rollback();
            return redirectBackWithErrors('Unable to register details. Please try again.');
        }

        commit();

        $email_id = $request->input('email');
        $subject = 'Registration';
        $body = '
            <strong>Dear '.$request->input('f_name').',</strong>
            <p>Congratulations! You have registered successfully.</p>
            <p>Your credentails for login are:</p>
            <p>Username: '.$request->input('email').'</p>
            <p>Password: '.$password.'</p>
            <br>
            <p>Please keep this information safe for further use.</p>
            <br>
            <p>Thank you</p>
        ';
        $view = 'mails.textMailView';

        Communication::email_send($email_id, $subject, $body, $view);

        return routeWithSuccess('website.login', 'You have registered successfully. Please check your mail for login credentials.');
    }
}
