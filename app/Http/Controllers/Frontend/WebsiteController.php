<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Classes\Communication;
use App\Models\BannerMaster;
use App\Models\FeedbackMaster;
use App\Models\WeServeMaster;
use App\Models\BlogMaster;
use App\Models\CityMaster;
use App\Models\ContactUsMaster;
use App\Models\Faq;
use App\Models\Leads;
use App\Models\OrderDetailMaster;
use App\Models\OrderDetails;
use App\Models\OrderMaster;
use App\Models\PromoCodeMaster;
use App\Models\ServicesAreaPrice;
use App\Models\StateMaster;
use App\Models\ServiceMaster;
use App\Models\SubService;
use App\Models\Team;
use App\Models\UserInfo;
use App\Models\UserMaster;
use App\Models\UserRedeemPromoDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class WebsiteController extends Controller
{
    public function index()
    {
        $sub_services = SubService::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

        foreach($sub_services as $sub_service){
            $services_area_prices = ServicesAreaPrice::selectRaw(" MIN(services_price) AS services_price")
                ->whereNull('deleted_at')
                ->whereStatus(1)
                ->where('services_id', $sub_service->services_id)
                ->where('sub_services_id', $sub_service->sub_service_id)
                ->first()->toArray();
            $sub_service->min_services_price = $services_area_prices['services_price'];
        }

        $services = ServiceMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('services_id', 'desc')
            ->limit(3)
            ->get();

        $blogs = BlogMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('blog_id', 'desc')
            ->limit(3)
            ->get();

        $testimonials = Team::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('id', 'desc')
            ->get();

        $weserves = WeServeMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('we_serve_master_id', 'asc')
            ->get();

        $banners = BannerMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('banner_id', 'desc')
            ->get();

        return view('frontend.index', compact('sub_services', 'blogs', 'testimonials','banners','services','weserves'));
    }

    public function about_us()
    {
        $testimonials = Team::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('id', 'desc')
            ->get();
        $weserves = WeServeMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('we_serve_master_id', 'asc')
            ->get();

        return view('frontend.about_us', compact('testimonials','weserves'));
    }

    public function blog()
    {
        $blogs = BlogMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('blog_id', 'desc')
            ->paginate(6);

        return view('frontend.blog', compact('blogs'));
    }

    public function contact_us()
    {
        return view('frontend.contact_us');
    }
    public function services()
    {
        return view('frontend.service');
    }

    public function lead_generation()
    {
        return view('frontend.lead_genration');
    }

    public function career()
    {
        $testimonials = Team::whereNull('deleted_at')
            ->whereStatus(1)
            ->orderBy('id', 'desc')
            ->get();

        return view('frontend.career', compact('testimonials'));
    }

    public function faq()
    {
        $faqs = Faq::whereNull('deleted_at')
            ->whereStatus(1)
            ->whereFaq_type(1)
            ->orderBy('sequence', 'asc')
            ->get();

        $faqs_2 = Faq::whereNull('deleted_at')
            ->whereStatus(1)
            ->whereFaq_type(2)
            ->orderBy('sequence', 'asc')
            ->get();

        $faqs_3 = Faq::whereNull('deleted_at')
            ->whereStatus(1)
            ->whereFaq_type(3)
            ->orderBy('sequence', 'asc')
            ->get();

        $faqs_4 = Faq::whereNull('deleted_at')
            ->whereStatus(1)
            ->whereFaq_type(4)
            ->orderBy('sequence', 'asc')
            ->get();


        return view('frontend.faq', compact('faqs','faqs_2','faqs_3','faqs_4'));
    }

    public function cancellation_policy()
    {
        return view('frontend.cancellation_policy');
    }

    public function refund_policy()
    {
        return view('frontend.refund_policy');
    }

    public function blog_details($id)
    {
        $blog = BlogMaster::find($id);

        $related_blogs = BlogMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('blog_id', '!=', $id)
            ->inRandomOrder()
            ->limit(5)
            ->get();

        $previous_blog = BlogMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('blog_id', '<', $id)
            ->orderBy('blog_id', 'desc')
            ->first();

        $next_blog = BlogMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('blog_id', '>', $id)
            ->orderBy('blog_id', 'asc')
            ->first();

        return view('frontend.blog_details', compact('blog', 'related_blogs', 'previous_blog', 'next_blog'));
    }

    public function service_details($id)
    {
        $sub_services = SubService::find($id);

        $services_area_prices = ServicesAreaPrice::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('services_id', $sub_services->services_id)
            ->where('sub_services_id', $sub_services->sub_service_id)
            ->get();

        $arrServiceType = array();
        foreach($services_area_prices as $services_area_price){
            $arrServiceType[] = $services_area_price->services_type;
        }

        $related_services = SubService::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('sub_service_id', '!=', $id)
            ->inRandomOrder()
            ->limit(3)
            ->get();
        foreach($related_services as $sub_service){
            $services_area_prices1 = ServicesAreaPrice::selectRaw(" MIN(services_price) AS services_price")
                ->whereNull('deleted_at')
                ->whereStatus(1)
                ->where('services_id', $sub_service->services_id)
                ->where('sub_services_id', $sub_service->sub_service_id)
                ->first()->toArray();
            $sub_service->min_services_price = $services_area_prices1['services_price'];
        }

        return view('frontend.service_details', compact('sub_services',
            'services_area_prices', 'arrServiceType', 'related_services'));
    }

    public function add_to_cart(Request $request)
    {
        $sub_service = SubService::find($request->input('sub_service_id'));
        $service_type = ServicesAreaPrice::find($request->input('service_type'));

        if(is_null($sub_service) or is_null($service_type))
        {
            $code = 200;
            $type = "error";
            $message = "Record not found.";

            return responseJsonAjax($code, $type, $message);
        }

        $cart = Session::get('cart');

        if(!empty($cart) and array_key_exists($service_type->services_type_id, $cart))
        {
            $code = 200;
            $type = "error";
            $message = "Service already available in your cart.";

            return responseJsonAjax($code, $type, $message);
        }

        // $service_price = $service_type->services_price * $request->input('service_quantity');

        $cart[$service_type->services_type_id] = array(
            "services_id" => $sub_service->services_id,
            "sub_service_id" => $sub_service->sub_service_id,
            "sub_services_title" => $sub_service->sub_services_title,
            "sub_services_image" => $sub_service->sub_services_image,
            "services_type_id" => $service_type->services_type_id,
            "services_type_name" => $service_type->services_type,
            "services_price" => $service_type->services_price,
            "service_category" => $request->input('service_category'),
            "service_date" => $request->input('service_date'),
            "service_time" => $request->input('service_time'),
            "service_quantity" => $request->input('service_quantity'),
        );

        Session::put('cart', $cart);
        $cart_counter = sizeof($cart);

        $code = 200;
        $type = "success";
        $message = "Service added to cart successfully.";
        $data = [
            'cart_counter' => $cart_counter,
        ];

        return responseJsonAjax($code, $type, $message, '', '', '', $data);
    }

    public function delete_cart($id)
    {
        $cart = Session::get('cart');

        if(!array_key_exists($id, $cart))
        {
            $code = 200;
            $type = "error";
            $message = "Record not found.";

            return responseJsonAjax($code, $type, $message);
        }

        unset($cart[$id]);
        Session::put('cart', $cart);

        $code = 200;
        $type = "success";
        $message = "Cart updated successfully.";

        return responseJsonAjax($code, $type, $message, '', '', '', '');

    }

    public function update_cart(Request $request)
    {
        $key = $request->key;
        $quantity = $request->quantity;

        if(!isset($quantity))
        {
            $code = 200;
            $type = "error";
            $message = "Please enter quantity.";

            return responseJsonAjax($code, $type, $message);
        }

        if(!isset($key))
        {
            $code = 200;
            $type = "error";
            $message = "Record not found.";

            return responseJsonAjax($code, $type, $message);
        }


        $cart = Session::get('cart');

        if(!array_key_exists($key, $cart))
        {
            $code = 200;
            $type = "error";
            $message = "Record not found.";

            return responseJsonAjax($code, $type, $message);
        }

        $cart[$key]['service_quantity'] = $quantity;
        Session::put('cart', $cart);

        $totalPrice = $cart[$key]['services_price'] * $cart[$key]['service_quantity'];

        $cartPriceDetails = calculateCartPriceDetails($cart);

        $code = 200;
        $type = "success";
        $message = "Cart updated successfully.";
        $data = [
            'totalPrice' => $totalPrice,
            'subTotal' => $cartPriceDetails['subTotal'],
            'gst' => $cartPriceDetails['gst'],
            'finalTotal' => $cartPriceDetails['finalTotal'],
        ];

        return responseJsonAjax($code, $type, $message, '', '', '', $data);
    }

    public function cart()
    {
        $cart = Session::get('cart');
        $userMasterSession = Session::get('userMasterSession');

        return view('frontend.cart', compact('cart', 'userMasterSession'));
    }

    public function apply_coupon_code(Request $request)
    {
        $action = $request->input('action');
        $c_user_id = $request->input('c_user_id');
        $c_finalTotal = $request->input('c_finalTotal');
        $coupon_code = $request->input('coupon_code');
        switch($action)
        {
            case "apply_code":
                if(!$request->filled('coupon_code')){
                    $code = 200;
                    $type = "error";
                    $message = "Please select promo code.";
                    $data = [];
                    break;
                }

                $fetchPromoCodeDetails = PromoCodeMaster::find($coupon_code);
                if (is_null($fetchPromoCodeDetails)) {
                    $code = 200;
                    $type = "error";
                    $message = "Invalid promo code selected.";
                    $data = [];
                    break;
                }

                if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
                    $code = 200;
                    $type = "error";
                    $message = "Selected promo code is expired.";
                    $data = [];
                    break;
                }

                $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', $c_user_id)
                    ->where('promo_code_id', $coupon_code)
                    ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
                    ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
                    ->where('status', 1)
                    ->count();

                if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
                    $code = 200;
                    $type = "error";
                    $message = "Your redeem limit exceeded for this promo code.";
                    $data = [];
                    break;
                }

                $finalTotal = 0;
                if($fetchPromoCodeDetails->promo_code_discount_type == 1){
                    $discount = $c_finalTotal * ($fetchPromoCodeDetails->promo_code_discount/100);
                    $finalTotal = $c_finalTotal - $discount;
                }
                elseif($fetchPromoCodeDetails->promo_code_discount_type == 2){
                    $finalTotal = $c_finalTotal - $fetchPromoCodeDetails->promo_code_discount;
                }

                $code = 200;
                $type = "success";
                $message = "Coupon code applied successfully.";
                $data = [
                    "action" => $action,
                    "finalTotal" => $finalTotal,
                    "coupon_code" => $coupon_code,
                ];
                break;
            case "remove_code":
                $code = 200;
                $type = "success";
                $message = "Coupon code removed successfully.";
                $data = [
                    "action" => $action,
                    "finalTotal" => $c_finalTotal,
                    "coupon_code" => "",
                ];
                break;
        }

        return responseJsonAjax($code, $type, $message, "", "", "", $data);
    }

    public function check_coupon_code(Request $request)
    {
        $user_id = $request->input('p_user_id');
        $promo_code_id = $request->input('p_coupon_code');

        if(isset($promo_code_id) and $promo_code_id != "")
        {
            $fetchPromoCodeDetails = PromoCodeMaster::find($promo_code_id);
            if (is_null($fetchPromoCodeDetails)) {
                $code = 200;
                $type = "error";
                $message = "Invalid promo code selected.";
                return responseJsonAjax($code, $type, $message);
            }

            if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
                $code = 200;
                $type = "error";
                $message = "Selected promo code is expired.";
                return responseJsonAjax($code, $type, $message);
            }

            $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', $user_id)
                ->where('promo_code_id', $promo_code_id)
                ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
                ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
                ->where('status', 1)
                ->count();

            if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
                $code = 200;
                $type = "error";
                $message = "Your redeem limit exceeded for this promo code.";
                return responseJsonAjax($code, $type, $message);
            }

            $code = 200;
            $type = "success";
            $message = "Promo code is valid.";
            return responseJsonAjax($code, $type, $message);
        }

        $code = 200;
        $type = "success";
        $message = "Promo code is valid."; // No promo code is applied...
        return responseJsonAjax($code, $type, $message);
    }

    public function checkout()
    {
        $cart = Session::get('cart');
        $userMasterSession = Session::get('userMasterSession');

        $state_master = StateMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

        $promo_code_master = PromoCodeMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->where('end_date', '>=', date("Y-m-d"))
            ->get();

        return view('frontend.checkout', compact('cart', 'userMasterSession', 'state_master', 'promo_code_master'));
    }

    public function my_account()
    {
        $userMasterSession = Session::get('userMasterSession');

        $state_master = StateMaster::whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

        return view('frontend.my_account', compact('userMasterSession', 'state_master'));
    }

    public function update_account_details(Request $request)
    {
        $user_master = UserMaster::find($request->input('user_id'));
        $user_info = UserInfo::find($request->input('user_info_id'));
        if(is_null($user_master) or is_null($user_info))
        {
            $code = 200;
            $type = "error";
            $message = "Record not found.";

            return responseJsonAjax($code, $type, $message);
        }

        beginTransaction();
        $user_master->f_name = $request->input('f_name') ?? null;
        $user_master->l_name = $request->input('l_name') ?? null;
        $user_master->company_name = $request->input('company_name') ?? null;
        $user_master->gender = $request->input('gender') ?? null;
        $user_master->mobile_no = $request->input('mobile_no') ?? null;

        if(!$user_master->save())
        {
            rollback();
            $code = 200;
            $type = "error";
            $message = "Unable to update details.";

            return responseJsonAjax($code, $type, $message);
        }

        $user_info->address1 = $request->input('address1') ?? null;
        $user_info->address2 = $request->input('address2') ?? null;
        $user_info->country_code = $request->input('country_code') ?? null;
        $user_info->state = $request->input('state') ?? null;
        $user_info->city = $request->input('city') ?? null;
        $user_info->pincode = $request->input('pincode') ?? null;

        if(!$user_info->save())
        {
            rollback();
            $code = 200;
            $type = "error";
            $message = "Unable to update details.";

            return responseJsonAjax($code, $type, $message);
        }

        commit();

        $userMasterSession = Session::get('userMasterSession');

        $userMasterSession['f_name'] = $request->input('f_name');
        $userMasterSession['l_name'] = $request->input('l_name');
        $userMasterSession['company_name'] = $request->input('company_name');
        $userMasterSession['mobile_no'] = $request->input('mobile_no');
        $userMasterSession['gender'] = $request->input('gender');
        $userMasterSession['address1'] = $request->input('address1');
        $userMasterSession['address2'] = $request->input('address2');
        $userMasterSession['country_code'] = $request->input('country_code');
        $userMasterSession['state'] = $request->input('state');
        $userMasterSession['city'] = $request->input('city');
        $userMasterSession['pincode'] = $request->input('pincode');

        Session::put('userMasterSession', $userMasterSession);

        $code = 200;
        $type = "success";
        $message = "Details updated successfully.";
        $success = ["Details updated successfully."];
        $view = view('frontend.ajax.success', compact('success'))->render();
        $data = [
            "firstname" => $request->input('f_name')." ".$request->input('l_name'),
            "phone" => $request->input('mobile_no'),
        ];

        return responseJsonAjax($code, $type, $message, $view, "", $success, $data);
    }

    public function save_leads(Request $request)
    {
        $data = [
            "name" => $request->input('lead_name') ?? null,
            "email_id" => $request->input('lead_email') ?? null,
            "mobile_no" => $request->input('lead_mobile') ?? null,
            "message" => $request->input('lead_message') ?? null,
            "category_name" => $request->input('category_name') ?? null,
            "services_id" => $request->input('services_id') ?? null,
            "lead_type" => $request->input('lead_type') ?? null,
            "status" => 1,
        ];

        $create = ContactUsMaster::create($data);
        if (!$create) {
            $code = 200;
            $type = "error";
            $message = "Something went wrong. Please try again.";

            return responseJsonAjax($code, $type, $message);
        }

        $email_id = "siddesh@appectual.com";
        $subject = 'Contact Us';
        $body = '
            <strong>Dear Admin,</strong>
            <p>You have a new lead. Details are as follows:</p>
            <p>Name: '.$request->input('lead_name').'</p>
            <p>Email ID: '.$request->input('lead_email').'</p>
            <p>Mobile No.: '.$request->input('lead_mobile').'</p>
            <p>Message: '.$request->input('lead_message').'</p>
            <br>
            <p>Thank you</p>
        ';
        $view = 'mails.textMailView';

        Communication::email_send($email_id, $subject, $body, $view);

        $code = 200;
        $type = "success";
        $message = "Thankyou for contacting us. We'll get back to you soon.";

        return responseJsonAjax($code, $type, $message);
    }

    public function save_feedback(Request $request){

        $data = [
            "name" => $request->input('feed_name') ?? null,
            "designation" => $request->input('feed_desg') ?? null,
            "orgnization_name" => $request->input('feed_org') ?? null,
            "address" => $request->input('feed_address') ?? null,
            "email_id" => $request->input('email') ?? null,
            "mobile_no" => $request->input('feed_phone') ?? null,
            "point_contact" => $request->input('feed_pcrl') ?? null,
            "responseStar" => 1,
            "informationStar" => 1,
            "testCertificateStar" => 1,
            "parameterRequest" => $request->input('example') ?? '',
            "customerSupportStar" => 1,
            "GrievanceRedressalStar" => 1,
            "experienceStar" => 1,
            "seekingYr" => $request->input('feed_long') ?? null,
            "useServiceAgain" => $request->input('example2') ?? '',
            "rlEmployee" => $request->input('feed_emp') ?? null,
            "feed_oserv" => $request->input('feed_oserv') ?? null,
            "comments" => $request->input('feed_comm') ?? null,
            "status" =>1,
        ];

        $create = FeedbackMaster::create($data);
        if (!$create) {
            $code = 200;
            $type = "error";
            $message = "Something went wrong. Please try again.";

            return responseJsonAjax($code, $type, $message);
        }

        /*$email_id = "siddesh@appectual.com";
        $subject = 'Contact Us';
        $body = '
            <strong>Dear Admin,</strong>
            <p>You have a new lead. Details are as follows:</p>
            <p>Name: '.$request->input('lead_name').'</p>
            <p>Email ID: '.$request->input('lead_email').'</p>
            <p>Mobile No.: '.$request->input('lead_mobile').'</p>
            <p>Message: '.$request->input('lead_message').'</p>
            <br>
            <p>Thank you</p>
        ';
        $view = 'mails.textMailView';

        Communication::email_send($email_id, $subject, $body, $view);*/

        $code = 200;
        $type = "success";
        $message = "Thankyou for contacting us. We'll get back to you soon.";

        return responseJsonAjax($code, $type, $message);
    }

    public function save_email_subscription(Request $request)
    {
        $data = [
            "lead_email" => $request->input('newsletter_email') ?? null,
            "lead_type" => 2,
            "status" => 1,
        ];

        $create = Leads::create($data);
        if (!$create) {
            $code = 200;
            $type = "error";
            $message = "Something went wrong. Please try again.";

            return responseJsonAjax($code, $type, $message);
        }

        $email_id = $request->input('newsletter_email');
        $subject = 'News Letter Subscription';
        $body = '
            <strong>Dear User,</strong>
            <p>Thankyou for subscribing our news letter.</p>
        ';
        $view = 'mails.textMailView';

        Communication::email_send($email_id, $subject, $body, $view);

        $code = 200;
        $type = "success";
        $message = "Thank you for subscribing!";

        return responseJsonAjax($code, $type, $message);
    }

    public function fetch_city($id)
    {
        $city_master = CityMaster::where('state_id', $id)
            ->whereNull('deleted_at')
            ->whereStatus(1)
            ->get();

        if (empty($city_master->toArray())) {
            return responseJson(500, 'error', 'City not found.');
        }

        return responseJson(200, 'success', $city_master);
    }

    public function checkout_cod(Request $request)
    {
        $user_id = $request->input('p_user_id');
        $user_info_id = $request->input('p_user_info_id');
        $subTotal = $request->input('p_subTotal');
        $gst = $request->input('p_gst');
        $finalTotal = $request->input('p_finalTotal');
        $promo_code_id = $request->input('p_coupon_code');
        $userMasterSession = Session::get('userMasterSession');
        $cart = Session::get('cart');
        $order_number = getGUIDnoHash();

        if(isset($promo_code_id) and $promo_code_id != "")
        {
            $fetchPromoCodeDetails = PromoCodeMaster::find($promo_code_id);
            if (is_null($fetchPromoCodeDetails)) {
                return redirectBackWithErrors('Invalid promo code selected.');
            }

            if (!in_range(strtotime(date('Y-m-d')), strtotime($fetchPromoCodeDetails->start_date), strtotime($fetchPromoCodeDetails->end_date), TRUE)) {
                return redirectBackWithErrors('Selected promo code is expired.');
            }

            $checkUsedPromoByUser = UserRedeemPromoDetail::where('user_master_id', $user_id)
                ->where('promo_code_id', $promo_code_id)
                ->whereDate('created_at', '>=', $fetchPromoCodeDetails->start_date)
                ->whereDate('created_at', '<=', $fetchPromoCodeDetails->end_date)
                ->where('status', 1)
                ->count();

            if ($fetchPromoCodeDetails->no_of_times_to_apply <= $checkUsedPromoByUser) {
                return redirectBackWithErrors('Your redeem limit exceeded for this promo code.');
            }
        }

        beginTransaction();
        $order_generate = OrderMaster::create([
            'order_number' => $order_number,
            'user_id' => $user_id,
            'total_price' => $finalTotal,
            'order_type' => 1, // Cash on delivery...
            'address' => $user_info_id,
            'promo_code_id' => $promo_code_id ?? null,
        ]);

        if (!$order_generate) {
            rollback();
            return redirectBackWithErrors('Unable to generate order. Please try again.');
        }

        $data = collect($cart)->map(function ($data) use ($order_generate) {
            $return['order_id'] = $order_generate->order_id;
            $return['order_number'] = $order_generate->order_number;
            $return['services_id'] = $data['services_id'];
            $return['sub_services_id'] = $data['sub_service_id'];
            $return['services_type_id'] = $data['services_type_id'];
            $return['order_date'] = $data['service_date'];;
            $return['order_time'] = $data['service_time'];
            $return['quantity'] = $data['service_quantity'];
            $return['price'] = $data['services_price'];
            $return['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $return['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            return $return;
        })->toArray();

        $order_details_master = OrderDetailMaster::insert($data);
        if (!$order_details_master) {
            rollback();
            return redirectBackWithErrors('Unable to generate order. Please try again.');
        }

        if(isset($promo_code_id) and $promo_code_id != "") {
            $create = UserRedeemPromoDetail::create([
                'user_master_id' => $user_id,
                'promo_code_id' => $promo_code_id
            ]);

            if (!$create) {
                rollback();
                return redirectBackWithErrors('Unable to save promo code details. Please try again.');
            }
        }

        $order_details = OrderDetails::create([
            'user_master_id' => $user_id,
            'order_master_id' => $order_generate->order_id,
            'first_name' => $userMasterSession['f_name'] ?? null,
            'last_name' => $userMasterSession['l_name'] ?? null,
            'company_name' => $userMasterSession['company_name'] ?? null,
            'address1' => $userMasterSession['address1'] ?? null,
            'address2' => $userMasterSession['address2'] ?? null,
            'country_code' => $userMasterSession['country_code'] ?? null,
            'state' => $userMasterSession['state'] ?? null,
            'city' => $userMasterSession['city'] ?? null,
            'pincode' => $userMasterSession['pincode'] ?? null,
            'gender' => $userMasterSession['gender'] ?? null,
            'mobile_no' => $userMasterSession['mobile_no'] ?? null,
        ]);

        if (!$order_details) {
            rollback();
            return redirectBackWithErrors('Unable to save order details. Please try again.');
        }

        commit();

        // Trigger Mail To Customer + Vendor... ( Delivery Details, Cart Details, Pricing Details )
//        $email_id = $userMasterSession['email'];
//        $subject = 'Order Details';
//
//        $body = '
//            <strong>Dear '. $userMasterSession['f_name'] .',</strong>
//            <p>Following is the service details:</p>
//        ';
//        $view = 'mails.textMailView';
//        Communication::email_send($email_id, $subject, $body, $view);

        // Remove the cart from session...
        $request->session()->forget('cart');

        return routeWithSuccess('website.checkout_success', 'Order generated successfully.');
    }

    public function generate_hash(Request $request)
    {
        $salt = "dwvmI5vZlP";
        $payhash_str = $request->key . '|' . checkNull($request->txnid) . '|' . checkNull($request->amount) . '|' .
            checkNull($request->productinfo) . '|' . checkNull($request->firstname) . '|' . checkNull($request->email) . '|' .
            checkNull($request->udf1) . '|' . checkNull($request->udf2) . '|' . checkNull($request->udf3) . '|' . checkNull($request->udf4) . '|' .
            checkNull($request->udf5) . '||||||' . $salt;
        $hash = strtolower(hash('sha512', $payhash_str));

        if(empty($hash) or !isset($hash) or $hash == "")
        {
            $code = 200;
            $type = "error";
            $message = "Unable to generate hash.";
            $data = [
                "hash" => "",
            ];

            return responseJsonAjax($code, $type, $message, "", "", "", $data);
        }

        $code = 200;
        $type = "success";
        $message = "Hash generated successfully.";
        $data = [
            "hash" => $hash,
        ];

        return responseJsonAjax($code, $type, $message, "", "", "", $data);
    }

    public function checkout_online(Request $request)
    {
        if($request->filled('key') and isset($request->key))
        {
            $salt = "dwvmI5vZlP";
            $status = $request->input('status');
            $mihpayid = $request->input('mihpayid');
            $resphash = $request->input('hash');

            $keyString = $request->input('key') . '|' . $request->input('txnid') . '|' . $request->input('amount') . '|' .
                $request->input('productinfo') . '|' . $request->input('firstname') . '|' . $request->input('email') .
                '|' . $request->input('udf1') . '|' . $request->input('udf2') . '|' . $request->input('udf3') .
                '|' . $request->input('udf4') . '|' . $request->input('udf5') . '|||||';
            $keyArray = explode("|", $keyString);
            $reverseKeyArray = array_reverse($keyArray);
            $reverseKeyString = implode("|",$reverseKeyArray);
            $CalcHashString = strtolower(hash('sha512', $salt.'|'.$status.'|'.$reverseKeyString));

            if ($status == 'success'  && $resphash == $CalcHashString) {
                // Do success order processing here...
                $user_id = $request->input('udf1');
                $user_info_id = $request->input('udf2');
                $subTotal = $request->input('udf3');
                $gst = $request->input('udf4');
                $finalTotal = $request->input('amount');
                $promo_code_id = $request->input('udf5');
                $userMasterSession = Session::get('userMasterSession');
                $cart = Session::get('cart');
                $order_number = $request->input('txnid');

                beginTransaction();
                $order_generate = OrderMaster::create([
                    'order_number' => $order_number,
                    'user_id' => $user_id,
                    'total_price' => $finalTotal,
                    'order_type' => 2, // Paid Online...
                    'order_payment' => 1, // Payment Success...
                    'address' => $user_info_id,
                    'promo_code_id' => $promo_code_id ?? null,
                ]);

                if (!$order_generate) {
                    rollback();
                    return redirectBackWithErrors('Unable to generate order. Please try again.');
                }

                $data = collect($cart)->map(function ($data) use ($order_generate) {
                    $return['order_id'] = $order_generate->order_id;
                    $return['order_number'] = $order_generate->order_number;
                    $return['services_id'] = $data['services_id'];
                    $return['sub_services_id'] = $data['sub_service_id'];
                    $return['services_type_id'] = $data['services_type_id'];
                    $return['order_date'] = $data['service_date'];;
                    $return['order_time'] = $data['service_time'];
                    $return['quantity'] = $data['service_quantity'];
                    $return['price'] = $data['services_price'];
                    $return['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                    $return['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

                    return $return;
                })->toArray();

                $order_details_master = OrderDetailMaster::insert($data);
                if (!$order_details_master) {
                    rollback();
                    return redirectBackWithErrors('Unable to generate order. Please try again.');
                }

                if(isset($promo_code_id) and $promo_code_id != "") {
                    $create = UserRedeemPromoDetail::create([
                        'user_master_id' => $user_id,
                        'promo_code_id' => $promo_code_id
                    ]);

                    if (!$create) {
                        rollback();
                        return redirectBackWithErrors('Unable to save promo code details. Please try again.');
                    }
                }

                $order_details = OrderDetails::create([
                    'user_master_id' => $user_id,
                    'order_master_id' => $order_generate->order_id,
                    'first_name' => $userMasterSession['f_name'] ?? null,
                    'last_name' => $userMasterSession['l_name'] ?? null,
                    'company_name' => $userMasterSession['company_name'] ?? null,
                    'address1' => $userMasterSession['address1'] ?? null,
                    'address2' => $userMasterSession['address2'] ?? null,
                    'country_code' => $userMasterSession['country_code'] ?? null,
                    'state' => $userMasterSession['state'] ?? null,
                    'city' => $userMasterSession['city'] ?? null,
                    'pincode' => $userMasterSession['pincode'] ?? null,
                    'gender' => $userMasterSession['gender'] ?? null,
                    'mobile_no' => $userMasterSession['mobile_no'] ?? null,
                ]);

                if (!$order_details) {
                    rollback();
                    return redirectBackWithErrors('Unable to save order details. Please try again.');
                }

                commit();

                // Trigger Mail To Customer + Vendor... ( Delivery Details, Cart Details, Pricing Details )
//                $email_id = $userMasterSession['email'];
//                $subject = 'Order Details';
//
//                $body = '
//                    <strong>Dear '. $userMasterSession['f_name'] .',</strong>
//                    <p>Following is the service details:</p>
//                ';
//                $view = 'mails.textMailView';
//                Communication::email_send($email_id, $subject, $body, $view);

                // Remove the cart from session...
                $request->session()->forget('cart');

                return routeWithSuccess('website.checkout_success', 'Order generated successfully.');
            }
            else {
                // Tampered or Failed...
                return routeWithErrors('website.checkout_success', 'Payment failed...!');
            }
        }
        else {
            echo $msg = "Something went wrong...!";
            return routeWithErrors('website.checkout_success', 'Something went wrong...!');
        }
    }

    public function checkout_success()
    {
        echo "R";
    }
}
