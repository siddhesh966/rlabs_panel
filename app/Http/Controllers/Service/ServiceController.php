<?php

namespace App\Http\Controllers\Service;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ServiceMaster;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Http\Controllers\ImageUploadController;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $details = ServiceMaster::whereNull('deleted_at')
            ->where(function ($q) use ($request) {
                if ($request->has('services_title') && !is_null($request->input('services_title'))) {
                    $q->where('services_title', 'like', '%' . $request->input('services_title') . '%');
                }
            })
            ->paginate(15);

        return view('service.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $file_name = null;
        $file_path = 'service';
        if ($request->hasFile('services_image') && $request->file('services_image')->isValid()) {
            $file = $request->file('services_image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store service image.');
            }
        } else {
            return redirectBackWithErrors('Invalid service image file format.');
        }

        $data = $request->except(['_token', 'services_image']);
        $data['services_image'] = $file_path . '/' . $file_name;

        //pdf
       /* $file_name_pdf = null;
        $file_path_pdf = 'servicePdf';
        if ($request->hasFile('services_pdf') && $request->file('services_pdf')->isValid()) {
            $file_pdf = $request->file('services_pdf');
            $file_name_pdf = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file_pdf, $file_path_pdf, $file_name_pdf) === false) {
                return redirectBackWithErrors('Unable to store service pdf.');
            }
        } else {
            return redirectBackWithErrors('Invalid service pdf file format.');
        }

        $data = $request->except(['_token', 'services_pdf']);
        $data['services_pdf'] = $file_path_pdf . '/' . $file_name_pdf;*/





        $create = ServiceMaster::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to store service details.');
        }

        return routeWithSuccess('service.index', 'Service details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = ServiceMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = ServiceMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service details.");
        }

        return view('service.edit', compact(
            'detail'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail = ServiceMaster::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service details.");
        }

        $detail->services_title = $request->input('services_title');
        $detail->services_description = $request->input('services_description');
        $detail->display_order = $request->input('display_order');

        $old_image = $detail->services_image;
        $file_name = '';
        $file_path = 'service';
        if ($request->hasFile('services_image') && $request->file('services_image')->isValid()) {
            $file = $request->file('services_image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store service image.');
            }

            $detail->services_image = $file_path . '/' . $file_name;
        }

        if (!$detail->save()) {
            if ($file_name != '') {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return redirectBackWithErrors('Unable to update service details.');
        }

        ImageUploadController::deleteFile($file_path, str_replace($file_path . '/', '', $old_image));
        return routeWithSuccess('service.index', 'Service details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = ServiceMaster::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
