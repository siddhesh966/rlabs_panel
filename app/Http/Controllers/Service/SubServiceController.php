<?php

namespace App\Http\Controllers\Service;

use Carbon\Carbon;
use App\Models\SubService;
use Illuminate\Http\Request;
use App\Models\ServiceMaster;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubServiceRequest;
use App\Http\Controllers\ImageUploadController;

class SubServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $details = SubService::whereNull('deleted_at')
            ->where(function ($q) use ($request) {
                if ($request->has('sub_services_title') && !is_null($request->input('sub_services_title'))) {
                    $q->where('sub_services_title', 'like', '%' . $request->input('sub_services_title') . '%');
                }
            })
            ->whereHas('service_master', function ($q) use ($request) {
                if ($request->has('services_title') && !is_null($request->input('services_title'))) {
                    $q->where('services_title', 'like', '%' . $request->input('services_id') . '%');
                }
            })
            ->paginate(15);

        return view('sub-service.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = ServiceMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        return view('sub-service.create', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubServiceRequest $request)
    {
        $file_name = null;
        $file_path = 'service/sub-service';
        if ($request->hasFile('sub_services_image') && $request->file('sub_services_image')->isValid()) {
            $file = $request->file('sub_services_image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store sub-service image.');
            }
        } else {
            return redirectBackWithErrors('Invalid sub-service image file format.');
        }

        $data = $request->except(['_token', 'sub_services_image']);
        $data['sub_services_image'] = $file_path . '/' . $file_name;

        $create = SubService::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to store sub-service details.');
        }

        return routeWithSuccess('service.index', 'Sub-Service details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = SubService::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = ServiceMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        $detail = SubService::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit sub-service details.");
        }

        return view('sub-service.edit', compact(
            'detail',
            'services'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubServiceRequest $request, $id)
    {
        $detail = SubService::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit sub-service details.");
        }

        $detail->sub_services_title = $request->input('sub_services_title');
        $detail->sub_services_description = $request->input('sub_services_description');

        $old_image = $detail->sub_services_image;
        $file_name = '';
        $file_path = 'service/sub-service';
        if ($request->hasFile('sub_services_image') && $request->file('sub_services_image')->isValid()) {
            $file = $request->file('sub_services_image');
            $file_name = time() . '_' . mt_rand(0000, 9999) . '.' . $file->getClientOriginalExtension();
            if (ImageUploadController::saveFile($file, $file_path, $file_name) === false) {
                return redirectBackWithErrors('Unable to store sub-service image.');
            }

            $detail->sub_services_image = $file_path . '/' . $file_name;
        }

        if (!$detail->save()) {
            if ($file_name != '') {
                ImageUploadController::deleteFile($file_path, $file_name);
            }

            return redirectBackWithErrors('Unable to update sub-service details.');
        }

        ImageUploadController::deleteFile($file_path, str_replace($file_path . '/', '', $old_image));
        return routeWithSuccess('sub-service.index', 'Sub-Service details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = SubService::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }
}
