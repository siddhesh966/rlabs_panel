<?php

namespace App\Http\Controllers\Service;

use Carbon\Carbon;
use App\Models\SubService;
use Illuminate\Http\Request;
use App\Models\ServiceMaster;
use App\Models\ServicesAreaPrice;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceTypeRequest;

class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $details = ServicesAreaPrice::whereNull('deleted_at')
            ->where(function ($q) use ($request) {
                if ($request->has('services_type') && !is_null($request->input('services_type'))) {
                    $q->where('services_type', 'like', '%' . $request->input('services_type') . '%');
                }
            })
            ->whereHas('service_master', function ($q) use ($request) {
                if ($request->has('services_title') && !is_null($request->input('services_title'))) {
                    $q->where('services_title', 'like', '%' . $request->input('services_id') . '%');
                }
            })
            ->whereHas('sub_service', function ($q) use ($request) {
                if ($request->has('sub_services_title') && !is_null($request->input('sub_services_title'))) {
                    $q->where('sub_services_title', 'like', '%' . $request->input('sub_services_title') . '%');
                }
            })
            ->paginate(15);

        return view('service-type.index', compact(
            'details'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = ServiceMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        return view('service-type.create', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceTypeRequest $request)
    {
        $data = $request->except(['_token']);
        $create = ServicesAreaPrice::create($data);
        if (!$create) {
            return redirectBackWithErrors("unable to save service type data");
        }

        return routeWithSuccess('service-type.index', 'Service type saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = ServicesAreaPrice::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = $status->status == 1 ? 0 : 1;
        if (!$status->save()) {
            return responseJson(500, 'Unable to change status.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = ServicesAreaPrice::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service type details.");
        }

        $services = ServiceMaster::whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        $sub_services = SubService::whereStatus(1)
            ->whereNull('deleted_at')
            ->where('services_id', $detail->services_id)
            ->get();

        return view('service-type.edit', compact(
            'detail',
            'services',
            'sub_services'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceTypeRequest $request, $id)
    {
        $detail = ServicesAreaPrice::find($id);
        if (is_null($detail)) {
            return redirectBackWithErrors("Unable to edit service type details.");
        }

        $detail->services_id = $request->input('services_id');
        $detail->sub_services_id = $request->input('sub_services_id');
        $detail->services_type = $request->input('services_type');
        $detail->services_price = $request->input('services_price');

        if (!$detail->save()) {
            return redirectBackWithErrors('Unable to update service type details.');
        }

        return routeWithSuccess('service-type.index', 'Service type details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = ServicesAreaPrice::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Status changed successfully.');
    }

    public function fetchSubServices($id)
    {
        $sub_services = SubService::where('services_id', $id)
            ->whereStatus(1)
            ->whereNull('deleted_at')
            ->get();

        if (empty($sub_services->toArray())) {
            return responseJson(500, 'error', 'sub-services not found.');
        }

        return responseJson(200, 'success', $sub_services);
    }
}
