<?php

namespace App\Http\Controllers\Faq;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Faq;
use Validator;
use DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DB::enableQueryLog(); // Enable Query Log...
        $details = Faq::whereNull('deleted_at')
            ->orderBy('sequence', 'asc')
            ->paginate(10);
        // dd(DB::getQueryLog()); // Show Results Of Query Log...

        return view('faq.index', compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'faq_que' => 'required',
            'faq_ans' => 'required',
            'sequence' => 'nullable|numeric'
        ];

        $messages = [
            'faq_que.required' => 'Question field is required.',
            'faq_ans.required' => 'Answer field is required.',
            'sequence.numeric' => 'Sequence field must be numeric.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $data = $request->all();
        $data['status'] = 1;

        $create = Faq::create($data);
        if (!$create) {
            return redirectBackWithErrors('Unable to create faq details.');
        }

        return routeWithSuccess('faq.index', 'Faq details created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Faq::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }
        $status->status = $status->status === 0 ? 1 : 0;
        if (!$status->save()) {
            return responseJson(500, 'Unable to update status of this record.');
        }

        return responseJson(200, 'Status updated successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = Faq::find($id);
        if (is_null($details)) {
            return redirectBackWithErrors('Record not found.');
        }

        return view('faq.edit', compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'faq_que' => 'required',
            'faq_ans' => 'required',
            'sequence' => 'nullable|numeric'
        ];

        $messages = [
            'faq_que.required' => 'Question field is required.',
            'faq_ans.required' => 'Answer field is required.',
            'sequence.numeric' => 'Sequence field must be numeric.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirectBackWithErrors($validator->errors()->all());
        }

        $details = Faq::find($id);
        if (is_null($details)) {
            return redirectBackWithErrors('Record not found.');
        }

        $details->faq_que = $request->input('faq_que');
        $details->faq_ans = $request->input('faq_ans');
        $details->sequence = $request->input('sequence');

        if (!$details->save()) {
            return redirectBackWithErrors('Unable to update faq details.');
        }
        return routeWithSuccess('faq.index', 'Faq details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Faq::find($id);
        if (is_null($status)) {
            return responseJson(404, 'Record not found.');
        }

        $status->status = 0;
        $status->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        if (!$status->save()) {
            return responseJson(500, 'Unable to delete this record.');
        }

        return responseJson(200, 'Record deleted successfully.');
    }
}
