<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    private $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->subject($this->details['subject'])
            ->view($this->details['view'], ['body' => $this->details['body']]);

        // For CC Recipients...
        if(!empty($this->details['arrCcEmailIds']))
        {
            $mail = $mail->cc($this->details['arrCcEmailIds']);
        }

        // For Attachment...
        if(!empty($this->details['arrAttachmentFiles']))
        {
            foreach($this->details['arrAttachmentFiles'] as $singleAttachment)
            {
                $mail = $mail->attach($singleAttachment);
            }
        }

        return $mail;
    }
}
