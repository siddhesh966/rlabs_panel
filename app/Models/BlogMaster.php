<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogMaster extends Model
{
	use SoftDeletes;

	protected $table = 'blog_master';

	protected $primaryKey = 'blog_id';

	protected $casts = [
		'status' => 'int',
		'sequence' => 'int'
	];

	protected $fillable = [
		'blog_id',
		'blogTitle',
		'blogSubTitle',
		'blogDesc',
		'blogCategory',
		'blogImage',
		'sequence',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    public function getBlogImageAttribute()
    {
        if ($this->attributes['blogImage'] != '' || !is_null($this->attributes['blogImage'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['blogImage'];
        }

        return null;
    }
}
