<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderMaster
 *
 * @property int $order_id
 * @property string $order_number
 * @property int $user_id
 * @property float $total_price
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property UserMaster $user_master
 * @property Collection|OrderDetailMaster[] $order_detail_masters
 *
 * @package App\Models
 */
class OrderMaster extends Model
{
	use SoftDeletes;

	protected $table = 'order_master';

	protected $primaryKey = 'order_id';

	protected $casts = [
		'user_id' => 'int',
		'total_price' => 'float',
		'promo_code_id' => 'float',
		'status' => 'int'
	];

	protected $fillable = [
		'order_id',
		'order_number',
		'user_id',
		'total_price',
		'order_type',
		'address',
		'promo_code_id',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function user_master()
	{
		return $this->belongsTo(UserMaster::class, 'user_id');
	}

	public function order_detail_masters()
	{
		return $this->hasMany(OrderDetailMaster::class, 'order_id');
	}

    public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'order_master_id');
    }

	public function user_infos()
	{
		return $this->hasMany(UserInfo::class, 'user_info_id','address');
	}
}
