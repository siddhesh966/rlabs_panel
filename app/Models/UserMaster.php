<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class UserMaster
 *
 * @property int $user_id
 * @property string $f_name
 * @property string $l_name
 * @property string $company_name
 * @property string $mobile_no
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property string $image
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|UserInfo[] $user_infos
 *
 * @package App\Models
 */
class UserMaster extends Authenticatable implements JWTSubject
{
    use SoftDeletes;

    protected $table = 'user_master';

    protected $primaryKey = 'user_id';

    protected $casts = [
        'status' => 'int'
    ];

    protected $hidden = [
        'password'
    ];

    protected $fillable = [
        'user_id',
        'f_name',
        'l_name',
        'company_name',
        'mobile_no',
        'email',
        'password',
        'gender',
        'image',
        'device_token',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getImageAttribute()
    {
        if ($this->attributes['image'] != '' || !is_null($this->attributes['image'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['image'];
        }

        return '';
    }

    public function user_infos()
    {
        return $this->hasMany(UserInfo::class, 'user_id');
    }

    public function order_masters()
    {
        return $this->hasMany(OrderMaster::class, 'user_id');
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'user_master_id');
    }

    public function otp_verifications()
    {
        return $this->hasMany(OtpVerification::class, 'user_id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
