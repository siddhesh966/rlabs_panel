<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use SoftDeletes;

    protected $table = 'faq';

    protected $casts = [
        'faq_type' => 'int',
        'sequence' => 'int',
        'status' => 'int'
    ];

    protected $fillable = [
        'id',
        'faq_que',
        'faq_ans',
        'faq_type',
        'sequence',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
