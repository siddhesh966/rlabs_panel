<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderDetailMaster
 * 
 * @property int $order_detail_id
 * @property int $order_id
 * @property string $order_number
 * @property int $services_id
 * @property int $sub_services_id
 * @property int $services_type_id
 * @property Carbon $order_date
 * @property string $order_time
 * @property string $quantity
 * @property float $price
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property OrderMaster $order_master
 * @property ServiceMaster $service_master
 * @property ServicesAreaPrice $services_area_price
 * @property SubService $sub_service
 *
 * @package App\Models
 */
class OrderDetailMaster extends Model
{
	use SoftDeletes;

	protected $table = 'order_detail_master';

	protected $primaryKey = 'order_detail_id';

	protected $casts = [
		'order_id' => 'int',
		'services_id' => 'int',
		'sub_services_id' => 'int',
		'services_type_id' => 'int',
		'price' => 'float',
		'status' => 'int'
	];

	protected $dates = [
		'order_date'
	];

	protected $fillable = [
		'order_detail_id',
		'order_id',
		'order_number',
		'services_id',
		'sub_services_id',
		'services_type_id',
		'order_date',
		'order_time',
		'quantity',
		'price',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function order_master()
	{
		return $this->belongsTo(OrderMaster::class, 'order_id');
	}

	public function service_master()
	{
		return $this->belongsTo(ServiceMaster::class, 'services_id');
	}

	public function services_area_price()
	{
		return $this->belongsTo(ServicesAreaPrice::class, 'services_type_id');
	}

	public function sub_service()
	{
		return $this->belongsTo(SubService::class, 'sub_services_id');
	}
}
