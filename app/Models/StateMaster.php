<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StateMaster
 *
 * @property int $state_id
 * @property string $state_name
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|CityMaster[] $city_masters
 * @property Collection|UserInfo[] $user_infos
 *
 * @package App\Models
 */
class StateMaster extends Model
{
	use SoftDeletes;

	protected $table = 'state_master';

	protected $primaryKey = 'state_id';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'state_id',
		'state_name',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function city_masters()
	{
		return $this->hasMany(CityMaster::class, 'state_id');
	}

	public function user_infos()
	{
		return $this->hasMany(UserInfo::class, 'state');
	}

    public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'state');
    }
}
