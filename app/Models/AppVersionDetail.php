<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppVersionDetail extends Model
{
	protected $table = 'app_version_details';

	protected $primaryKey = 'app_version_id';

    public $timestamps = true;

	protected $casts = [
		'status' => 'int',
		'android_update' => 'int',
		'ios_update' => 'int'
	];

	protected $fillable = [
		'app_version_id',
		'android_version_code',
		'ios_version_code',
        'android_update',
		'ios_update',
		'status',
		'created_at',
		'updated_at'
	];
}
