<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use StorageHelper;

class PushNotificationLog extends Model
{
	protected $table = 'push_notification_log';

	protected $primaryKey = 'id';

    public $timestamps = true;

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'id',
		'title',
		'body',
		'image',
		'user_ids',
		'status',
		'created_at',
		'updated_at',
	];

//    public function getImageAttribute($value)
//    {
//        return StorageHelper::getSignedFilePath($value, config('app_config.s3_file.expire_time', 2));
//    }
}
