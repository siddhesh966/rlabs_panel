<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AppContactUsMaster
 *
 * @property int $app_contact_id
 * @property string $address1
 * @property int $address2
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class AppContactUsMaster extends Model
{
    use SoftDeletes;

    protected $table = 'app_contact_us_master';

    protected $primaryKey = 'app_contact_id';

    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'app_contact_id',
        'address1',
        'address2',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function app_contact_us_mobile_masters()
    {
        return $this->hasMany(AppContactUsMobileMaster::class, 'app_contact_id');
    }

    public function app_contact_us_email_masters()
    {
        return $this->hasMany(AppContactUsEmailMaster::class, 'app_contact_id');
    }
}
