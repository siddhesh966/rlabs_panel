<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactUsMaster
 *
 * @property int $contact_id
 * @property string $category_name
 * @property string $services_id
 * @property string $lead_type
 * @property string $name
 * @property string $mobile_no
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class ContactUsMaster extends Model
{
    use SoftDeletes;

    protected $table = 'contact_us_master';

    protected $primaryKey = 'contact_id';

    protected $casts = [
        /*'services_id' => 'int',*/
        'status' => 'int'
    ];

    protected $fillable = [
        'contact_id',
        'category_name',
        'services_id',
        'lead_type',
        'name',
        'mobile_no',
        'email_id',
        'message',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function service_master()
    {
        return $this->belongsTo(ServiceMaster::class , 'services_id');
    }
}
