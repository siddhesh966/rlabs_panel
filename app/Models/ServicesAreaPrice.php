<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServicesAreaPrice
 *
 * @property int $services_type_id
 * @property int $services_id
 * @property int $sub_services_id
 * @property string $services_type
 * @property float $services_price
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property ServiceMaster $service_master
 * @property SubService $sub_service
 * @property Collection|OrderDetailMaster[] $order_detail_masters
 *
 * @package App\Models
 */
class ServicesAreaPrice extends Model
{
	use SoftDeletes;

	protected $table = 'services_area_price';

	protected $primaryKey = 'services_type_id';

	protected $casts = [
		'services_id' => 'int',
		'sub_services_id' => 'int',
		'services_price' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'services_type_id',
		'services_id',
		'sub_services_id',
		'services_type',
		'services_price',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function service_master()
	{
		return $this->belongsTo(ServiceMaster::class, 'services_id');
	}

	public function sub_service()
	{
		return $this->belongsTo(SubService::class, 'sub_services_id');
	}

	public function order_detail_masters()
	{
		return $this->hasMany(OrderDetailMaster::class, 'services_type_id');
	}
}
