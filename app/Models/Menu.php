<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * 
 * @property int $id
 * @property int $parent_id
 * @property bool $permission
 * @property string $menu_name
 * @property string $route_prefix
 * @property string $route_name
 * @property string $icon
 * @property bool $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Menu $menu
 * @property Collection|Menu[] $menus
 *
 * @package App\Models
 */
class Menu extends Model
{
    protected $table = 'menus';

	protected $casts = [
		'parent_id' => 'int',
		'permission' => 'bool',
		'status' => 'bool'
	];

	protected $fillable = [
		'parent_id',
		'permission',
		'menu_name',
		'route_prefix',
		'route_name',
		'icon',
		'status'
	];

	public function menu()
	{
		return $this->belongsTo(\App\Models\Menu::class, 'parent_id');
	}

	public function menus()
	{
		return $this->hasMany(\App\Models\Menu::class, 'parent_id');
	}
}
