<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    protected $table = 'team';

    protected $casts = [
        'sequence' => 'int',
        'status' => 'int'
    ];

    protected $fillable = [
        'id',
        'emp_name',
        'emp_designation',
        'emp_desc',
        'emp_profile_filename',
        'sequence',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
