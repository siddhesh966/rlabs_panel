<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CityMaster
 *
 * @property int $city_id
 * @property int $state_id
 * @property string $city_name
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property StateMaster $state_master
 * @property Collection|UserInfo[] $user_infos
 *
 * @package App\Models
 */
class CityMaster extends Model
{
	use SoftDeletes;

	protected $table = 'city_master';

	protected $primaryKey = 'city_id';

	protected $casts = [
		'state_id' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'city_id',
		'state_id',
		'city_name',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function state_master()
	{
		return $this->belongsTo(StateMaster::class, 'state_id');
	}

	public function user_infos()
	{
		return $this->hasMany(UserInfo::class, 'city');
	}

    public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'city');
    }
}
