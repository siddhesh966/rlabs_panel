<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AppContactUsEmailMaster
 *
 * @property int $app_contact_us_email_id
 * @property int $app_contact_id
 * @property string $email
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class AppContactUsEmailMaster extends Model
{
    use SoftDeletes;

    protected $table = 'app_contact_us_email_master';

    protected $primaryKey = 'app_contact_us_email_id';

    protected $casts = [
        'app_contact_id' => 'int',
        'status' => 'int'
    ];

    protected $fillable = [
        'app_contact_us_email_id',
        'app_contact_id',
        'email',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function app_contact_us_e_master()
    {
        return $this->belongsTo(AppContactUsMaster::class, 'app_contact_id');
    }
}
