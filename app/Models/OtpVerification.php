<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OtpVerification
 *
 * @property int $otp_verification_id
 * @property string $mobile_no
 * @property int $otp_code
 * @property int $attempt
 * @property Carbon $created_at
 * @property Carbon $verified_at
 *
 * @package App\Models
 */
class OtpVerification extends Model
{
	protected $table = 'otp_verification';

	protected $primaryKey = 'otp_verification_id';

	public $timestamps = false;

	protected $casts = [
		'otp_code' => 'int',
		'attempt' => 'int'
	];

	protected $dates = [
		'verified_at'
	];

	protected $fillable = [
		'otp_verification_id',
		'mobile_no',
		'otp_code',
		'attempt',
		'created_at',
		'verified_at'
	];
}
