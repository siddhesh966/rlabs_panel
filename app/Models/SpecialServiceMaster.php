<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SpecialServiceMaster
 * 
 * @property int $special_service_master_id
 * @property string $member_name
 * @property string $member_email
 * @property string $member_mobile
 * @property string $message
 * @property string $special_category
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class SpecialServiceMaster extends Model
{
	use SoftDeletes;

	protected $table = 'special_service_master';

	protected $primaryKey = 'special_service_master_id';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'special_service_master_id',
		'member_name',
		'member_email',
		'member_mobile',
		'message',
		'special_category',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];
}
