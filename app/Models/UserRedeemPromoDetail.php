<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRedeemPromoDetail extends Model
{
    protected $table = 'user_redeem_promo_details';

    protected $primaryKey = 'user_redeem_promo_id';

    public $timestamps = true;

    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'user_redeem_promo_id',
        'user_master_id',
        'promo_code_id',
        'status',
        'created_at',
        'updated_at',
    ];
}
