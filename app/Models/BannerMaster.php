<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BannerMaster
 *
 * @property int $banner_id
 * @property string $banner_title
 * @property string $banner_description
 * @property string $banner_image_path
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class BannerMaster extends Model
{
	use SoftDeletes;

	protected $table = 'banner_master';

	protected $primaryKey = 'banner_id';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'banner_id',
		'banner_title',
		'banner_description',
		'banner_image_path',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    public function getBannerImagePathAttribute()
    {
        if ($this->attributes['banner_image_path'] != '' || !is_null($this->attributes['banner_image_path'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['banner_image_path'];
        }

        return null;
    }
}
