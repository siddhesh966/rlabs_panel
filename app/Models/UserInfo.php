<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserInfo
 * 
 * @property int $user_info_id
 * @property int $user_id
 * @property string $country_code
 * @property string $address
 * @property int $city
 * @property int $state
 * @property string $pincode
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property CityMaster $city_master
 * @property StateMaster $state_master
 * @property UserMaster $user_master
 *
 * @package App\Models
 */
class UserInfo extends Model
{
	use SoftDeletes;

	protected $table = 'user_info';

	protected $primaryKey = 'user_info_id';

	protected $casts = [
		'user_id' => 'int',
		'city' => 'int',
		'state' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'user_info_id',
		'user_id',
		'country_code',
		'address1',
		'address2',
		'city',
		'state',
		'pincode',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function city_master()
	{
		return $this->belongsTo(CityMaster::class, 'city');
	}

	public function state_master()
	{
		return $this->belongsTo(StateMaster::class, 'state');
	}

	public function user_master()
	{
		return $this->belongsTo(UserMaster::class, 'user_id');
	}
}
