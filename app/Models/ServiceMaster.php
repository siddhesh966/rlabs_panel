<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceMaster
 *
 * @property int $services_id
 * @property string $services_title
 * @property string $services_description
 * @property string $services_image
 * @property int $display_order
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|OrderDetailMaster[] $order_detail_masters
 * @property Collection|ServicesAreaPrice[] $services_area_prices
 * @property Collection|SubService[] $sub_services
 *
 * @package App\Models
 */
class ServiceMaster extends Model
{
	use SoftDeletes;

	protected $table = 'service_master';

	protected $primaryKey = 'services_id';

	protected $casts = [
		'display_order' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'services_id',
		'services_title',
		'services_description',
		'services_image',
        'services_pdf',
		'display_order',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    public function getServicesImageAttribute()
    {
        if ($this->attributes['services_image'] != '' || !is_null($this->attributes['services_image'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['services_image'];
        }

        return null;
    }

	public function order_detail_masters()
	{
		return $this->hasMany(OrderDetailMaster::class, 'services_id');
	}

	public function services_area_prices()
	{
		return $this->hasMany(ServicesAreaPrice::class, 'services_id');
	}

	public function sub_services()
	{
		return $this->hasMany(SubService::class, 'services_id');
	}
}
