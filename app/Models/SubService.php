<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SubService
 * 
 * @property int $sub_service_id
 * @property int $services_id
 * @property string $sub_services_title
 * @property string $sub_services_description
 * @property string $sub_services_image
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property ServiceMaster $service_master
 * @property Collection|OrderDetailMaster[] $order_detail_masters
 * @property Collection|ServicesAreaPrice[] $services_area_prices
 *
 * @package App\Models
 */
class SubService extends Model
{
	use SoftDeletes;

	protected $table = 'sub_services';

	protected $primaryKey = 'sub_service_id';

	protected $casts = [
		'services_id' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'sub_service_id',
		'services_id',
		'sub_services_title',
		'sub_services_description',
		'sub_services_image',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    public function getSubServicesImageAttribute()
    {
        if ($this->attributes['sub_services_image'] != '' || !is_null($this->attributes['sub_services_image'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['sub_services_image'];
        }

        return null;
    }

	public function service_master()
	{
		return $this->belongsTo(ServiceMaster::class, 'services_id');
	}

	public function order_detail_masters()
	{
		return $this->hasMany(OrderDetailMaster::class, 'sub_services_id');
	}

	public function services_area_prices()
	{
		return $this->hasMany(ServicesAreaPrice::class, 'sub_services_id');
	}
}
