<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContentMaster
 * 
 * @property int $content_id
 * @property int $content_type
 * @property string $content_title
 * @property string $content_body
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class ContentMaster extends Model
{
	use SoftDeletes;

	protected $table = 'content_master';

	protected $primaryKey = 'content_id';

	protected $casts = [
		'content_type' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'content_id',
		'content_type',
		'content_title',
		'content_body',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];
}
