<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromoCodeMaster
 *
 * @property int $promo_code_id
 * @property string $promo_code_name
 * @property int $promo_code_discount
 * @property int $promo_code_discount_type
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class PromoCodeMaster extends Model
{
	use SoftDeletes;

	protected $table = 'promo_code_master';

	protected $primaryKey = 'promo_code_id';

	protected $casts = [
		'promo_code_discount_type' => 'int',
		'no_of_times_to_apply' => 'int',
		'status' => 'int'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'promo_code_id',
		'promo_code_name',
		'promo_code_discount',
		'promo_code_discount_type',
		'start_date',
		'end_date',
		'no_of_times_to_apply',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];
}
