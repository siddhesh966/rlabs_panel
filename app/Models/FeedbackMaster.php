<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactUsMaster
 *
 * @property int $contact_id
 * @property string $category_name
 * @property string $services_id
 * @property string $lead_type
 * @property string $name
 * @property string $mobile_no
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class FeedbackMaster extends Model
{
    use SoftDeletes;

    protected $table = 'feedback_master';

    protected $primaryKey = 'feedback_master_id';

    protected $casts = [
        /*'services_id' => 'int',*/
        'status' => 'int'
    ];

    protected $fillable = [
        'feedback_master_id',
        'name',
        'designation',
        'orgnization_name',
        'address',
        'email_id',
        'mobile_no',
        'point_contact',
        'responseStar',
        'informationStar',
        'testCertificateStar',
        'customerSupportStar',
        'GrievanceRedressalStar',
        'experienceStar',
        'seekingYr',
        'useServiceAgain',
        'rlEmployee',
        'comments',
        'feed_oserv',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}
