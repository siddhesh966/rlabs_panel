<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BannerMaster
 *
 * @property int $we_serve_master_id
 * @property string $title
 * @property string $description
 * @property string $image_path
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class WeServeMaster extends Model
{
	use SoftDeletes;

	protected $table = 'we_serve_master';

	protected $primaryKey = 'we_serve_master_id';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'we_serve_master_id',
		'title',
		'description',
		'image_path',
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    public function getImagePathAttribute()
    {
        if ($this->attributes['image_path'] != '' || !is_null($this->attributes['image_path'])) {
            return env("APP_URL") . UPLOADS . $this->attributes['image_path'];
        }

        return null;
    }
}
