<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetails extends Model
{
    use SoftDeletes;

    protected $table = 'order_details';

    protected $casts = [
        'user_master_id' => 'int',
        'order_master_id' => 'int',
        'state' => 'int',
        'city' => 'int',
        'status' => 'int'
    ];

    protected $fillable = [
        'id',
        'user_master_id',
        'order_master_id',
        'first_name',
        'last_name',
        'company_name',
        'address1',
        'address2',
        'country_code',
        'state',
        'city',
        'pincode',
        'gender',
        'mobile_no',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user_master()
    {
        return $this->belongsTo(UserMaster::class, 'user_master_id');
    }

    public function order_master()
    {
        return $this->belongsTo(OrderMaster::class, 'order_master_id');
    }

    public function state_master()
    {
        return $this->belongsTo(StateMaster::class, 'state');
    }

    public function city_master()
    {
        return $this->belongsTo(CityMaster::class, 'city');
    }
}
