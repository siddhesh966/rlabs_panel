<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leads extends Model
{
    use SoftDeletes;

    protected $table = 'leads';

    protected $casts = [
        'lead_type' => 'int',
        'status' => 'int'
    ];

    protected $fillable = [
        'id',
        'lead_name',
        'lead_email',
        'lead_mobile',
        'lead_message',
        'lead_type',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
