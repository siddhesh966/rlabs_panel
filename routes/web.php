<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

define('UPLOADS', 'uploads/');

Route::get('/', 'Frontend\WebsiteController@index')->name('/');

Route::group([], function () {

    Route::get('login', [
        'as' => 'login',
        'uses' => 'Auth\LoginController@showLoginForm'
    ]);

    Route::post('login', [
        'as' => 'login-submit',
        'uses' => 'Auth\LoginController@login'
    ]);

    Route::get('logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout'
    ]);

    Route::get('password/reset', [
        'as' => 'password.request',
        'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
    ]);

    Route::post('password/email', [
        'as' => 'password.email',
        'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
    ]);

    Route::get('password/reset/{token}', [
        'as' => 'password.reset',
        'uses' => 'Auth\ResetPasswordController@showResetForm'
    ]);

    Route::post('password/reset', [
        'as' => 'password.update',
        'uses' => 'Auth\ResetPasswordController@reset'
    ]);

    Route::get('super_admin_roles', [
        'as' => 'super_admin_roles',
        'uses' => 'Home\HomeController@superAdminPermissions'
    ]);

    /*Route::get('/', [
        'as' => 'home.index',
        'uses' => 'Home\HomeController@index'
    ]);*/

    Route::get('home', [
        'as' => 'home.index',
        'uses' => 'Home\HomeController@index'
    ]);

    Route::group(['middleware' => ['auth', 'acl']], function () {

        Route::get('password/change', [
            'as' => 'password.change',
            'uses' => 'Home\HomeController@showChangePassword'
        ]);

        Route::post('password/change', [
            'as' => 'password.change.request',
            'uses' => 'Home\HomeController@changePassword'
        ]);

        Route::group(['protect_alias' => 'role'], function () {
            Route::post('role/fetch-role', 'Access\RoleController@roles')
                ->name('role.roles');

            Route::resource('role', 'Access\RoleController');
        });

        Route::group(['protect_alias' => 'user'], function () {
            Route::resource('user', 'Access\UserController');
        });

        Route::group(['protect_alias' => 'app-user'], function () {
            Route::resource('app-user', 'Access\AppUserController');
        });

        Route::group(['protect_alias' => 'banner'], function () {
            Route::resource('banner', 'Banner\BannerController');
        });

        Route::group(['protect_alias' => 'we_serve'], function () {
            Route::resource('we_serve', 'WeServe\WeServeController');
        });

        Route::group(['protect_alias' => 'service'], function () {
            Route::resource('service', 'Service\ServiceController');
        });

        Route::group(['protect_alias' => 'sub-service'], function () {
            Route::resource('sub-service', 'Service\SubServiceController');
        });

        Route::group(['protect_alias' => 'service-type'], function () {

            Route::get('fetch/sub-services/{id?}', 'Service\ServiceTypeController@fetchSubServices')
                ->name('fetch.sub-services');

            Route::resource('service-type', 'Service\ServiceTypeController');
        });

        Route::group(['protect_alias' => 'enquiry'], function () {
            Route::resource('enquiry', 'Enquiry\EnquiryController');
        });

        Route::group(['protect_alias' => 'promo'], function () {
            Route::resource('promo', 'Promo\PromoController');
        });

        Route::group(['protect_alias' => 'content'], function () {
            Route::resource('content', 'Content\ContentController');
        });

        Route::group(['protect_alias' => 'order'], function () {
            Route::resource('order', 'Order\OrderController');
        });

        Route::group(['protect_alias' => 'push-notification'], function () {
            Route::resource('push-notification', 'PushNotification\PushNotificationController');
        });

        Route::group(['protect_alias' => 'faq'], function () {
            Route::resource('faq', 'Faq\FaqController');
        });

        Route::group(['protect_alias' => 'blog'], function () {
            Route::resource('blog', 'Blog\BlogController');
        });

        Route::group(['protect_alias' => 'articles'], function () {
            Route::resource('articles', 'Blog\BlogController');
        });

        Route::group(['protect_alias' => 'team'], function () {
            Route::resource('team', 'Team\TeamController');
        });
    });
});

// Frontend Website Routes...
Route::prefix('website')->name('website.')->group(function () {
    Route::get('index', 'Frontend\WebsiteController@index')->name('index');
    Route::get('about_us', 'Frontend\WebsiteController@about_us')->name('about_us');
    Route::get('blog', 'Frontend\WebsiteController@blog')->name('blog');
    Route::get('contact_us', 'Frontend\WebsiteController@contact_us')->name('contact_us');
    Route::get('career', 'Frontend\WebsiteController@career')->name('career');
    Route::get('lead_generation', 'Frontend\WebsiteController@lead_generation')->name('lead_generation');
    Route::get('faq', 'Frontend\WebsiteController@faq')->name('faq');
    Route::get('cancellation_policy', 'Frontend\WebsiteController@cancellation_policy')->name('cancellation_policy');
    Route::get('refund_policy', 'Frontend\WebsiteController@refund_policy')->name('refund_policy');

    Route::get('cart', 'Frontend\WebsiteController@cart')->name('cart');
    Route::get('delete_cart/{id}', 'Frontend\WebsiteController@delete_cart')->name('delete_cart');
    Route::get('update_cart', 'Frontend\WebsiteController@update_cart')->name('update_cart');
    Route::post('add_to_cart', 'Frontend\WebsiteController@add_to_cart')->name('add_to_cart');

    Route::get('services', 'Frontend\WebsiteController@services')->name('services');
    Route::get('service_details/{id}', 'Frontend\WebsiteController@service_details')->name('service_details');

    Route::get('blog_details/{id}', 'Frontend\WebsiteController@blog_details')->name('blog_details');

    Route::get('login', 'Frontend\LoginController@showLoginForm')->name('login');
    Route::post('login_submit', 'Frontend\LoginController@login')->name('login_submit');
    Route::post('register_submit', 'Frontend\LoginController@register')->name('register_submit');

    Route::post('save_leads', 'Frontend\WebsiteController@save_leads')->name('save_leads');
    Route::post('save_feedback', 'Frontend\WebsiteController@save_feedback')->name('save_feedback');
    Route::post('save_email_subscription', 'Frontend\WebsiteController@save_email_subscription')->name('save_email_subscription');

    Route::group([
        'middleware' => [
            'website_user_auth'
        ]
    ], function () {
        Route::get('my_account', 'Frontend\WebsiteController@my_account')->name('my_account');
        Route::post('update_account_details', 'Frontend\WebsiteController@update_account_details')->name('update_account_details');

        Route::post('generate_hash', 'Frontend\WebsiteController@generate_hash')->name('generate_hash');
        Route::post('apply_coupon_code', 'Frontend\WebsiteController@apply_coupon_code')->name('apply_coupon_code');
        Route::post('check_coupon_code', 'Frontend\WebsiteController@check_coupon_code')->name('check_coupon_code');
        Route::post('checkout_cod', 'Frontend\WebsiteController@checkout_cod')->name('checkout_cod');
        Route::post('checkout_online', 'Frontend\WebsiteController@checkout_online')->name('checkout_online');

        Route::get('checkout_success', 'Frontend\WebsiteController@checkout_success')->name('checkout_success');

        Route::get('checkout', 'Frontend\WebsiteController@checkout')->name('checkout');

        Route::get('logout', 'Frontend\LoginController@logout')->name('logout');
    });

    Route::get('fetch_city/{id?}', 'Frontend\WebsiteController@fetch_city')->name('fetch_city');
});

