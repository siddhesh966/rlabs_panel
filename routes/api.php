<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application');

Route::prefix('/v1')->group(function () {

    Route::any('/', function () {
        return responseJson();
    });

    Route::get('/version/{platform?}', 'Api\V1\VersionController@AppVersion');

    Route::post('user/registration', 'Api\V1\AuthController@registration');

    Route::post('user/login', 'Api\V1\AuthController@login');

    Route::post('user/forgot-password', 'Api\V1\AuthController@forgotPassword');

    Route::post('user/reset-password', 'Api\V1\AuthController@resetPassword');

    Route::get('services', 'Api\V1\ServiceController@fetchServices');

    Route::get('services/slider', 'Api\V1\ServiceController@fetchServiceSliders');

    Route::post('special-service/save', 'Api\V1\ServiceController@saveSpecialService');

    Route::get('master/states', 'Api\V1\MasterController@fetchStates');

    Route::get('master/cities', 'Api\V1\MasterController@fetchCities');

    Route::get('content/{id}', 'Api\V1\MasterController@fetchContents');

    Route::get('master/promo-codes', 'Api\V1\MasterController@fetchPromoCodes');

    Route::post('check/available-order', 'Api\V1\MasterController@checkOrderAvailable');

    Route::get('contact-us/fetch', 'Api\V1\ContactUsController@fetchContactUs');

    Route::post('contact-us/save', 'Api\V1\ContactUsController@saveContactUs');

    Route::post('user/payumoney-hash', 'Api\V1\OrderController@payuMoneyHash');

    Route::post('cart/check-orders', 'Api\V1\OrderController@checkOrderAvailToCheckout');

    Route::post('update/orders', 'Api\V1\OrderController@updateOrders');

    Route::group(['middleware' => ['auth:api', 'jwt.auth']], function () {

        Route::get('user/details', function () {
            return auth()->user();
        });

        Route::delete('user/logout', 'Api\V1\AuthController@logout');

        Route::post('user/change-password', 'Api\V1\AuthController@changePassword');

        Route::post('user/update-profile', 'Api\V1\AuthController@updateProfile');

        Route::post('user/save-address', 'Api\V1\OrderController@saveAddress');

        Route::post('user/save-order', 'Api\V1\OrderController@saveOrder');

        Route::post('user/apply/promo-code', 'Api\V1\OrderController@applyPromoCode');

        Route::post('user/check/promo-code', 'Api\V1\OrderController@checkPromoCode');

        Route::get('user/fetch-orders', 'Api\V1\OrderController@fetchOrders');

        Route::post('user/update-order', 'Api\V1\OrderController@updateOrder');

        // Route For Notification Screen...
        Route::get('user/notification/list', 'Api\V1\DashboardController@fetchNotificationList');
    });
});
